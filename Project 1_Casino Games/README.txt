/*
 * Name: Marc Fleming
 * Student ID: 2858623
 * Section: CSC_5 Spring 2022
 * Date: 04/06
 * HW: Midterm Project
 * Problem: Create two Casino Games
 * I certify this is my own work and code
 */ 

Included in this folder is my Casino Games Project. The first game is BlackJack, and the second is 5 Card Draw Poker.
This was achieved by creating one large menu inside of a loop comprised of the entire code. First, the code will start with
giving the Player a free 100 chips to start with. Using the menu, the menu will display how many chips the player has, then
prompt the Player to select which game they would like to play. After the Player enters a value, the code will continue
to a switch/case statement, separating each of the games I have created.

For case 1, the first game is BlackJack. I initialize the cards for both the players and the dealer, the value of their
hands, and strings for both player cards and dealer cards to visualize on the screen. Using the vital shuffleDeck function,
the function randomizes between 1 and 13, assigned values of Ace(1) to King (13), then randomizes 1-4, this time for the 
suit. Shows the player their cards, then the program checks if the player received an Ace. If so, asks the player if they
want the Ace's value to be 1 or 11. Player chooses and continues. The program then checks if the player has a Blackjack. If
they do, the player automatically wins and the game pays out their winnings. If they do not have a blackjack (Just like at
a real table), the dealer will deal themselves two cards and flip up the second card for the player to see. Afterwords,
the player can determine if they want to 'hit' (accept another card from the Dealer), or 'stay' (hold the current score of
their hand). If the player goes over 21, the program will inform the player they 'Bust' and keep their bet. Then, the
dealer will reveal their other card and attempt to reach 21. The dealer MUST go to at least a soft 17, and will keep drawing 
cards until they are at 17 or greater, or until they bust over 21. The program will finally compare the two scores, and 
will output if the player or the dealer won. Then the program will ask the player if they want to play again.

For case 2, the second game is 5 card draw Poker. I initialize a matrix of 5 for the players hand, and for the value of 
their hand. I also make a matrix of strings for the players hand to viusalize on the screen. Loops the deckShuffle function
5 times, storing each 'card' in a matrix. Then the program will ask the user if they want to replace any cards, one by one.
After the player has decided for each card in their hand, the program will display their new hand. Then the program will
run through 5 different functions, each checking the players hand for a 'win'. if any of the functions return true, the
program will display what the player won and how many chips they won. Then the program will ask the player if they want to play again.

If the player does not want to play again, the program will ask them if they want to return to the main menu, if not,
the program will end.

Thank you very much for your time!