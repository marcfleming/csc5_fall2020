/*
 * Name: Marc Fleming
 * Student ID: 2858623
 * Date: 03/25/2022
 * HW: PROJECT ONE
 * Problem: Create two casino games
 * I certify this is my own work and code
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <windows.h>

using namespace std;

/* 
 * 
 * PROJECT 1: Create Two Casino Games
 * 
 */

/*==================================================================================*/
/*    PROTOTYPE FUNCTIONS! PLEASE SEE BOTTOM FOR NOTES AND COMMENTS ON FUNCTIONS.   */
/*==================================================================================*/
int betChecker(int bet, int chips);
int shuffleDeck(int cardValue, string &card, string &suit);
int blackJackFaceCards(int cardValue);
int aceFunction(int cardValue);
bool aPair(int pokerCard[]);
bool twoPairs(int pokerCard[]);
bool threeOfAKind(int pokerCard[]);
bool fourOfAKind(int pokerCard[]);
bool fullHouse(int pokerCard[]);
void screenClear();

// MAIN

int main(int argc, char** argv)
{
    /* First, I am going to create the menu, so the user can select which game they
     * would like to play
     */
    string doWhileGameInput;
    string repeatBlackJackInput;
    string repeatPokerInput;
    int chips = 100;
    int bet, winningChips, newChips;
    srand(time(0));

    cout << "WELCOME TO THE FLEMING CASINO!!!!" << endl;
    cout << "As A Friendly Entry Bonus, We Are Offering You A FREE " << chips << " chips!!!" << endl;

    do
    {
        //Here I am creating the Menu
        int menuOption;
        cout << "Friendly Reminder, You Currently Have " << chips << " Chips." << endl;
        cout << "Please Select From the Following Games!!!" << endl;
        cout << setfill('-') << setw(70) << " " << endl;
        cout << setfill(' ') << "(1): Blackjack" << setw(32) << "(2): 5 Card Draw Poker" << endl;
        cin >> menuOption;
        screenClear();

        switch(menuOption)
        {
            case 1:
            {
                // First Game is going to be a game of blackjack
                // I will begin by starting with a repeating menu
                cout << "WELCOME TO BLACKJACK" << endl;
                do
                {
                    // Initialize bets, 52 card deck, player and dealer
                    int cardNumOne, cardNumTwo, cardNumNew, dealerCardNumOne, dealerCardNumTwo, dealerCardNumNew;
                    int playerHand = 0, newHand = 0, dealerHand = 0, dealerNewHand = 0;
                    int cardValue;
                    string card, suit, cardOne, cardTwo, cardNew, suitOne, suitTwo, suitNew;
                    string dealerCardOne, dealerCardTwo, dealerCardNew, dealerSuitOne, dealerSuitTwo, dealerSuitNew;
                    string blackJackHit;

                    cout << setfill('-') << setw(60) << " " << endl;
                    cout << "PLEASE PLACE YOUR BETS" << endl;
                    cout << setfill(' ')
                            << "($10)" << setw(10) << "($20)" << setw(10) << "($30)" << endl;
                    cin >> bet;
                    bet = betChecker(bet, chips);
                    chips = chips - bet; // In a real game of poker your bet is removed from your total chips and put on the table
                    cout << "Thank you!" << endl;
                    cout << "Shuffling the deck. . . . ." << endl;

                    // Deck Shuffle Function
                    cardNumOne = shuffleDeck(cardValue, card, suit);
                    cardNumOne = blackJackFaceCards(cardNumOne);
                    cardOne = card;
                    suitOne = suit;


                    cout << "Your cards are the " << cardOne << " of " << suitOne << endl;

                    cardNumTwo = shuffleDeck(cardValue, card, suit);
                    cardNumTwo = blackJackFaceCards(cardNumTwo);
                    cardTwo = card;
                    suitTwo = suit;

                    cout << "And the " << cardTwo << " of " << suitTwo << endl;

                    // Create a function for Aces

                    if(cardOne == "Ace")
                    {
                        cardNumOne = aceFunction(cardNumOne);
                    }
                    if(cardTwo == "Ace")
                    {
                        cardNumTwo = aceFunction(cardNumTwo);
                    }

                    playerHand = cardNumOne + cardNumTwo;
                    cout << "Your hand has a score of " << playerHand << endl;

                    // Need to create a code to auto-win on a Blackjack

                    if(playerHand == 21)
                    {
                        cout << "We got a Blackjack!!! You Win!!!" << endl;
                        winningChips = bet * 2.5;
                        newChips = chips + winningChips;
                        chips = newChips;
                    }
                    else // If the player does not get a blackjack, the Dealer continues.
                    {
                        cout << "Now It's Time For The Dealer to Show a Card." << endl;

                        (dealerCardNumOne) = shuffleDeck(cardValue, card, suit);
                        (dealerCardNumOne) = blackJackFaceCards(dealerCardNumOne);
                        dealerCardOne = card;
                        dealerSuitOne = suit;

                        (dealerCardNumTwo) = shuffleDeck(cardValue, card, suit);
                        (dealerCardNumTwo) = blackJackFaceCards(dealerCardNumTwo);
                        dealerCardTwo = card;
                        dealerSuitTwo = suit;
                        dealerHand = dealerCardNumOne + dealerCardNumTwo;
                        cout << "Dealer shows " << dealerCardTwo << " of " << dealerSuitTwo << endl;
                        cout << " " << endl;

                        // Here I am creating the script to allow the player to get more 
                        // cards until they stop or they go over 21
                        cout << "Would You Like To Hit? (Would You Like Another Card?)" << endl;
                        cin >> blackJackHit;
                        blackJackHit = tolower(blackJackHit[0]);
                        if(blackJackHit == "y")
                        {
                            do
                            {
                                (cardNumNew) = shuffleDeck(cardValue, card, suit);
                                (cardNumNew) = blackJackFaceCards(cardNumNew);
                                cardNew = card;
                                suitNew = suit;

                                if(cardNew == "Ace")
                                {
                                    cardNumNew = aceFunction(cardNumNew);
                                }

                                newHand = playerHand + cardNumNew;
                                playerHand = newHand;
                                cout << "Your card is the " << cardNew << " of " << suitNew << endl;
                                cout << "Your hand has a score of " << playerHand << endl;
                                if(playerHand <= 21)
                                {
                                    cout << "Would You Like To Hit? (Would You Like Another Card?)" << endl;
                                    cin >> blackJackHit;
                                    blackJackHit = tolower(blackJackHit[0]);
                                }
                                else
                                {
                                    cout << "BUST" << endl;
                                }
                            }
                            while(blackJackHit == "y" && playerHand <= 21);
                        }
                        else
                        {
                            blackJackHit = "no";
                        }

                        cout << "Dealer Reveals Other Card. . ." << endl;
                        cout << "The " << dealerCardOne << " of " << dealerSuitOne << endl;
                        cout << "Dealer Has a Score of " << dealerHand << endl;

                        // This Code Will Only Occur if the Player Does NOT Bust!
                        // Dealer goes to soft 17
                        if(dealerHand < 17 && playerHand < 21)
                        {
                            cout << "Dealer Goes To Soft 17." << endl;
                            cout << "Drawing a card. . . " << endl;
                            do
                            {
                                (dealerCardNumNew) = shuffleDeck(cardValue, card, suit);
                                (dealerCardNumNew) = blackJackFaceCards(dealerCardNumNew);
                                dealerCardNew = card;
                                dealerSuitNew = suit;
                                dealerNewHand = dealerHand + dealerCardNumNew;
                                dealerHand = dealerNewHand;
                                cout << "Dealer Reveals The " << dealerCardNew << " of " << dealerSuitNew << endl;
                                cout << "Dealer's New Score Is " << dealerHand << endl;
                                if(dealerHand < 17)
                                {
                                    cout << "Dealer Under 17." << endl;
                                    cout << "Dealer Hits Again. . ." << endl;
                                    blackJackHit = "y";
                                }
                                else if(dealerHand > 21)
                                {
                                    cout << "DEALER BUST!!!" << endl;
                                    blackJackHit = "No";
                                }
                                else // If the Dealer is at soft 17 or over, but under 21. 
                                    //I could code this but then there would be no else
                                {
                                    cout << "Dealer has a score of " << dealerHand << endl;
                                    blackJackHit = "No";
                                }
                            }
                            while(blackJackHit == "y" && playerHand <= 21);
                        }
                        else
                        {
                            cout << "Dealer Stays At " << dealerHand << endl;
                        }

                        // Compare the player's hand with the Dealer's hand.
                        cout << "Your Hand Has a Score of " << playerHand << endl;
                        if((playerHand <= 21 && playerHand > dealerHand) || (playerHand <= 21 && dealerHand > 21))
                        {
                            cout << "YOU WIN!!!" << endl;
                            winningChips = bet * 2;
                            newChips = chips + winningChips;
                            chips = newChips;
                        }
                        else if(playerHand == dealerHand && dealerHand <= 21 && playerHand <= 21)
                        {
                            cout << "Push! Nobody Wins!" << endl;
                            newChips = chips + bet; // Give the player their chips back
                            chips = newChips;
                        }
                        else
                        {
                            cout << "Sorry You Lose!" << endl;
                        }
                    }
                    cout << "You Currently Have " << chips << " Chips Remaining." << endl;
                    cout << setfill('-') << setw(65) << " " << endl;
                    cout << "Do you want to play again? Y/N : " << endl;
                    cin >> repeatBlackJackInput;
                    screenClear();

                }
                while(tolower(repeatBlackJackInput[0]) == 'y');
                cout << "Thank you for playing!" << endl;
                screenClear();
                break;
            }
            case 2:
            {
                // Second Game is going to be 5 card draw`                    
                cout << "WELCOME TO FIVE CARD DRAW POKER!!!" << endl;
                do
                {
                    //Initialize the players hand of 5 cards, cardValue, and suits
                    int pokerCard[5], newCard[5];
                    int cardValue;
                    string newCardInput;
                    string card, suit, cardHand[5], suitHand[5];
                    cout << setfill('-') << setw(60) << " " << endl;
                    cout << "PLEASE PLACE YOUR BETS" << endl;
                    cout << setfill(' ')
                            << "($10)" << setw(15) << "($20)" << setw(15) << "($30)" << endl;
                    cin >> bet;
                    bet = betChecker(bet, chips);
                    chips = chips - bet; // Remove the bet from the players chips
                    cout << "Thank You!" << endl;
                    cout << "Shuffling the deck. . . . ." << endl;
                    cout << "Your Cards Are:" << endl;

                    // Deck Shuffle Function, in a loop to give the player a hand of 5

                    for(int i = 0; i < 5; i++)
                    {
                        pokerCard[i] = shuffleDeck(cardValue, card, suit);
                        cardHand[i] = card;
                        suitHand[i] = suit;
                        cout << (i + 1) << ") " << " The " << cardHand[i] << " of " << suitHand[i] << endl;
                    }

                    // Allow the Player to Get replacement cards
                    cout << "Note Each of These Cards Are Assigned a Number. IE the First Card is 1." << endl;
                    for(int i = 0; i < 5; i++)
                    {
                        cout << "Would You Like To Swap Card " << (i + 1) << "? Y/N: " << endl;
                        cin >> newCardInput;
                        if(tolower(newCardInput[0]) == 'y')
                        {
                            newCard[i] = shuffleDeck(cardValue, card, suit);
                            pokerCard[i] = newCard[i];
                            cardHand[i] = card;
                            suitHand[i] = suit;
                        }
                    }

                    cout << "Your Cards Are:" << endl;
                    for(int i = 0; i < 5; i++)
                    {
                        cout << (i + 1) << ") " << " The " << cardHand[i] << " of " << suitHand[i] << endl;
                    }
                    cout << " " << endl;

                    // Win-Check Functions.    
                    if(fullHouse(pokerCard))
                    {
                        cout << "WOW That's a Full House!!!!" << endl;
                        winningChips = bet * 5;
                        newChips = chips + winningChips;
                        chips = newChips;
                        cout << "You Won " << winningChips << " chips!!!!" << endl;
                    }
                    else if(fourOfAKind(pokerCard))
                    {
                        cout << "Four of a Kind!!! Wow!!!" << endl;
                        winningChips = bet * 4;
                        newChips = chips + winningChips;
                        chips = newChips;
                        cout << "You Won " << winningChips << " chips!!!" << endl;
                    }
                    else if(threeOfAKind(pokerCard))
                    {
                        cout << "Three of a Kind!!" << endl;
                        winningChips = bet * 3;
                        newChips = chips + winningChips;
                        chips = newChips;
                        cout << "You Won " << winningChips << " chips!!" << endl;
                    }
                    else if(twoPairs(pokerCard))
                    {
                        cout << "You Got Two Pairs!" << endl;
                        winningChips = bet * 2;
                        newChips = chips + winningChips;
                        chips = newChips;
                        cout << "You Won " << winningChips << " chips!" << endl;
                    }
                    else if(aPair(pokerCard))
                    {
                        cout << "You Got a Pair!" << endl;
                        winningChips = bet; // In Poker if you get a pair its the same as a 'push' in blackjack;
                        // You just receive your bet back.
                        newChips = chips + winningChips;
                        chips = newChips;
                        cout << "You received your bet back" << endl;
                    }
                    else
                    {
                        cout << "Sorry you didn't win anything this game..." << endl;
                    }

                    cout << "You have " << chips << " chips." << endl;
                    cout << setfill('-') << setw(65) << " " << endl;
                    cout << "Would You Like To Play Again?" << endl;
                    cin >> repeatPokerInput;
                    screenClear();
                }
                while(tolower(repeatPokerInput[0]) == 'y');
                cout << "Thank you for playing!" << endl;
                screenClear();
                break;
            }
            default:
            {
                cout << "ERROR, YOU DID NOT SELECT A CORRECT VALUE" << endl;
                cout << "PLEASE TRY AGAIN" << endl;
                screenClear();
            }
        }
        cout << "Would You Like to Return To the Game Menu? Y/N?" << endl;
        cin >> doWhileGameInput;
        screenClear();
    }
    while(tolower(doWhileGameInput[0]) == 'y');
    cout << "Have a Wonderful Day!" << endl;
    return 0;
}

/**
 * Function That checks the bet the player made to make sure the player cannot bet
 * more than they have. Also will error if the player enters a negative number.
 * Created for user friendliness
 * @param bet the amount of chips the player will bet
 * @param chips that the player currently has
 * @return the correct bet amount.
 */
int betChecker(int bet, int chips)
{
    if(bet < 0 || chips < bet)
        do
        {
            cout << "YOUT CANNOT BET NEGATIVE OR MORE CHIPS THAN YOU OWN!!!" << endl;
            cout << "PLEASE TRY AGAIN!!! THE SYSTEM IS WATCHING!!!" << endl;
            cout << " " << endl << "Now Then, How Many Chips Would You Like To Bet?: " << endl;
            cin >> bet;
        }
        while(bet < 0 || chips < bet);
    return bet;
}

/*
 * Because It is used multiple times, I created a Deck Shuffle Function.
 * Generates One card and outputs the card value for Blackjack, also sends the string 
 * of the card to a .dat file that can be used at any time outside of the function. 
 */

/**
 * Deck Shuffle Function. Randomizes twice, first time between 1 and 13 (Ace thru King)
 * and 1 and 4 (Hearts thru Spades)
 * @param cardValue The numeric "Value" of the card
 * @return Returns the Value of the card back to the user.
 */
shuffleDeck(int cardValue, string &card, string &suit)
{
    int cardRand, suitRand;

    cardRand = (rand() % 13) + 1;
    // Sleep(1500);
    suitRand = (rand() % 4) + 1;
    cardValue = cardRand;

    if(cardRand == 1)
    {
        card = "Ace";
    }
    else if(cardRand == 2)
    {
        card = "Two";
    }
    else if(cardRand == 3)
    {
        card = "Three";
    }
    else if(cardRand == 4)
    {
        card = "Four";
    }
    else if(cardRand == 5)
    {
        card = "Five";
    }
    else if(cardRand == 6)
    {
        card = "Six";
    }
    else if(cardRand == 7)
    {
        card = "Seven";
    }
    else if(cardRand == 8)
    {
        card = "Eight";
    }
    else if(cardRand == 9)
    {
        card = "Nine";
    }
    else if(cardRand == 10)
    {
        card = "Ten";
    }
    else if(cardRand == 11)
    {
        card = "Jack";
    }
    else if(cardRand == 12)
    {
        card = "Queen";
    }
    else // If cardRand == 13
    {
        card = "King";
    }

    // Then, what suit the card was
    if(suitRand == 1)
    {
        suit = "Hearts";
    }
    else if(suitRand == 2)
    {
        suit = "Diamonds";
    }
    else if(suitRand == 3)
    {
        suit = "Spades";
    }
    else
    {
        suit = "Clubs";
    }

    // Reset Values
    cardRand = 0;
    suitRand = 0;

    // Create File to be used as a resource at any time outside of the function.
    string input;
    ofstream fout;
    fout.open("Deck_Shuffler.dat");

    fout << card << endl;
    fout << suit << endl;

    fout.close();

    return cardValue;
}

/**
 * The BlackJack Face Cards Function goes hand in hand with the shuffleDeck function. 
 * This allows a slight modification to the deck, making all face value cards worth '10'.
 * @param cardValue The value of the card is brought into the function
 * @return returns the proper value if the card is a face card, or remains the same value 
 * if it's not a face card.
 */
int blackJackFaceCards(int cardValue)
{
    if(cardValue == 11)
    {
        cardValue = 10;
    }
    else if(cardValue == 12)
    {
        cardValue = 10;
    }
    else if(cardValue == 13)
    {
        cardValue = 10;
    }
    else // If there isn't a face card, the value stays the same
    {
        cardValue = cardValue;
    }
    return cardValue;
}

/**
 * Allows the User to choose if they want their Ace to be worth 1 or 11 points
 * @param cardValue The Numeric "Value" of the card, to be chosen by the user
 * @return The numeric value that the user has chosen
 */
aceFunction(int cardValue)
{
    int ace;
    do
    {
        cout << "You got an Ace! Would You Like It's Value to be 1 or 11?" << endl;

        cin >> ace;
        if(ace == 1)
        {
            cardValue = 1;
        }
        else if(ace == 11)
        {
            cardValue = 11;
        }
        else // if(ace != 1 && ace != 11)
        {
            cout << "ERROR! YOU MUST ENTER EITHER 1 or 11!!!" << endl;
        }
    }
    while(!(ace == 1 || ace == 11));
    return cardValue;
}

/**
 * Function checking if the player has a pair in their hand.
 * @param pokerCard checking the entire hand of pokerCards
 * @return true if the player has a pair, false if not
 */
bool aPair(int pokerCard[])
{
    for(int i = 0; i < 5; i++)
    {
        for(int compareCard = i + 1; compareCard < 6; compareCard++)
        {
            if(pokerCard[i] == pokerCard[compareCard])
            {
                return true;
            }
        }
    }
    return false;
}

/**
 * Function checking if the player has two pairs in their hand.
 * @param pokerCard checking the entire hand of pokerCards
 * @return true if the player has a pair, false if not.
 */
bool twoPairs(int pokerCard[])
{
    int match = 0;

    for(int i = 0; i < 5; i++)
    {
        // recursion
        for(int compareCard = i + 1; compareCard < 6; compareCard++)
        {
            if(pokerCard[i] == pokerCard[compareCard])
            {
                match++;
            }
        }
    }
    if(match == 2)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * Function to check if the player got a Three of a Kind. A bit tricky to figure out. 
 * achieved by creating a new matrix for pokerCard and comparing each value against the next.
 * if the loop finds a match, adds one to match and loops again.
 * @param pokerCard matrix of each pokerCard in hand
 * @return true if three of a kind exists, false if not
 */
bool threeOfAKind(int pokerCard[])
{
    for(int i = 0; i < 5; i++)
    {
        int match = 0;

        for(int compareCard = i + 1; compareCard < 6; compareCard++)
        {
            if(pokerCard[i] == pokerCard[compareCard])
            {
                match++;
            }

            if(match == 2)
            {
                return true;
            }
        }
    }
    return false;
}

/**
 * Function to check if the player got a Four of a Kind. 
 * achieved by creating a new matrix for pokerCard and comparing each value against the next.
 * if the loop finds a match, adds one to match and loops again.
 * @param pokerCard matrix of each pokerCard in hand
 * @return true if four of a kind exists, false if not
 */
bool fourOfAKind(int pokerCard[])
{
    for(int i = 0; i < 5; i++)
    {
        int match = 0;

        for(int compareCard = i + 1; compareCard < 6; compareCard++)
        {
            if(pokerCard[i] == pokerCard[compareCard])
            {
                match++;
            }

            if(match == 3)
            {
                return true;
            }
        }
    }
    return false;
}

/**
 * Function to Check if the player has a Full House in their Hand. Bit tricky to figure out, as 
 * both a three of a kind, and a pair are in this. First, I evaluated if there was a three of a kind
 * in the hand. If so, create an index of the three of a kind, and evaluate the remaining cards
 * to check for a single pair, not included the three of a kind. If a match is found,
 * return true, if not, return false.
 * @param pokerCard Array of the players hand
 * @return true if conditions are met, false if not
 */
bool fullHouse(int pokerCard[])
{
    if(threeOfAKind(pokerCard))
    {
        int threeOfAKindIndex[5];
        for(int i = 0; i < 5; i++)
        {
            for(int compareCard = i + 1; compareCard < 6; compareCard++)
            {
                if(pokerCard[i] == pokerCard[compareCard])
                {
                    threeOfAKindIndex[i] = pokerCard[i];
                }
                else
                {
                    threeOfAKindIndex[i] = 0;
                }
            }
        }
        for(int i = 0; i < 5; i++)
        {
            int match = 0;

            for(int compareCard = i + 1; compareCard < 6; compareCard++)
            {
                if(pokerCard[i] == pokerCard[compareCard] && pokerCard[i] != threeOfAKindIndex[i])
                {
                    match++;
                }
                if(match == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
    else
    {
        return false;
    }
}

/**
 * Function created to clear the screen. Created this because clearing the screen
 * is necessary multiple times.
 */
void screenClear()
{
    // Clear the Screen, As requested from the professor (30 lines)
    cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;
}