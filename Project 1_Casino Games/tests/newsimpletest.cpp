/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/testFiles/simpletest.cpp to edit this template
 */

/* 
 * File:   newsimpletest.cpp
 * Author: expec
 *
 * Created on April 4, 2022, 9:44 PM
 */

#include <stdlib.h>
#include <iostream>

/*
 * Simple C++ Test Suite
 */

bool threeOfAKind(int pokerCard[]);

void
testThreeOfAKind()
    {
    int* pokerCard;
    bool result = threeOfAKind(pokerCard);
    if(true /*check result*/)
    {
        std::cout << "%TEST_FAILED% time=0 testname=testThreeOfAKind (newsimpletest) message=error message sample" << std::endl;
    }
    }

int
main(int argc, char** argv)
    {
    std::cout << "%SUITE_STARTING% newsimpletest" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    std::cout << "%TEST_STARTED% testThreeOfAKind (newsimpletest)" << std::endl;
    testThreeOfAKind();
    std::cout << "%TEST_FINISHED% time=0 testThreeOfAKind (newsimpletest)" << std::endl;

    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return(EXIT_SUCCESS);
    }

