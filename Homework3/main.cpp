/*
* Name: Marc Fleming
* Student ID: 2858623
* Date: 03/12/2022
* HW: Homework 3
* Problem: 7 Questions
* I certify this is my own work and code
*/

/* 
 * File:   main.cpp
 * Author: Marc Fleming
 *
 * Created on March 12, 2022, 8:18 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <string>
#include <windows.h>

using namespace std;

int main()

//Wrap the Entire Menu in a Do-While Loop
{
  string doWhileMenuInput;
  string repeatGameInput;
  do
    {
  //Here I am creating the Menu
        int menuOption;
        cout << "Welcome to the Homework Menu!" 
                << "Please Select From the Following Options:" << endl;
        cout << setfill('-') << setw(71) << " " << endl;
        cout << setfill(' ') << "(1-3): The Calculating Calculator"
                << setw(37) << "(4): The Chocolate Calculator" << endl;
        cout << "(5): The Exercise Calculator" << setw(41) << "(6-7): Rock, Paper, Scissors" << endl;
        cin >> menuOption;
switch(menuOption)
        {
        case 1: case 2: case 3:
/* 
 * QUESTION 1: Write a program that reads in ten whole numbers and outputs the 
 * sum of all the numbers greater than zero, the sum of all the numbers less 
 * than zero (which will be a negative number or zero), and the sum of all the 
 * numbers, whether positive, negative or zero. The user enters the ten numbers 
 * one at a time and the user can enter them in any order. The program should 
 * not ask to enter the positive and negative numbers separately. The program 
 * should also display the numbers used.
 */
    
        {
      // Decide what Data I am storing, and assign data types for variables
            int num1, num2, num3, num4, num5, num6, num7, num8, num9, num10;
            int randNum = rand();
            int low = -100;
            int high = 100;
            int number = 0;
            int positiveNum = 0;
            int negativeNum = 0;
            int positiveCount = 0;
            int negativeCount = 0;
            int sumTotal = 0;
            double positiveTotal = 0;
            double negativeTotal = 0;
            double positiveAverage = 0.00;
            double negativeAverage = 0.00;
            double totalAverage = 0;
      
      //Ask the User to Input Values for Variables
            cout << "Hello and Welcome to The Calculating Calculator!" << endl;
            cout << setfill('-') << setw(50) << " " << endl;
            cout << "I am going to ask you to input 10 different numbers," << endl;
            cout << "They can be positive or negative" << endl;
      
      //Create Function to achieve desired results
        for(int i = 0; i < 10; i++) 
          {
            srand(time(0));
            randNum = (rand() % (high - low + 1)) + low;
            cout << "Please enter a number:" << endl;
            //cin >> number;
            number = randNum;
            cout << "You Entered: " << number << endl;
            Sleep(1000);
            if (number > 0) 
            {
              positiveNum = number;
              positiveTotal = positiveTotal + positiveNum;
              positiveCount++;
            } 
            else if (number < 0) 
            {
              negativeNum = number;
              negativeTotal = negativeTotal + negativeNum;
              negativeCount++;
            } 
            else if (number == 0) 
            {
              continue;
            } 
            else 
            {
              cout << "Not Within Valid Parameters!" << endl;
              cout << "Please Try again!" << endl;
              break;
            }
          }
      
            sumTotal = negativeTotal + positiveTotal;
            positiveAverage = (positiveTotal)/(positiveCount);
            negativeAverage = (negativeTotal)/(negativeCount);
            totalAverage = sumTotal/10;
            cout << "Thank you for your patience!" << endl;

      //cout << "To start, these are the numbers that you inputted into the "
              //<< "system." << endl;
      //cout << setfill('-') << setw(50) << " " << endl;
      //cout << setfill(' ') << num1 << setw(8) << num2 << setw(8) 
              //<< num3 << setw(8) << num4 << setw(8) << num5 << endl;
      
            cout << setfill('-') << setw(65) << " " << endl;
            cout << "The Sum of all the numbers is: " << sumTotal << endl;
            cout << " " << endl;
            cout << "The Sum of all the Positive numbers is: " << positiveTotal << endl;
            cout << " " << endl;
            cout << "The Average of all the Positive numbers is: " 
                  << showpoint << setprecision(3) << positiveAverage << endl;
            cout << " " << endl;
            cout << "The Sum of all the Negative numbers is: " << negativeTotal << endl;
            cout << " " << endl;
            cout << "The Average of all the Negative numbers is: " 
                  << showpoint << setprecision(3) << negativeAverage << endl;
            cout << " " << endl;
            cout << "And the Average of all the numbers is: " << totalAverage << endl;
            cout << setfill('-') << setw(65) << " " << endl;
      
        break;
       }
        case 4:
 /* QUESTION 4: Write a program that allows the user to input in the following 
  * order: his or her weight in pounds, height in inches, age in years, and the
  * character M for male and F for female. The program should then output the 
  * number of chocolate bars that should be consumed to maintain one’s weight 
  * for the appropriate sex of the specified weight, height, and age. 
  * Allow the user to do this as many times as they wish.
  */
        {
          // Wrap entire code in do-while loop for user repeat
          string chocolateCodeInput;
          do
            {
    // Decide what Data I am storing, and assign data types for variables
            double weight = 0, height = 0, age = 0;
            string gender;
            double bmr;
            double chocolate = 0;
          
    //Ask the User to Input Values for Variables
            cout << "Welcome to the Chocolate Calculator!" << endl;
            cout << setfill('-') << setw(65) << " " << endl;
            cout << "Please Enter Your Weight In Pounds: ";
            cin >> weight;
            cout << "Thank you! (I'll be discreet with that information)" << endl;
            cout << "Please Enter Your Height In Inches: ";
            cin >> height;
            cout << "I appreciate that! How old are you?: ";
            cin >> age;
            cout << "Thank you! Lastly, Were You Born a Male or Female?: ";
            cin >> gender;
                  
    //Create Function to achieve desired results.
            gender = tolower(gender[0]);
                if (gender == "m")
                {
                bmr = 66 + (6.3 * weight) + (12.9 * height) - (6.8 * age);
                chocolate = bmr/230;
                }
                else if (gender == "f")
                {
                bmr = 655 + (4.3 * weight) + (4.7 * height) - (4.7 * age);
                chocolate = bmr/230;
                }
                else
                {
                  cout << "You Have Entered Something Invalid! Apologies!" << endl;
                  return 0;
                }
    //Output the information
            cout << "Thank You For Providing Your Information! If you were ever" << endl;
            cout << "Wondering How Many Chocolate Bars You Could Eat A Day," << endl;
            cout << "Wonder No More! For You Can Eat " << chocolate << " Bars " << endl;
            cout << "A Day To Maintain Your Basal Metabolic Rate!" << endl;
            cout << "(these statements are not to be considered health advice," << endl;
            cout << "please consult your dietitian)" << endl;
            cout << setw(65) << " " << endl;
            cout << "Would you Like to Run the Chocolate Calculator again Y/N?" << endl;
            cin >> chocolateCodeInput;
            }
            while (tolower(chocolateCodeInput[0]) == 'y');
        break;
        }
        case 5:
/* QUESTION 5: Write a program that calculates the total grade for N classroom 
 * exercises as a percentage. The user should input the value for N followed
 * by each of the N scores and totals. Calculate the overall percentage 
 * (sum of the total points earned divided by the total points possible) and 
 * output it as a percentage.
 */
        {
    // Decide what Data I am storing, and assign data types for variables
        double score, totalScore;
        double total, totalTotal;
        double percent;     
        int n = 0;

    //Ask the User to Input Values for Variables
          
        cout << "How many exercises to input? ";
        cin >> n;
        cout << " " << endl;
        for (int i=0; i <n; i++)
        {
            cout << "Score received for exercise " << i+1 << ": ";
            cin >> score;
            totalScore = totalScore + score;
            cout << "Total points possible for exercise " << i+1 << ": ";
            cin >> total;
            totalTotal = totalTotal + total;
            cout << " " << endl;
        }
    //Create Function to achieve desired results
        
        percent = (totalScore / totalTotal)*100;
        
    //Output the Results
        
        cout << "Your total is " << totalScore << " out of " << totalTotal
                << ", or " << setprecision(2) << fixed << percent << "%." << endl;
        break;
        }
        case 6: case 7:
/* QUESTION 6: Write a program to score the rock-paper-scissor game. Each of two
 * users types in either P, R, or S. The program then announces the winner as 
 * well as the basis for determining the winner:
 */
            {
              // QUESTION 7: Allow the User to repeat the game.
              // Achieved by wrapping entire case in a do-while loop.
            do
                {
             // Decide what Data I am storing, and assign data types for variables
                string playerOne;
                string playerTwo;
                
             //Ask the User to Input Values for Variables
                cout << "Welcome to Rock, Paper, Scissors!" << endl; 
                cout << "Two Players Will Each Choose Rock, Paper or Scissors." << endl; 
                cout << "Player 1 Will Go First" << endl;
                cout << "Player 1, Please Choose Your Weapon!!! (Rock/Paper/Scissors): ";
                cin >> playerOne;
                playerOne = (tolower(playerOne[0]));
                cout << "Thank you! Player 2, Please Choose Your Weapon!!! (Rock/Paper/Scissors): ";
                cin >> playerTwo;
                playerTwo = (tolower(playerTwo[0]));
            //Create Function to achieve desired results    
    		if (playerOne == ("s") && playerTwo == ("p"))        
                    {
                            cout << "Scissors cut paper, Player 1 Wins" << endl;
                    }
                    else if (playerOne == ("p") && playerTwo == ("r"))
                    {
                            cout << "Paper covers rock, Player 1 Wins" << endl;
                    }
                    else if (playerOne == ("r") && playerTwo == ("s"))
                    {
                            cout << "Rock breaks scissors, Player 1 Wins" << endl;
                    }
                    else if (playerOne == ("p") && playerTwo == ("s"))
                    {
                            cout << "Scissors cut paper, Player 2 Wins" << endl;
                    }
                    else if (playerOne == ("r") && playerTwo == ("p"))
                    {
                            cout << "Paper covers rock, Player 2 Wins" << endl;
                    }
                    else if (playerOne == ("s") && playerTwo == ("r"))
                    {
                            cout << "Rock breaks scissors, Player 2 Wins" << endl;
                    }
                    else if (playerOne == playerTwo)
                    {
                            cout << "It's a Draw! Nobody Wins." << endl;
                    }
                    else
                    {
                            cout << "ERROR! A PLAYER HAS ENTERED AN INVALID CHARACTER!!" << endl;
                            cout << "PLEASE TRY AGAIN" << endl;
                    }
                    cout << setfill('-') << setw(65) << " " << endl;
                    cout << "Do you want to play again? Y/N : " << endl;
                    cin >> repeatGameInput;
                }
            while (tolower(repeatGameInput[0]) == 'y');
            cout << "Thank you for playing!" << endl;
            break;
            }
        default:
        cout << "Sorry Friend, That's not a Valid Entry!" << endl;
        return 0;
        }
        cout << setfill('-') << setw(65) << " " << endl;
        cout << "Would you Like to Return To the Main Menu? Y/N?" << endl;
        cin >> doWhileMenuInput;
    }
  while (tolower(doWhileMenuInput[0]) == 'y');
  cout << "Have a Wonderful Day!" << endl;
  return 0;
}
