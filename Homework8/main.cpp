/*
 * Name: Marc Fleming
 * Student ID: 2858623
 * Date: 04/28/2022
 * HW: Homework 8
 * Problem: 8 Questions
 * I certify this is my own work and code
 */

#include <windows.h>
#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

int menuFunction();
void output(const vector<int>& v);
void output(const int a[], int size);
void output(const char c[], int size);
void removal(vector<int>& v, int deleteLocation);
int removal(int a[], int size, int deleteLocation);
void deleteNum(vector<int>& v, int deleteValue);
void deleteNum(int a[], int& size, int deleteValue);
bool findNum(vector<int>& v, int findValue);
void randChar(char c[], int size);
void delete_repeats(char c[], int& size);
void problem1();
void problem2();
void problem3And4();
void problem5();
void problem6();
void problem7();
void problem8();

int main(int argc, char** argv)
{
    string doWhileMenuInput;
    srand(time(0)); // SEED
    do
    {
        int menuOption;
        menuOption = menuFunction();
        switch(menuOption)
        {
            case 1:
                problem1();
                break;

            case 2:
                problem2();
                break;

            case 3: case 4:
                problem3And4();
                break;

            case 5:
                problem5();
                break;

            case 6:
                problem6();
                break;
                
            case 7:
                problem7();
                break;
                
            case 8:
                problem8();
                break;
                
            default:
            {
                cout << "You Selected an Invalid Response!" << endl;
            }
        }
        cout << setfill('-') << setw(65) << " " << endl;
        cout << "Would You Like to Return To the Main Menu? Y/N?" << endl;
        cin >> doWhileMenuInput;
    }
    while(tolower(doWhileMenuInput[0]) == 'y');
    cout << "Have a Wonderful Day!" << endl;
    return 0;
}
//END OF MAIN


/**
 * Function created for the menu. Prompts the user to select which assignment they
 * would like to run.
 * @return the user's option
 */
int menuFunction()
{
    int menuOption;
    //Here I am creating the Menu
    cout << "Welcome to the Homework Menu!" << endl;
    cout << "Please Select From the Following Options:" << endl;
    cout << setfill('-') << setw(71) << " " << endl;
    cout << setfill(' ') << "(1): Delete From a Vector"
            << setw(36) << "(2): Delete From an Array" << endl;
    cout << "(3-4): Delete From a Vector Pt 2!" << setw(36) << "(5): Delete From an Array Pt 2!!!" << endl;
    cout << "(6): Does the Vector Have the Value?" << "(7): Enter Data!" << endl;
    cout << "(8): Character Check" << endl;
    cin >> menuOption;

    return menuOption;
}

/**
 * Function to Read from a vector. Runs a loop, iterates through the vector to
 * the end of it.
 * @param v the vector
 */
void output(const vector<int>& v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << " ";
    }
    cout << endl;
}

/**
 * Function to Remove the location from a vector. Unordered removal, vector sets 
 * the delete location value to the last space of the vector, then pop pack the 
 * last space
 * @param v vector
 * @param deleteLocation the location the user wanted to delete
 */
void removal(vector<int>& v, int deleteLocation)
{
    int temp;
    // Error Checking on the Vector
    if(deleteLocation > v.size() || deleteLocation < 0)
    {
        cout << "That Is Outside the Parameters of This Specific Vector!" << endl;
    }
    else
    {
        // May you please explain the swap function? I could not get it to work
        temp = v[deleteLocation];
        v[deleteLocation] = v[v.size() - 1];
        v[v.size() - 1] = temp;
        // Pop back the last value
        v.pop_back();
    }

}

/** 
 * Function to Read from an array. Runs a loop, iterates through the vector to
 * the end of it.
 * @param a the array
 * @param size the size of the array
 */
void output(const int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}

/**
 * Function to read from a character array string. Run a loop, outputting each 
 * element in the array to the end of the array.
 * @param c character array
 * @param size the size of the array
 */
void output(const char c[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << c[i] << " ";
    }
    cout << endl;
}

/**
 * Function to remove a location from an array. Creates a temp array with every 
 * value but the one the user wanted to delete, the reassigns the array back to
 * the main array.
 * @param a the array
 * @param size the size of the array
 * @param deleteLocation the location the user wanted to delete
 */
int removal(int a[], int size, int deleteLocation)
{
    int arraySize = 0;
    int temp;
    // Create a temp array with every value but the one the user wanted to delete
    for(int i = 0; i < size; i++)
    {
        if(i != deleteLocation)
        {
            a[arraySize] = a[i];
            arraySize++;
        }
    }
    return arraySize;
}

/**
 * Function to delete a specific number that the user has chosen. Function will 
 * loop, storing every value but that deleted value. 
 * @param v the vector
 * @param size the size of the vector
 * @param deleteValue the value the user wants to delete
 * @return the new size of the vector
 */
void deleteNum(vector<int>& v, int deleteValue)
{
    vector<int> numbers;
    int match = 0;
    int newSize = 0;
    // Check if the vector has the value
    for(int i = 0; i < v.size(); i++)
    {
        // Store all values BUT the value chosen by the user in a temp vector.
        if(deleteValue != v[i])
        {
            numbers.push_back(v[i]);
            newSize++;
        }
        else // If the loop finds a match
        {
            match++;
        }
    }
    if(match == 0)
    {
        cout << "There Are No Values in the Vector Matching That Value!!!" << endl;
        cout << "Vector Will Remain The Same..." << endl;
    }
    else
    {
        // Reassign the temp vector back to main
        for(int i = 0; i < newSize; i++)
        {
            v[i] = numbers[i];
        }
        // delete the last values for each time loop found a match
        for(int i = 0; i < match; i++)
        {
            v.pop_back();
        }
    }
}

/**
 * Function to delete a number from an array. No longer using temp, Function will
 * run a loop checking array if it matches the value the user wanted to delete.
 * if not, will assign the position of array back to itself, then increase size, 
 * then reiterate.
 * @param a the array
 * @param size the size of the array
 * @param deleteValue the value the user wants to delete
 * @return the new size of the array
 */
void deleteNum(int a[], int& size, int deleteValue)
{
    int match = 0;
    int newSize = 0;
    // Check if the array has the value
    for(int i = 0; i < size; i++)
    {
        // Store all values BUT the value chosen by the user in a Array.
        if(deleteValue != a[i])
        {
            a[newSize] = a[i];
            newSize++;
        }
        else // If the loop finds a match
        {
            match++;
        }
    }
    size = newSize;
}

/**
 * Function to check if a number is inside the vector or not. Bool function, will
 * return true if value is found in the vector, false if not,
 * @param v the vector
 * @param size the size of the vector
 * @param findValue the value the user wants to check for
 * @return false if there are no matches, true if not
 */
bool findNum(vector<int>& v, int findValue)
{
    int match = 0;
    // Check if the vector has the value
    for(int i = 0; i < v.size(); i++)
    {
        // Check if the number is inside the vector
        if(findValue == v[i])
        {
            match++;
        }
    }
    if(match == 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

/**
 * Function to create a randomized array in the size the user has entered
 * @param c character array
 * @param size the size of the array
 */
void randChar(char c[], int size)
{
    for(int i = 0; i < size; i++)
    {
        int randNum = (rand() % 26) + 1;
        if(randNum == 1)
        {
            c[i] = 'a';
        }
        else if(randNum == 2)
        {
            c[i] = 'b';
        }
        else if(randNum == 3)
        {
            c[i] = 'c';
        }
        else if(randNum == 4)
        {
            c[i] = 'd';
        }
        else if(randNum == 5)
        {
            c[i] = 'e';
        }
        else if(randNum == 6)
        {
            c[i] = 'f';
        }
        else if(randNum == 7)
        {
            c[i] = 'g';
        }
        else if(randNum == 8)
        {
            c[i] = 'h';
        }
        else if(randNum == 9)
        {
            c[i] = 'i';
        }
        else if(randNum == 10)
        {
            c[i] = 'j';
        }
        else if(randNum == 11)
        {
            c[i] = 'k';
        }
        else if(randNum == 12)
        {
            c[i] = 'l';
        }
        else if(randNum == 13)
        {
            c[i] = 'm';
        }
        else if(randNum == 14)
        {
            c[i] = 'n';
        }
        else if(randNum == 15)
        {
            c[i] = 'o';
        }
        else if(randNum == 16)
        {
            c[i] = 'p';
        }
        else if(randNum == 17)
        {
            c[i] = 'q';
        }
        else if(randNum == 18)
        {
            c[i] = 'r';
        }
        else if(randNum == 19)
        {
            c[i] = 's';
        }
        else if(randNum == 20)
        {
            c[i] = 't';
        }
        else if(randNum == 21)
        {
            c[i] = 'u';
        }
        else if(randNum == 22)
        {
            c[i] = 'v';
        }
        else if(randNum == 23)
        {
            c[i] = 'w';
        }
        else if(randNum == 24)
        {
            c[i] = 'x';
        }
        else if(randNum == 25)
        {
            c[i] = 'y';
        }
        else
        {
            c[i] = 'z';
        }
    }
}

/**
 * Function to delete repeats in a character array, runs a loop checking each character
 * in the array to the character the user has selected. new sizes the array with every
 * element but the elements chosen by the user
 * @param c character array
 * @param size size of the array
 * @param deleteRepeat the character the user has decided to delete
 * @return the new size of the array
 */
void delete_repeats(char c[], int& size)
{
    int match = 0;

    for(int i = 0; i < size; i++)
    {
        // Create new Index
        for(int newIndex = i + 1; newIndex < size; newIndex++)
        {
            // Check for matches in the array
            if(c[i] == c[newIndex])
            {
                match++;
                size--;
                // Move the matches further out of the array so it will not be displayed
                for(int j = newIndex; j < size; j++)
                {
                    c[j] = c[j + 1];
                }
                newIndex--;
            }
        }
    }
    if(match == 0)
    {
        cout << "There Are No Characters in the Array That Match!!!" << endl;
        cout << "Array Will Remain The Same..." << endl;
    }
}

void problem1()
{
    string menuInput;
    do
    {
        vector<int> numbers;
        int size, deleteLocation;
        string modifyInput;

        /* QUESTION 1: Write a function that will delete a number from 
         * a vector from a given location. The function should have a 
         * vector and the location as parameters.
         */

        // Ask the User how large they would like the vector to be               
        cout << "How Many Values Would You Like The Vector To Hold?: " << endl;
        cin >> size;
        cout << endl;

        // Store the values inside a vector
        for(int i = 0; i < size; i++)
        {
            int randNum = (rand() % 100) + 1;
            numbers.push_back(randNum);
        }

        cout << "Vector is:" << endl;
        output(numbers);

        cout << "Would You Like To Modify the Vector? (Y/N)" << endl;

        cin >> modifyInput;
        while(tolower(modifyInput[0]) == 'y')
        {
            cout << "C++ Starts at Zero Location" << endl;
            cout << "What location would you like to delete?:" << endl;
            cin >> deleteLocation;

            removal(numbers, deleteLocation);

            cout << "New Vector is:" << endl;
            output(numbers);
            cout << "Would You Like To Try to Modify the Vector Again? (Y/N)" << endl;
            cin >> modifyInput;
        }
        cout << setfill('-') << setw(65) << " " << endl;
        cout << "Would You Like To Repeat This Assignment? Y/N : " << endl;
        cin >> menuInput;
    }
    while(tolower(menuInput[0]) == 'y');
    cout << "Come Back Soon!" << endl;
}

void problem2()
{
    string menuInput;
    do
    {
        /* QUESTION 2: Repeat problem 1, but with an array. 
         * Remember that arrays need to have the size as a parameter.
         */
        int size;
        int deleteLocation;
        string modifyInput;

        // Ask the User how large they would like the array to be               
        cout << "How Many Values Would You Like The Array To Hold?: " << endl;
        cin >> size;
        cout << endl;

        int numbers[size];

        // Store the values inside a vector
        for(int i = 0; i < size; i++)
        {
            int randNum = (rand() % 100) + 1;
            numbers[i] = randNum;
        }

        cout << "Array is:" << endl;
        output(numbers, size);

        // Ask the user if they would like to modify the array
        cout << "Would You Like To Modify the Array? (Y/N)" << endl;

        cin >> modifyInput;
        while(tolower(modifyInput[0]) == 'y')
        {
            cout << "C++ Starts at Zero Location" << endl;
            cout << "What location would you like to delete?:" << endl;
            cin >> deleteLocation;

            // Error Checking on the Array
            if(deleteLocation >= size || deleteLocation < 0)
            {
                cout << "That Is Outside the Parameters of This Specific Array!" << endl;
            }
            else
            {
                size = removal(numbers, size, deleteLocation);
                cout << "New Array is:" << endl;
                output(numbers, size);
            }
            cout << "Would You Like To Try to Modify the Array Again? (Y/N)" << endl;
            cin >> modifyInput;
        }
        cout << " " << endl;
        cout << setfill('-') << setw(65) << " " << endl;
        cout << "Would You Like To Repeat This Assignment? Y/N : " << endl;
        cin >> menuInput;
    }
    while(tolower(menuInput[0]) == 'y');
    cout << "Come Back Soon!" << endl;
}

void problem3And4()
{
    string menuInput;
    do
    {
        vector<int> numbers;
        int size, deleteValue;
        string modifyInput;

        /* QUESTION 3: Write a function that deletes a number given 
         * by the user from a vector, but keeps the order of the elements 
         * the same. For example, if the vector contains the 
         * values [1 3 6 5 7 4], and I delete the value 6, the vector
         * will become [1 3 5 7 4].
         */

        // Ask the User how large they would like the vector to be               
        cout << "How Many Values Would You Like The Vector To Hold?: " << endl;
        cin >> size;
        cout << endl;

        // Store the values inside a vector
        for(int i = 0; i < size; i++)
        {
            int randNum = (rand() % 100) + 1;
            numbers.push_back(randNum);
        }

        cout << "Vector is:" << endl;
        output(numbers);

        // Ask the user if they would like to modify the Vector
        cout << "Would You Like To Modify the Vector? (Y/N)" << endl;

        cin >> modifyInput;
        while(tolower(modifyInput[0]) == 'y')
        {
            cout << "What value would you like to delete?:" << endl;
            cin >> deleteValue;

            deleteNum(numbers, deleteValue);

            cout << "New Vector is:" << endl;
            output(numbers);

            cout << "Would You Like To Try to Modify the Vector Again? (Y/N)" << endl;
            cin >> modifyInput;
        }

        cout << setfill('-') << setw(65) << " " << endl;
        cout << "Would You Like To Repeat This Assignment? Y/N : " << endl;
        cin >> menuInput;
    }
    while(tolower(menuInput[0]) == 'y');
    cout << "Come Back Soon!" << endl;
}

void problem5()
{
    string menuInput;
    do
    {
        int size, deleteValue;
        string modifyInput;

        /* Question 5: Repeat number 3, but with an array. Remember the 
         * array needs a variable to hold its size as the array changes value.
         */

        // Ask the User how large they would like the array to be               
        cout << "How Many Values Would You Like The Array To Hold?: " << endl;
        cin >> size;
        cout << endl;
        int numbers[size];

        // Store the values inside an array
        for(int i = 0; i < size; i++)
        {
            int randNum = (rand() % 100) + 1;
            numbers[i] = randNum;
        }

        cout << "Array is:" << endl;
        output(numbers, size);

        // Ask the user if they would like to modify the array
        cout << "Would You Like To Modify the Array? (Y/N)" << endl;

        cin >> modifyInput;
        while(tolower(modifyInput[0]) == 'y')
        {
            cout << "What value would you like to delete?:" << endl;
            cin >> deleteValue;

            deleteNum(numbers, size, deleteValue);

            cout << "New Array is:" << endl;
            output(numbers, size);

            cout << "Would You Like To Try to Modify the Array Again? (Y/N)" << endl;
            cin >> modifyInput;
        }

        cout << setfill('-') << setw(65) << " " << endl;
        cout << "Would You Like To Repeat This Assignment? Y/N : " << endl;
        cin >> menuInput;
    }
    while(tolower(menuInput[0]) == 'y');
    cout << "Come Back Soon!" << endl;
}

void problem6()
{
    string menuInput;
    do
    {
        vector<int> numbers;
        int size, findValue;

        /* QUESTION 6: Write a function that determines if a number is already 
         * contained inside the vector. Create a driver that tests if the 
         * function works properly.
         */

        // Ask the User how large they would like the vector to be               
        cout << "How Many Values Would You Like The Vector To Hold?: " << endl;
        cin >> size;
        cout << endl;

        // Store the values inside a vector
        for(int i = 0; i < size; i++)
        {
            int randNum = (rand() % 100) + 1;
            numbers.push_back(randNum);
        }

        cout << "Vector is:" << endl;
        output(numbers);

        // Ask the user what value they would like to check
        cout << "What Value would you like me to check?" << endl;

        cin >> findValue;

        if(findNum(numbers, findValue))
        {
            cout << "That Number Is In the Vector!!!" << endl;
        }
        else
        {
            cout << "That Number Is NOT In the Vector!!!" << endl;
        }

        cout << setfill('-') << setw(65) << " " << endl;
        cout << "Would You Like To Repeat This Assignment? Y/N : " << endl;
        cin >> menuInput;
    }
    while(tolower(menuInput[0]) == 'y');
    cout << "Come Back Soon!" << endl;
}

void problem7()
{
    /* QUESTION 7: Using the function from number 6, create a program
     * that allows the user to continually enter data into a vector. 
     * If the number is already inside the vector, inform the user 
     * that the value cannot be inserted because it is already being used.
     */
    string repeatInput;
    vector<int> numbers;
    int addValue;

    cout << "Vector Is Currently Empty." << endl;
    do
    {
        cout << "What Value Would You Like To Add?" << endl;
        cin >> addValue;
        cout << "\n";
        if(findNum(numbers, addValue))
        {
            cout << "That Value Is Already Inside the Vector!" << endl;
            cout << "It Cannot Be Used" << endl;
        }
        else // If there isn't a match then the program should insert the value
        {
            numbers.push_back(addValue);

        }
        cout << "Vector Is:" << endl;
        for(int i = 0; i < numbers.size(); i++)
        {
            cout << numbers[i] << " ";
        }
        cout << endl;
        cout << setfill('-') << setw(65) << " " << endl;
        cout << "Would You Like to Insert A Different Value? Y/N?" << endl;
        cin >> repeatInput;
    }
    while(tolower(repeatInput[0]) == 'y');
    cout << "Have a Wonderful Day!" << endl;
}

void problem8()
{
    string menuInput;
    do
    {
        int size;

        /* QUESTION 8: See HW page
         */

        // Ask the User how large they would like the array to be               
        cout << "How Many Characters Would You Like The Array To Hold?: " << endl;
        cin >> size;
        cout << endl;
        char letters[size];

        // Store the values inside an array
        randChar(letters, size);

        cout << "Array is:" << endl;
        output(letters, size);

        cout << "Delete Repeats Function Will Now Be Called..." << endl;

        delete_repeats(letters, size);

        cout << "New Array is:" << endl;
        output(letters, size);

        cout << setfill('-') << setw(65) << " " << endl;
        cout << "Would You Like To Repeat This Assignment? Y/N : " << endl;
        cin >> menuInput;
    }
    while(tolower(menuInput[0]) == 'y');
    cout << "Come Back Soon!" << endl;
}