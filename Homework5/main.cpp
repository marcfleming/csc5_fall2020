/*
* Name: Marc Fleming
* Student ID: 2858623
* Date: 03/28/2022
* HW: HOMEWORK 5
* Problem: 6 QUESTIONS
* I certify this is my own work and code
*/

/* 
 * File:   main.cpp
 * Author: expec
 *
 * Created on March 28, 2022, 11:51 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <windows.h>
#include <cmath>


using namespace std;
//Globally defined constant
const double litersPerGallon = 0.264179;

const double kilograms = 2.2046; // per Pound
const double grams = 1000; // per KiloGram
const double ounces = 16; // per Pound
/*
 * FUNCTIONS LISTED HERE.
 */

/**
 * Outputs the game of Rock Paper Scissors.
 * @param playerOne can select Rock, Paper, Or Scissors.
 * @param playerTwo can select Rock, Paper, Or Scissors.
 * Void Function, Outputs the winner of the game inside the function.
 */
void userRockPaper(string playerOne, string playerTwo);

/**
 * Outputs the game of Rock Paper Scissors
 * @param Parameters are all self contained in the script.
 * The script is an AI vs another AI, no inputs.
 * Void Function, Outputs the winner of the game inside the function
 */
void AIRockPaper();

/**
 * Function that receives the liters and miles from the user, then outputs 
 * the Miles Per Gallon
 * @param liters is the liters that the vehicle used
 * @param miles is how many miles the vehicle drove
 * @return the miles per gallon to the user
 */
int mpgFunction(double liters, double miles);

/**
 * Function to show the user how many customers are in the store, and how many
 * scoops per customer
 * @param scoops of ice cream that each custom wants
 * @param customers that came in to the store
 * @return the weight of ice cream that is needed to serve the customers
 */
double iceCreamStoreFunction(double scoops, double customers);

// I learned you can use the '&' to address variables between functions in the book. I don't know why this wasn't shown in class but
// I feel that it's a very useful tool!

/**
 * Function to receive the weight in pounds and ounces from the user. Prompts the user, then saves the values to be used
 * in another function
 * @param poundsWeight user enters weight in pounds
 * @param ouncesWeight user enters weight in ounces
 * Void Function, Self contained, variables are addressed with '&'
 */
void weightInputFunction(double &poundsWeight, double &ouncesWeight);

/**
 * Function to Calculate the weight from pounds and ounces to kilograms and grams. This was a tricky one, I used the book 
 * to find math functions like remainder(). Works super well. Takes the decimal of kilograms and removes it to be used for the 
 * grams, then floor removes the decimal remainder again completely.
 * @param poundsWeight Received from weightInputFunction
 * @param ouncesWeight Received from weightInputFunction
 * @param totalWeightPounds The total weight of pounds and ounces
 * @param totalWeightKilograms totalWeightPounds converted into Kilograms
 * @param totalWeightGrams removed the remaining decimal from totalWeightKilograms, then converted remainder to grams.
 * Void Function, Self contained, variables are addressed with '&'
 */
void calculateWeight(double poundsWeight, double ouncesWeight, double &totalWeightPounds, double &totalWeightKilograms, double &totalWeightGrams);

/**
 * Function to output the desired results in kilograms and grams
 * @param poundsWeight Included to compare weight
 * @param ouncesWeight Included to compare weight
 * @param kilogramsWeight Displays weight in Kilograms
 * @param gramsWeight Displays weight in Grams
 * Void Function, Self contained, no return.
 */
void outputFunction(double poundsWeight, double ouncesWeight, double totalWeightKilograms, double totalWeightGrams);

int main(int argc, char** argv)
{

    //Wrap the Entire Menu in a Do-While Loop

    string doWhileMenuInput;
    string repeatRockPaperInput;
    string rockPaperMenuInput;
    srand(time(0));

    do
    {

        //Here I am creating the Menu
        int menuOption;
        cout << "Welcome to the Homework Menu!" << endl;
        cout << "Please Select From the Following Options:" << endl;
        cout << setfill('-') << setw(71) << " " << endl;
        cout << setfill(' ') << "(1): Rock, Paper, Scissors"
                << setw(29) << "(2): MPG Calculator" << endl;
        cout << "(3): The Ice Cream Store" << setw(36) << " (4-6): Weight Conversion" << endl;
        cin >> menuOption;

        switch(menuOption)
        {
            case 1:
            {
                /* QUESTION 1: Create functions for the two types of games in 
                 * problem 3, homework 4 (the rock paper scissors). The menu 
                 * based operation should call functions userRockPaper() or 
                 * AIRockPaper() to run the selected program.
                 */
                do
                {
                
                string playerOne, playerTwo;
                int rockPaperOption;
                    cout << "Welcome to the Rock Paper Scissors Menu!" << endl;
                    cout << "Please Select From the Following Options:" << endl;
                    cout << setfill('-') << setw(71) << " " << endl;
                    cout << setfill(' ') << "1) PVP"
                            << setw(37) << "2) CPU vs CPU" << endl;
                    cin >> rockPaperOption;
                    if ( rockPaperOption == 1 )
                    {
                        do
                        {
                            
                            // Calling the Function
                            userRockPaper(playerOne, playerTwo);
                            
                            cout << setfill('-') << setw(65) << " " << endl;
                            cout << "Do you want to play again? Y/N : " << endl;
                            cin >> repeatRockPaperInput;
                        }
                        while ( tolower(repeatRockPaperInput[0]) == 'y' );
                        cout << "Thank you for playing!" << endl;
                    }
                    else if ( rockPaperOption == 2 )
                    {
                        do
                        {
                            
                            // Calling the Function
                            AIRockPaper();
                            
                            cout << setfill('-') << setw(65) << " " << endl;
                            cout << "Do you want to play again? Y/N : " << endl;
                            cin >> repeatRockPaperInput;
                        }
                        while ( tolower(repeatRockPaperInput[0]) == 'y' );
                        cout << "Thank you for playing!" << endl;
                    }
                    else
                    {
                        cout << "Sorry Friend, That's not a Valid Entry!" << endl;
                        break;
                    }
                    cout << setfill('-') << setw(65) << " " << endl;
                    cout << "Would You Like To Return to the Rock Paper Scissors Menu? Y/N : " << endl;
                    cin >> rockPaperMenuInput;
                }
                while ( tolower(rockPaperMenuInput[0]) == 'y' );
                cout << "Come Back Soon!" << endl;
                break;         
            }
            case 2:
            {
            /* QUESTION 4: A liter is 0.264179 gallons. Write a program that will read 
             * in the number of liters of gasoline consumed by the user’s car and the 
             * number of miles traveled by the car. Then, output the number of miles
             * per gallon the car delivered. Your program should allow the user to
             * repeat this calculation as often as the user wishes. Define a function
             * to compute the number of miles per gallon. Your program should use 
             * a globally defined constant for the number of liters per gallon. 
             */
                string mpgRepeatInput;
                do
                {
                    double mpg;
                    double liters;
                    double miles;
                    
                    // Calling the function
                    mpg = mpgFunction(liters, miles);
                    
                    // Output Data
                    cout << "Thank you for your information!" << endl;
                    cout << "Your car gets " << mpg << " Miles Per Gallon." << endl;
                    
                    cout << setfill('-') << setw(65) << " " << endl;
                    cout << "Do You Want to Run the Program Again? Y/N : " << endl;
                    cin >> mpgRepeatInput;
                }

                // Menu Return
                while(tolower(mpgRepeatInput[0]) == 'y');
                cout << "Thank you!" << endl;
                break;
            }
            case 3:
            {
                /* QUESTION 3: Write a function that calculates the portion size
                 * of ice cream a group of customers will receive. The function
                 * should receive the number of customers, and the weight of 
                 * the ice cream. The function should also output the portion 
                 * of each customer.
                 */
                
                // This is a very ambiguous question, I am going to assume we can
                // create our own values for portion sizes and weight.
                double portions;
                double scoops, customers;
                
                portions = iceCreamStoreFunction(scoops, customers);
                
                cout << "Each Customer Is Getting " << portions << " Ounces of Ice Cream Today." << endl;
                cout << "Thank You For Coming To the Ice Cream Store!" << endl;
                cout << "Have a 'Chill' Day!!!" << endl;
                
                break;
            }
            case 4: case 5: case 6:
            {
                /* QUESTION 6: Write a program that reads in a weight in pounds 
                 * and ounces and outputs the equivalent weight in kilograms 
                 * and grams. Use at least three functions: one for input, one 
                 * or more for calculating, and one for output. Include a loop 
                 * that lets the user repeat this computation for new input 
                 * value until the user says he or she wants to end the program.
                 * There are 2.2046 pounds in a kilogram, 1000 grams in a 
                 * kilogram, and 15 ounces in a pound.
                 */

                double poundsWeight, ouncesWeight;
                double totalWeightPounds, totalWeightKilograms, totalWeightGrams;
                string weightInputOption;


                do
                {
                    // Call the Three Functions
                    weightInputFunction(poundsWeight, ouncesWeight);
                    
                    calculateWeight(poundsWeight, ouncesWeight, totalWeightPounds, totalWeightKilograms, totalWeightGrams);
                    
                    outputFunction(poundsWeight, ouncesWeight, totalWeightKilograms, totalWeightGrams);

                    // Repeat the Functions as many times as the User would like.
                    cout << "Would You Like to Input A New Weight?: " << endl;
                    cin >> weightInputOption;
                    cout << endl;
                }
                while(tolower(weightInputOption[0]) == 'y');
                break;
            }
            default:
            {
                cout << "Sorry You didn't pick the right value" << endl;
            }
        }
        cout << setfill('-') << setw(65) << " " << endl;
        cout << "Would You Like to Return To the Main Menu? Y/N?" << endl;
        cin >> doWhileMenuInput;
    }
    while(tolower(doWhileMenuInput[0]) == 'y');
    cout << "Have a Wonderful Day!" << endl;
    return 0;
}

void userRockPaper(string playerOne, string playerTwo)
{

    //Ask the User to Input Values for Variables

    cout << "Welcome to Rock, Paper, Scissors!" << endl;
    cout << "Two Players Will Each Choose Rock, Paper or Scissors." << endl;
    cout << "Player 1 Will Go First" << endl;
    cout << "Player 1, Please Choose Your Weapon!!! (Rock/Paper/Scissors): ";
    cin >> playerOne;
    playerOne = (tolower(playerOne[0]));
    cout << "Thank you! Player 2, Please Choose Your Weapon!!! (Rock/Paper/Scissors): ";
    cin >> playerTwo;
    playerTwo = (tolower(playerTwo[0]));

    //Create Function to achieve desired results    

    if(playerOne == ("s") && playerTwo == ("p"))
    {
        cout << "Scissors cut paper, Player 1 Wins" << endl;
    }
    else if(playerOne == ("p") && playerTwo == ("r"))
    {
        cout << "Paper covers rock, Player 1 Wins" << endl;
    }
    else if(playerOne == ("r") && playerTwo == ("s"))
    {
        cout << "Rock breaks scissors, Player 1 Wins" << endl;
    }
    else if(playerOne == ("p") && playerTwo == ("s"))
    {
        cout << "Scissors cut paper, Player 2 Wins" << endl;
    }
    else if(playerOne == ("r") && playerTwo == ("p"))
    {
        cout << "Paper covers rock, Player 2 Wins" << endl;
    }
    else if(playerOne == ("s") && playerTwo == ("r"))
    {
        cout << "Rock breaks scissors, Player 2 Wins" << endl;
    }
    else if(playerOne == playerTwo)
    {
        cout << "It's a Draw! Nobody Wins." << endl;
    }
    else
    {
        cout << "ERROR! A PLAYER HAS ENTERED AN INVALID CHARACTER!!" << endl;
        cout << "PLEASE TRY AGAIN" << endl;
    }
  
}

void AIRockPaper()
{
    
    // Decide what Data I am storing, and assign data types for variables

    string weaponOne;
    string weaponTwo;
    int ai;
    int cpuOne = 0, cpuTwo = 0;

    //Ask the User to Input Values for Variables

    cout << "Welcome to Rock, Paper, Scissors!" << endl;
    cout << "Two CPUs Will Each Choose Rock, Paper or Scissors." << endl;
    cout << "CPU 1 Will Go First" << endl;
    cout << "CPU 1, Please Choose Your Weapon!!! (Rock/Paper/Scissors): " << endl;
    ai = (rand() % 3) + 1;
    cpuOne = ai;
    if(cpuOne == 1)
    {
        weaponOne = "Rock";
    }
    else if(cpuOne == 2)
    {
        weaponOne = "Paper";
    }
    else if(cpuOne == 3)
    {
        weaponOne = "Scissors";
    }
    ai = 0;
    // Sleep(2000); I removed sleep. Could You explain why I can't use it? It helps slow down
    // The system so the user can read what the CPUs are choosing.
    cout << "Computer One Chose " << weaponOne << endl;
    weaponOne = (tolower(weaponOne[0]));
    cout << "Thank you! CPU 2, Please Choose Your Weapon!!! (Rock/Paper/Scissors): " << endl;
    ai = (rand() % 3) + 1;
    cpuTwo = ai;
    if(cpuTwo == 1)
    {
        weaponTwo = "Rock";
    }
    else if(cpuTwo == 2)
    {
        weaponTwo = "Paper";
    }
    else if(cpuTwo == 3)
    {
        weaponTwo = "Scissors";
    }
    ai = 0;
    //Sleep(2000);
    cout << "Computer Two Chose " << weaponTwo << endl;
    weaponTwo = (tolower(weaponTwo[0]));

    //Create Function to achieve desired results    

    if(weaponOne == ("s") && weaponTwo == ("p"))
    {
        cout << "Scissors cut paper, CPU 1 Wins" << endl;
    }
    else if(weaponOne == ("p") && weaponTwo == ("r"))
    {
        cout << "Paper covers rock, CPU 1 Wins" << endl;
    }
    else if(weaponOne == ("r") && weaponTwo == ("s"))
    {
        cout << "Rock breaks scissors, CPU 1 Wins" << endl;
    }
    else if(weaponOne == ("p") && weaponTwo == ("s"))
    {
        cout << "Scissors cut paper, CPU 2 Wins" << endl;
    }
    else if(weaponOne == ("r") && weaponTwo == ("p"))
    {
        cout << "Paper covers rock, CPU 2 Wins" << endl;
    }
    else if(weaponOne == ("s") && weaponTwo == ("r"))
    {
        cout << "Rock breaks scissors, CPU 2 Wins" << endl;
    }
    else if(weaponOne == weaponTwo)
    {
        cout << "It's a Draw! Nobody Wins." << endl;
    }
    else
    {
        cout << "ERROR! A CPU HAS ENTERED AN INVALID CHARACTER!!" << endl;
        cout << "PLEASE TRY AGAIN" << endl;
    }    
}

int mpgFunction(double liters, double miles)
{
    double gallons, mpg;

    cout << "Welcome to the MPG Calculator!" << endl;
    cout << "Here, you will input the Liters of gas you used, and the Miles" << endl;
    cout << "you drove. Then I will show you your vehicle's MPG." << endl;
    cout << "Please enter how many liters you used: " << endl;
    cin >> liters;
    cout << " " << endl;
    cout << "Thank you. How far did you drive? Please enter your information" << endl;
    cout << "in miles: " << endl;
    cin >> miles;
    cout << " " << endl;

    // Create Function to achieve desired results
    
    gallons = liters * litersPerGallon;
    mpg = miles / gallons;

    return mpg;
}

double iceCreamStoreFunction(double scoops, double customers)
{
    // Initialize Variables

    // A scoop of ice cream is approx 68 grams. My Ice Cream Store does
    // not skimp out on their customers, so our ice cream scoops will 
    // be 100 grams each. This is approx 3.5274 ounces.
    const double iceCreamWeight = 3.5274;

    double portions, totalWeight, weightInLBS;

    cout << "Hello and Welcome to The Ice Cream Store!" << endl;
    cout << "How Many People Are In Your Party Today?" << endl;
    cin >> customers;
    cout << "We Can Handle " << customers << " Customers!" << endl;
    cout << "How Many Scoops of Ice Cream Per Person?" << endl;
    cin >> scoops;
    cout << "Well Now That's a Lot of Ice Cream!" << endl;
    cout << "Let Me Check The Back." << endl;
    Sleep(3000);

    // Create Function to Achieve Desired Results               
    totalWeight = customers * (scoops * iceCreamWeight);
    weightInLBS = totalWeight / 16;
    portions = totalWeight / customers;

    // Output Data
    cout << "You Are Getting " << fixed << setprecision(2) << totalWeight
            << " Ounces of Ice Cream Today!" << endl;
    cout << "That's " << weightInLBS << " Pounds!" << endl;
    
    return portions;
    }

void weightInputFunction(double &poundsWeight, double &ouncesWeight)
{
    cout << "Enter Weight In Pounds: " << endl;
    cin >> poundsWeight;
    // Make The user Unable to enter a negative number
    while(poundsWeight < 0)
    {
        cout << "Weight Can't Be Less Than Zero!" << endl;
        cout << "Please Enter Again:" << endl;
        cin >> poundsWeight;
    }
    cout << "Enter ounces weight: ";
    cin >> ouncesWeight;
    while(ouncesWeight < 0)
    {
        cout << "Weight Can't Be Less Than Zero!" << endl;
        cout << "Please Enter Again:" << endl;
        cin >> ouncesWeight;
    }
    cout << " " << endl;
}

void calculateWeight(double poundsWeight, double ouncesWeight, double &totalWeightPounds, double &totalWeightKilograms, double &totalWeightGrams)
{
    totalWeightPounds = poundsWeight + (ouncesWeight / ounces);
    totalWeightKilograms = totalWeightPounds / kilograms;
    totalWeightGrams = remainder(totalWeightKilograms, 1);
    totalWeightKilograms = floor(totalWeightKilograms);
    totalWeightGrams = totalWeightGrams * grams;
}

void outputFunction(double poundsWeight, double ouncesWeight, double totalWeightKilograms, double totalWeightGrams)
{
    cout << "Your Weight Was " << poundsWeight << " Pounds And " << ouncesWeight << " Oz." << endl;
    cout << "This Is Equivalent To " << totalWeightKilograms << " Kilograms And " << totalWeightGrams << " Grams." << endl;
}