/*
* Name: Marc Fleming
* Student ID: 2858623
* Date: 3/5/2022
* HW: Homework 2
* Problem: 4 Questions, one at a time
* I certify this is my own work and code
*/

// QUESTION 1:   a) Give an example of a syntax (compiler) error.
//               b) Give an example of a logic (runtime) error. 

// (a) A syntax error would be like leaving out a semicolon and the build fails
// (b) A logic error would be if the system ran properly, but it divided by
// zero, and the final output was not what it should have been.
// To conclude, syntax normally refers to error in code, whereas logic errors
// refer to incorrect outputs from the program. (I like to think, "Humans have 
// bad logic, we are the ones that make logic errors" ((that was supposed to be
// funny)) )



/* string name1 = "Calvin";
   string name2 = "Bill";
  
  cout << "Hello my name is " << name1 << endl;
  cout << "Hi there " << name1 << "My name is " << name2 << endl;
  cout << "Nice to meet you " << name2 << ".\n"
       << "I am wondering if you're available for what?\n";
  cout << "What? Bye" << name1 << "!\n";
  cout << "Uh... sure, bye " << name2 << "!\n\n";
  
  cout << name1 << name1 << name1 << name1 << name1 << endl;
  cout << name2 << name2 << name2 << name2 << name2 << endl;
  
   return 0;
 */

/* QUESTION 2
 * a) What is the output? (Be Specific) 
 * The output is a strange conversation between Calvin and Bill. There is a
 * lacking space in like 14 and the output would state "ByeCalvin". At the last
 * two Cout statements the output reads Calvin 5 times and Bill 5 times.
 * 
 * b) How Many Statements are there in the main() function?
 * 10 Total Statements. Line 12 and 13 are a single statement, noted by the 
 * semicolon at the end of line 13
 * 
 * c) Identify the code that are variable declaration and initialization
 *      statements.
 * Lines 7 and 8 are variable declaration and initialization statements.  
 * 
 * d) Identify the code that is the program 
 * Wouldn't the entirety of the code be the program? 
 *  It is one singular program correct?
 * 
 * e) Can you figure out one reason how variables are useful in this example?
 * Variables store the values of the names to be used in future outputs. This 
 * is practical when we need to access a directory of names.
 */

/* QUESTION 3
 * Create a program that receives 12 total scores, four for each person, and 
 * calculates the average of scores for each quiz. The program should output
 * all values as indicated below. The averages have only three data points, 
 * and all numbers are right justified to the center of the QUIZ 1 output. 
 */


#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int main(int argc, char** argv)
{
    {
  // Decide my Data I am storing, and assign data types to variables
        double john1 = 10;
        double john2 = 7;
        double john3 = 10;
        double john4 = 9;
    
        double mary1 = 9;
        double mary2 = 8;
        double mary3 = 10;
        double mary4 = 9;
    
        double matthew1 = 8;
        double matthew2 = 7;
        double matthew3 = 9;
        double matthew4 = 10;
    
        double averageQuiz1;
        double averageQuiz2;
        double averageQuiz3;
        double averageQuiz4;
    
  // Create Function to Calculate Data
        averageQuiz1 = (john1 + mary1 + matthew1)/3;
        averageQuiz2 = (john2 + mary2 + matthew2)/3;
        averageQuiz3 = (john3 + mary3 + matthew3)/3;
        averageQuiz4 = (john4 + mary4 + matthew4)/3;
    
  // Display Information on Screen
    
        cout << "name" << setw(14) << "Quiz 1" << setw(10) << "Quiz 2" << setw(10)
            << "Quiz 3" << setw(10) << "Quiz 4" << endl;
        cout << "----" << setw(14) << "------" << setw(10) << "------" << setw(10)
            << "------" << setw(10) << "------" << endl;
        cout << "John" << setw(12) << right << john1 << setw(10) << john2 
            << setw(10) << john3 << setw(10) << john4 << endl;
        cout << "Mary" << setw(12) << right << mary1 << setw(10) << mary2 
            << setw(10) << mary3 << setw(10) << mary4 << endl;
        cout << "Matthew" << setw(9) << right << matthew1 << setw(10) << matthew2 
            << setw(10) << matthew3 << setw(10) << matthew4 << endl;    
        cout << " " << endl; //Is this 'style' correct? Just want to be sure :)
        cout << "Average" << setw(9) << setprecision(3) << averageQuiz1
            << setw(10) << averageQuiz2 << setw(10) << averageQuiz3
            << setw(10) << averageQuiz4 << endl;
            // Does this look visually well enough? I figured tabbing in the 
            // long 'cout' statements would make it more clear to read
        // End of Step Cout
        cout << " " << endl;
    
    }
 
 /* QUESTION 4: Write a program that plays the game of Mad Lib. Your program 
  * should prompt the user to enter the following strings: For simplicity, 
  * each input can only be a max of 10 characters.
  */
    {
   // Decide my Data I am storing, and assign data types to variables
        string name1;
        string name2;
        string food;
        double number;
        string adjective;
        string color;
        string animal;
    
   // Create Interface to Obtain Inputs for Variables Mentioned
        cout << "WELCOME TO THE WILD GAME OF MAD LIB" << endl;
        cout << setfill('-') << setw(50) << " " << endl;
        cout << "Please Enter a Name: " << endl;
        cin >> name1;
        cout << "Thank you!\n" << "Please Enter Another Name: " << endl;
        cin >> name2;
        cout << "Thank you!\n" << "Please Enter a Food Item: " << endl;
        cin >> food;
        cout << "Thank you!\n" << "Please Enter A Number Between 100 and 120: " 
                << endl;
        cin >> number;
            if (number >= 100 && number <= 120)
            {
                    cout << "Thank you! " << endl;
            }   
            else 
            {
                cout << "The number is not between 100 and 120!" << endl;
                cout << "Please start the program again!" << endl;
                return 0;
            }
        cout << "Please Enter an Adjective : " << endl;
        cin >> adjective;
        cout << "Thank you!\n" << "Please Enter a Color: " << endl;
        cin >> color;
        cout << "Almost done!\n" << "Please Enter an Animal: " << endl;
        cin >> animal;
        cout << setfill('-') << setw(50) << " " << endl;
        cout << "Thank you for playing! Here is your Mad Lib:" << endl;
        cout << " " << endl;
    
    //Output the Result on the Screen
    cout << "Dear "<< name2 <<"," << endl;
    cout << " " << endl;
    cout << "I am sorry that I am unable to turn in my homework at this time. "
                << "First, I ate a rotten " << food << "," << endl;
    cout << "which made me turn " << color 
            <<" and extremely ill. I came down with a fever of "
            << number << "." << endl;
    cout << "Next, my "<< adjective << " pet " << animal 
            << " must have smelled the remains of the " << food 
            << " on my " << endl;
    cout << "homework because he ate it. I am currently rewriting my "
            << "homework and hope you will accept" << endl;
    cout << "it late." << endl;
    cout << " " << endl;
    cout << "Sincerely," << endl;
    cout << name1 << endl;

    }
 
  return 0;
}