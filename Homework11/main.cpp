/*
 * Name: Marc Fleming
 * Student ID: 2858623
 * Date: 05/23/2022
 * HW: Homework 11
 * Problem: 5. Questions
 * I certify this is my own work and code
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <vector>


using namespace std;

// ALIAS
typedef char* CharPtr;
typedef int* IntPtr;

// STRUCTURES
struct Animal
{
    string type;
    string name;
    int age;
    
};

// PROTOTYPES
int menuFunction();
string* createArray(int size);
void randArray(int num[], int size);
void output(const string names[], int size);
string* deleteEntry(string *names, int& size, int location);
string* addEntry(string *names, int& size, string name);
CharPtr* create2DArray(int rows, int columns);
IntPtr* create2DNumbers(int rows, int columns);
void createSquare(CharPtr* twoD, int rows, int columns);
void output(CharPtr* twoD, int rows, int columns);
void output(IntPtr* twoD, int rows, int columns);
Animal generateAnimal();
void output(const Animal &p);
void createNumbers(IntPtr* numbers, int rows, int columns);

void problem1();
void problem2();
void problem3();
void problem4();
void problem5();

int main(int argc, char** argv)
{
    string doWhileMenuInput;
    do
    {
        int menuOption;
        menuOption = menuFunction();
        switch(menuOption)
        {
            case 1:
                problem1();
                break;

            case 2:
                problem2();
                break;

            case 3:
                problem3();
                break;
                
            case 4:
                problem4();
                break;
                
            case 5:
                problem5();
                break;
                
            default:
            {
                cout << "You Selected an Invalid Response!" << endl;
            }
        }
        cout << right << setfill('-') << setw(65) << " " << endl;
        cout << "Would You Like to Return To the Main Menu? Y/N?" << endl;
        cin >> doWhileMenuInput;
    }
    while(tolower(doWhileMenuInput[0]) == 'y');
    cout << "Have a Wonderful Day!" << endl;
    return 0;
}
//END OF MAIN



/**
 * Function created for the menu. Prompts the user to select which assignment they
 * would like to run.
 * @return the user's option
 */
int menuFunction()
{
    int menuOption;
    //Here I am creating the Menu
    cout << "Welcome to the Homework Menu!" << endl;
    cout << "Please Select From the Following Options:" << endl;
    cout << setfill('-') << setw(71) << " " << endl;
    cout << setfill(' ') << setw(40) << left << "(1): Deleting Dynamic Names"
            << "(2): Adding Dynamic Names" << endl;
    cout << setw(40) << left << "(3): The Square" << "(4): The Zoo" << endl;
    cout << setw(40) << left << "(5): Increasing Numbers" << endl;
    cin >> menuOption;

    return menuOption;
}

/**
 * Function used to create a dynamic array of size
 * @param size of the array
 * @return the dynamic array
 */
string* createArray(int size)
{
    string* array = new string[size];

    array[0] = "Marc";
    array[1] = "Jen";
    array[2] = "Matt";
    array[3] = "Dave";
    array[4] = "Josh";

    return array;
}

/** 
 * Function to Read from an array. Runs a loop, iterates through the array to
 * the end of it.
 * @param names the array
 * @param size - the size of the array
 */
void output(const string names[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << setw(6) << left << names[i] << " ";
    }
    cout << endl;
}

/**
 * Function to output the 2D Dynamic Array
 * @param twoD the Dynamic Array
 * @param rows the rows in the array
 * @param columns the columns in the array
 */
void output(CharPtr* twoD, int rows, int columns)
{
    // Outer loop for rows
    for(int i = 0; i < rows; i++)
    {
        // Inner loop for columns
        for(int j = 0; j < columns; j++)
        {
            cout << twoD[i][j];
        }
        cout << endl;
    }
    cout << endl;
}

/**
 * Function to output the 2D Dynamic Array
 * @param twoD the Dynamic Array
 * @param rows the rows in the array
 * @param columns the columns in the array
 */
void output(IntPtr* twoD, int rows, int columns)
{
    // Outer loop for rows
    for(int i = 0; i < rows; i++)
    {
        // Inner loop for columns
        for(int j = 0; j < columns; j++)
        {
            cout << twoD[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
}

/**
 * Function to remove a name with a given location. Will first create a new dynamic array,
 * copy all values except one, and decrease the size. Error Checking included.
 * @param names the array of names
 * @param size the size of the array
 * @param location the location the user wants to delete
 * @return the new array
 */
string* deleteEntry(string *names, int& size, int location)
{
    /* Error Checking Done Initially to save computer processing power. If the user-entered
     * a location that is larger than the size of the array, the function will just return the
     * array back unchanged. Prevents unnecessary looping and memory allocations.
     */

    // NOTE: if the array is Size 5, then there is only locations 0 - 4
    if(location >= size)
    {
        cout << "Location Not Found!" << endl;
        cout << "Array Will Remain The Same" << endl;
        return names;
    }
    else
    {
        int arraySize = 0;
        // Create new dynamic array 
        string* newArray = new string[size];
        // Copy All strings except user chosen location
        for(int i = 0; i < size; i++)
        {
            if(i != location)
            {
                newArray[arraySize] = names[i];
                arraySize++;
            }
        }
        size--;
        return newArray;
    }
}

/**
 * Function to add an entry to a string array
 * @param names the names in the array
 * @param size the size of the array
 * @param name the name to be added to the array
 * @return the new array
 */
string* addEntry(string *names, int& size, string name)
{
    int arraySize = size + 1;
    // Create new dynamic array one larger
    string* newArray = new string[arraySize];

    // Copy All strings over to the new array
    for(int i = 0; i <= size; i++)
    {
        if(i < size)
        {
            newArray[i] = names[i];
        }
        else // i = size
        {
            newArray[i] = name;
        }
    }

    // Increase size
    size++;
    // Return the completed array
    return newArray;
}

CharPtr* create2DArray(int rows, int columns)
{
    // Outside Dynamic Array
    CharPtr* twoD = new CharPtr[rows];

    // Inside Dynamic Array
    for(int i = 0; i < rows; i++)
    {
        twoD[i] = new char[columns];
    }
    return twoD;
}

IntPtr* create2DNumbers(int rows, int columns)
{
    // Outside Dynamic Array
    IntPtr* twoD = new IntPtr[rows];

    // Inside Dynamic Array
    for(int i = 0; i < rows; i++)
    {
        twoD[i] = new int[columns];
    }
    return twoD;
}

void createSquare(CharPtr* twoD, int rows, int columns)
{
    // First the Rows
    for(int i = 0; i < rows; i++)
    {
        // Then Check the Columns
        if(i == 0)
        {
            for(int j = 0; j < columns; j++)
            {
                twoD[i][j] = '*';
            }
        }
        else if(i == 1 || i == 2)
        {
            twoD[i][0] = '*';
            twoD[i][1] = ' ';
            twoD[i][2] = ' ';
            twoD[i][3] = '*';
        }
        else // i = 3
        {
            for(int j = 0; j < columns; j++)
            {
                twoD[i][j] = '*';
            }
        }
    }
}

Animal generateAnimal()
{
    Animal temp;

    cout << "What kind of Animal?" << endl;
    cin >> temp.type;
    cout << "What is the Animal's name?" << endl;
    cin >> temp.name;
    cout << "How Old is the Animal?" << endl;
    cin >> temp.age;

    return temp;
}

void output(const Animal &p)
{
    cout << "Animal: " << p.type << endl;
    cout << "Name: " << p.name << endl;
    cout << "Age: " << p.age << endl;
}

void createNumbers(IntPtr* board, int rows, int columns)
{
    // First the Rows
    for(int i = 0; i < rows; i++)
    {
        // Then the Columns
        for(int j = 0; j < columns; j++)
        {
            board[i][j] = i*columns + j + 1;
        }
    }
}

void problem1()
{
    /* QUESTION 1: The function will first create a new dynamic array, copy all the 
     * values except for one at the location given, and decrease the size. This will 
     * only be done if the location is actually valid, so proper error checking is 
     * needed. Finally, it will return the dynamic array.
     */

    int location;
    int size = 5;
    string input;
    string* newArray;

    // Create Dynamic Array
    string* array = createArray(size);
    output(array, size);
    do
    {
        cout << "What Location Would You Like to Delete?" << endl;
        cin >> location;

        newArray = deleteEntry(array, size, location);
        cout << "New Array Is:" << endl;
        output(array, size);
        cout << right << setfill('-') << setw(65) << " " << setfill(' ') << endl;
        cout << "Would You Like to Delete Another Name? Y/N?" << endl;
        cin >> input;
    }
    while(tolower(input[0]) == 'y');

    // Deallocation
    delete[] array;
    delete[] newArray;
}

void problem2()
{
    /* QUESTION 2: Instead of deleting entries, what if we wanted to add a new name? 
     * Repeat problem 5, but instead of deleting an entry, add a new name at the end 
     * of the dynamics array. The function performs the same way as deleteEntry, but 
     * instead increases the size of the dynamic array and adds the element at the end. 
     */


    string name;
    int size = 5;
    string input;
    string* newArray = createArray(size);

    // Create Dynamic Array
    string* array = createArray(size);
    output(array, size);
    do
    {
        cout << "Please Enter A Name To Be Added:" << endl;
        cin >> name;

        array = addEntry(array, size, name);
        cout << "New Array Is:" << endl;
        output(array, size);
        cout << right << setfill('-') << setw(65) << " " << setfill(' ') << endl;
        cout << "Would You Like to Add Another Name? Y/N?" << endl;
        cin >> input;
    }
    while(tolower(input[0]) == 'y');
    // Deallocation
    delete[] array;
    delete[] newArray;
}

void problem3()
{
    // Create Rows and Columns
    int rows = 4;
    int columns = 4;

    cout << "Implementing the 2D Array..." << endl;
    CharPtr* twoDArray = create2DArray(rows, columns);
    createSquare(twoDArray, rows, columns);
    output(twoDArray, rows, columns);

    // Deallocation, inner first
    for(int i = 0; i < rows; i++)
    {
        delete[] twoDArray[i];
    }

    // Delete outer
    delete[] twoDArray;
}

void problem4()
{
    /* QUESTION 4: Create a structure called Animal with three different attributes. 
     * Allow the user to store values into the structure. Output the contents of 
     * the structure to the console. Write a function for the output.
     */
    string input;

    Animal first = generateAnimal();

    cout << "Displaying Animal From Zoo:\n\n";
    output(first);

    cout << "Would You Like To Add Another Animal To the Zoo?" << endl;
    cin >> input;
    if(tolower(input[0]) == 'y')
    {
        vector<Animal> zoo;
        // Add the First Animal
        zoo.push_back(first);
        do
        {
            zoo.push_back(generateAnimal());
            // Output The Zoo
            for(int i = 0; i < zoo.size(); i++)
            {
                cout << "Displaying Animal " << i + 1 << "." << "\n\n";
                output(zoo[i]);
            }
            cout << "Would you Like To Add Another Animal?" << endl;
            cin >> input;
        }
        while(tolower(input[0]) == 'y');
    }
}

void problem5()
{
    /* QUESTION 5: Create a program that gets two numbers from the user. These 
     * numbers represent the number of rows and columns in a Multidimensional 
     * dynamic array. Then, store the increasing values into the dynamic arrays
     **/
    int rows, columns;
    string input;

    cout << "First, Please Select The Amount Of Rows The Array Will Have." << endl;
    cin >> rows;
    cout << "Next, Please Select The Amount Of Columns The Array Will Have." << endl;
    cin >> columns;
    IntPtr* numbers = create2DNumbers(rows, columns);
    createNumbers(numbers, rows, columns);
    output(numbers, rows, columns);
    cout << "Would You Like To Add Another Row?" << endl;
    cin >> input;
    while(tolower(input[0]) == 'y')
    {
        rows++;
        numbers = create2DNumbers(rows, columns);
        createNumbers(numbers, rows, columns);
        output(numbers, rows, columns);
        cout << "Would You Like To Add Another Row?" << endl;
        cin >> input;
    }
    
    // Deallocation
    for(int i = 0; i < rows; i++)
    {
        delete[] numbers[i];
    }
    // Delete outer
    delete[] numbers;
}