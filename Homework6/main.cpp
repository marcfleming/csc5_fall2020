/*
 * Name: Marc Fleming
 * Student ID: 2858623
 * Date: 04/02
 * HW: Homework 6
 * Problem: 7 Questions
 * I certify this is my own work and code
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <windows.h>
#include <cmath>

using namespace std;

// GLOBAL CONSTANTS
const double METER = 0.3048; // meters in a foot 
const double CENTIMETER = 100; // centimeters in a meter
const double INCH = 12; // inches in a foot

double max(double num1, double num2);
double max(double num1, double num2, double num3);
void swapIntegers(int &integerNum1, int &integerNum2);
void swapStrings(string &string1, string &string2);
void swapChars(char char1, char char2, string &string1, string &string2);


void lengthInputFunction(double &feetLength, double &inchesLength);
void calculateLength(double feetLength, double inchesLength, double &totalLengthMeters, double &totalLengthCentimeters);
void outputLengthFunction(double feetLength, double inchesLength, double &totalLengthMeters, double &totalLengthCentimeters);
void computeCoins(int coinValue, int &num, int &amountLeft);

int main(int argc, char** argv)
{

    //Wrap the Entire Menu in a Do-While Loop

    string doWhileMenuInput;
    string lengthInputOption;

    srand(time(0));

    do
    {

        //Here I am creating the Menu
        int menuOption;
        cout << "Welcome to the Homework Menu!" << endl;
        cout << "Please Select From the Following Options:" << endl;
        cout << setfill('-') << setw(71) << " " << endl;
        cout << setfill(' ') << "(1): Overload"
                << "\t\t\t" << "(2): Swap Numbers" << endl;
        cout << "(3): Swap Strings" << "\t\t" << "(4): Swap Characters" << endl;
        cout << "(5): Length Conversion" << "\t\t" << "(6): Get Some Cents" << endl;
        cin >> menuOption;

        switch(menuOption)
        {
            case 1:
            {
                /* Write two overloaded functions called max that takes either 
                 * two or three parameters of type double and returns the largest of them.
                 */
                double num1, num2, num3, largest;

                cout << "Hello and Welcome to Overload!" << endl;
                cout << "Please enter your first number: " << endl;
                cin >> num1;
                cout << "Please enter your second number: " << endl;
                cin >> num2;
                largest = max(num1, num2);
                cout << "The largest of those two numbers is " << largest << endl;
                cout << "Please enter your third number: " << endl;
                cin >> num3;
                largest = max(num1, num2, num3);
                cout << "The largest of those three numbers is " << largest << endl;
                break;
            }
            case 2:
            {
                /* 
                 * Implement the swap function. The swap function does not
                 * return any value, and uses two integers as input. 
                 * Call your function swapIntegers
                 */
                int integerNum1, integerNum2;

                cout << "Please enter 2 values, One at a time: " << endl;
                cin >> integerNum1;
                cin >> integerNum2;

                cout << "Before: " << endl;
                cout << "Number 1: " << integerNum1 << "\t Number 2: " << integerNum2 << endl;

                // Swap the values
                swapIntegers(integerNum1, integerNum2);

                cout << "After: " << endl;
                cout << "Number 1: " << integerNum1 << "\t Number 2: " << integerNum2 << endl;
                break;
            }
            case 3:
            {
                /*
                 * QUESTION 3: Implement the swap function. The swap function 
                 * does not return any value, takes two strings as parameters.
                 * Call your function swapStrings.
                 */

                string string1, string2;


                cout << "Please enter a word: " << endl;
                cin >> string1;
                cout << "Please enter another word: " << endl;
                cin >> string2;

                cout << "Before: " << endl;
                cout << "String 1: " << string1 << "\t String 2: " << string2 << endl;

                // Swap the values
                swapStrings(string1, string2);

                cout << "After: " << endl;
                cout << "String 1: " << string1 << "\t String 2: " << string2 << endl;
                break;
            }
            case 4:
            {
                /*
                 * QUESTION 4: Implement a 3rd swap function. The swap function 
                 * does not return any value, takes two chars as parameters. 
                 * Call your function swapChars. Invoke the function by swapping
                 * two characters in both a single string variable, and within
                 * between two string variables.
                 */


                char char1, char2;
                string string1, string2;


                cout << "Please enter a character: " << endl;
                cin >> char1;
                cout << "Please enter another character: " << endl;
                cin >> char2;

                cout << "Before: " << endl;
                cout << "Character 1: " << char1 << "\t Character 2: " << char2 << endl;

                // Swap the values
                swapChars(char1, char2, string1, string2);

                cout << "After: " << endl;
                cout << "Character 1: " << string1 << "\t Character 2: " << string2 << endl;
                break;
                break;
            }
            case 5:
            {
                /*
                 * QUESTION 5: Write a program that reads in the length in feet
                 * and inches and outputs the equivalent length in meters and 
                 * centimeters. User at least three functions: one for input, 
                 * one or more for calculating, and one for output. Include a 
                 * loop that lets the user repeat this computation for new input
                 * values until the user says he or she wants to end the program.
                 * There are 0.3048 meters in a foot, 100 centimeters in a meter,
                 * and 12 inches in a foot.  
                 */
                double feetLength, inchesLength, totalLengthMeters, totalLengthCentimeters;

                do
                {
                    // Call the Three Functions
                    lengthInputFunction(feetLength, inchesLength);

                    calculateLength(feetLength, inchesLength, totalLengthMeters, totalLengthCentimeters);

                    outputLengthFunction(feetLength, inchesLength, totalLengthMeters, totalLengthCentimeters);

                    // Repeat the Functions as many times as the User would like.
                    cout << "Would You Like to Input A New Length?: " << endl;
                    cin >> lengthInputOption;
                    cout << endl;
                }
                while(tolower(lengthInputOption[0]) == 'y');
                cout << "Thank You!" << endl;
                break;
            }
            case 6:
            {
                do
                {
                    /*
                     * QUESTION 6: Write a program that tells what coins to give out
                     * for any amount of change from 1 cent to 99 cents. 
                     */
                    int totalCoins, amountLeft;
                    int pennies = 0, dimes = 0, quarters = 0;

                    cout << "How Many Cents Do You Have?: " << endl;
                    cin >> totalCoins;
                    amountLeft = totalCoins;

                    // Call the function 3 times for quarters, dimes, and pennies
                    computeCoins(25, quarters, amountLeft);

                    computeCoins(10, dimes, amountLeft);

                    computeCoins(1, pennies, amountLeft);

                    cout << "You Have " << totalCoins << " cents!" << endl;
                    cout << "Here Is Your Change:" << endl;
                    cout << quarters << " Quarter(s), " << dimes << " Dime(s), and "
                            << pennies << " penny(pennies)." << endl;

                    // Repeat the Functions as many times as the User would like.
                    cout << "Would You Like Some More Change? Y/N: " << endl;
                    cin >> lengthInputOption;

                }
                while(tolower(lengthInputOption[0]) == 'y');
                cout << "Thank You!" << endl;
                break;
            }
            case 7:
            {
                /*
                 * QUESTION 7: Include a loop that lets the user repeat the 
                 * problem 6 computation for new input values until the user
                 * wants to end the program.
                 */

                // Done


                break;
            }
            default:
            {
                cout << "Sorry You didn't pick the right value" << endl;
            }
        }
        cout << setfill('-') << setw(65) << " " << endl;
        cout << "Would You Like to Return To the Main Menu? Y/N?" << endl;
        cin >> doWhileMenuInput;
    }
    while(tolower(doWhileMenuInput[0]) == 'y');
    cout << "Have a Wonderful Day!" << endl;
    return 0;
}

/**
 * Function that calculates what value is the largest of the two inputed by the user
 * @param num1 First value
 * @param num2 Second value
 * @return the largest value
 */
double max(double num1, double num2)
{
    if(num1 > num2)
    {
        return num1;
    }
    else
    {
        return num2;
    }
}

/**
 * Function that calculates what value is the largest of the two inputed by the user
 * @param num1 First value
 * @param num2 Second value
 * @param num3 Third value
 * @return the largest value
 */
double max(double num1, double num2, double num3)
{
    if(num1 > num2 && num1 > num3)
    {
        return num1;
    }
    else if(num2 > num1 && num2 > num3)
    {
        return num2;
    }
    else
    {
        return num3;
    }
}

/**
 * Function that Swaps two integers. Takes first int, asigns it to temp variable. 
 * Second to first, then temp to second
 * @param integerNum1 User Inputs first value
 * @param integerNum2 User Inputs second value
 * both ints are addressed to display in main
 */
void swapIntegers(int &integerNum1, int &integerNum2)
{
    int temp = integerNum1;
    integerNum1 = integerNum2;
    integerNum2 = temp;
}

/**
 * Function that Swaps two integers. Takes first int, assigns it to temp variable. 
 * Second to first, then temp to second
 * @param string1 User Inputs first string
 * @param string2 User Inputs second string
 * both strings are addressed to display in main
 */
void swapStrings(string &string1, string &string2)
{
    string tempString = string1;
    string1 = string2;
    string2 = tempString;
}

/**
 * Function that swaps the characters that a user inputs into the program. converts
 * to a string as requested by the professor, and then swaps thems.
 * @param char1 User Inputs first character
 * @param char2 User Inputs second character
 */
void swapChars(char char1, char char2, string &string1, string &string2)
{
    string tempString;

    string1 += char1;
    string2 += char2;
    tempString = string1;
    string1 = string2;
    string2 = tempString;
}

/**
 * Function that receives a length in feet and inches, and verifies that the user
 * did not enter a negative number. Part 1 of 3 functions.
 * @param feetLength the length in feet that the user inputs
 * @param inchesLength the length in inches that the user inputs
 * both doubles are addressed to display in main
 */
void lengthInputFunction(double &feetLength, double &inchesLength)
{
    cout << "Enter Length in feet: " << endl;
    cin >> feetLength;
    // Make The user Unable to enter a negative number
    while(feetLength < 0)
    {
        cout << "Length Can't Be Less Than Zero!" << endl;
        cout << "Please Enter Again:" << endl;
        cin >> feetLength;
    }
    cout << "Enter Inches: ";
    cin >> inchesLength;
    while(inchesLength < 0)
    {
        cout << "Length Can't Be Less Than Zero!" << endl;
        cout << "Please Enter Again:" << endl;
        cin >> inchesLength;
    }
    cout << " " << endl;
}

/**
 * Function that converts Feet and Inches to Meters and Centimeters. Done simply by
 * flooring meters, thus removing the decimal, and subtracting that from the original
 * amount of meters, therefore leaving me with just the decimal. Multiply that by 100
 * to get centimeters. Part 2 of 3 functions
 * @param feetLength the length in feet received from previous function
 * @param inchesLength the length in inches received from previous function
 * @param totalLengthMeters length calculated in meters
 * @param totalLengthCentimeters remaining amount calculated to centimeters
 * meters and centimeters are addressed to display in main
 */
void calculateLength(double feetLength, double inchesLength, double &totalLengthMeters, double &totalLengthCentimeters)
{
    double temp;
    totalLengthMeters = (feetLength + (inchesLength / INCH)) * METER;
    totalLengthCentimeters = totalLengthMeters - floor(totalLengthMeters);
    totalLengthCentimeters = totalLengthCentimeters * CENTIMETER;
    totalLengthMeters = floor(totalLengthMeters);
}

/**
 * Function that outputs the desired result to the user. Displays first what the user
 * inputted into the system, then the converted meters and centimeters
 * Part 3 of 3 functions
 * @param feetLength the length in feet received from previous function
 * @param inchesLength the length in inches received from previous function
 * @param totalLengthMeters length calculated in meters from previous function
 * @param totalLengthCentimeters remaining amount calculated to centimeters from 
 * previous function
 */
void outputLengthFunction(double feetLength, double inchesLength, double &totalLengthMeters, double &totalLengthCentimeters)
{
    cout << "The Length You Gave Was " << feetLength << " Feet And " << inchesLength << " Inches." << endl;
    cout << "This Is Equivalent To " << totalLengthMeters << " Meters And " << totalLengthCentimeters << " Centimeters." << endl;
}

/**
 * Function to compute the amount of coins the user will receive back from their inputted
 * amount. First checks to make sure the user has not entered an invalid number, then
 * subtracts the coinValue from the amountLeft. Loops until amountLeft is less than coinValue
 * @param coinValue the value of the coin to be compared (i.e. dimes are 10)
 * @param num how many of that coin the user would receive
 * @param amountLeft the amount leftover (if any) 
 */
void computeCoins(int coinValue, int &num, int &amountLeft)
{
    // Precondition: 0 < coin_value < 100; o <= amount_left < 100.
    if(0 < coinValue < 100 || 0 <= amountLeft < 100)
    {
        while(amountLeft >= coinValue)
        {
            num++;
            amountLeft = amountLeft - coinValue;
        }
    }
    else // If the user enters a value 0 or less, or more than 100
    {
        cout << "ERROR! I APOLOGIZE BUT YOU CAN ONLY ENTER A NUMBER BETWEEN 0 AND 100." << endl;
        cout << "I don't make the rules" << endl;
    }
}

