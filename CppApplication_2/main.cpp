/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/cppFiles/main.cc to edit this template
 */

/* 
 * File:   main.cpp
 * Author: The Boss
 *
 * Created on June 4, 2022, 9:15 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <windows.h>
#include <cmath>
#include <vector>

using namespace std;



int main()
{

       int random;
    vector<int> vect;
    //Populate vector
    for(int i = 0; i < 10; i++)
    {
        random = (rand() % 70) + 1;
        vect.push_back(random);
    }
    
    // Output Vector
    for(int i = 0; i < vect.size(); i++)
    {
        cout << vect[i] << endl;
    }
    
    // Get Average
    int average = 0;
    for(int i = 0; i < vect.size(); i++)
    {
        average = vect[i] + average;
    }
    average = average/(vect.size());
    cout << "The Average Is: " << average << endl;
    
    // Location For the location of the average
    for(int i = 0; i < vect.size(); i++)
    {
        if(average == vect[i])
        {
            cout << "Average is: " << average << " At Location " << i << endl;
        }
    }
    int distance = 10000;
    int closestNum;
    // If the location is NOT in the vector, get nearest one
    for(int i = 0; i < vect.size(); i++)
    {
        int difference;
        difference = average - vect[i];
        difference = abs(difference);
        if(difference < distance)
        {
            closestNum = vect[i];
            distance = difference;
        }
    }
    cout << "The Closest Number is: " << closestNum << endl;
    
}

