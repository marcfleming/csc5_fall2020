
/*
* Name: Marc Fleming
* Student ID: 2858623
* Date: 02/26/2022
* HW: Homework 1 (Full Project)
* Problem: Create multiple I/O functions on a single file
* I certify this is my own work and code
*/

#include <iostream>


using namespace std;

/*
 * Step 1: Write a program that stores the integers 50(a) and 100(b) in 
 * variables, and stores 
 * the sum(sum) of these two in a variable named total
 */
int main(int argc, char** argv) {

    int a;
    int b;
    int total;
    {
    
    a=50;
    b=100;
    total=a+b;
    
    cout << "STEP ONE:" << endl;
    cout << "The sum of " << a << " and " << b << " is: " << total << endl;
           //adding a single line break per each step
            cout << "  " << endl;
    }

/*
 * Step 2: Write a program that will compute the total sales tax on a $95 
 * purchase. 
 * Assume the state sales tax is 4 percent, and the county sales tax is 
 * 2 percent.
 */

    {
        // Decide my Data I am storing, and assign values to inputs
        int purchase;
        double statetax;
        double countytax;
        double totalsalestax;
        double t;
        double p;
        double s;
        double c;
        double t2;
        
        purchase=95;
        statetax= .04;
        countytax= .02;
        
        //Shorten Values for Q.o.L
        t=totalsalestax;
        p=purchase;
        s=statetax;
        c=countytax;
        
        // Create Function to Calculate Data
        t=((p * s) + (p * c));
        t2=((p * s) + (p * c) + p);
        
        //Output the data
        cout << "STEP TWO:" << endl;
        cout << "A purchase of $" << p << " would have a tax of $" << t << endl;
        cout << "The total cost would be: $" << t2 << endl;
        //Line Break End of Step
        cout << " " << endl;
       
    }
    
/*
 * Step 3: Write a program that computes the tax and tip on a restaurant bill 
 * for a patron with a $88.67 meal charge. The tax should be 6.75 percent of 
 * the meal cost. The tip should be 20 percent of the total after adding 
 * the tax. Display the meal cost, tax amount, tip amount, and 
 * total bill on the screen.
 */
    {
        // Decide my Data I am storing, and assign data types to variables
        float meal;
        float tax;
        float tip;
        //Reset Value for 'total'
            total=0.0; //(Possibly Unnecessary?)
            float total;
            
        
        // Create Function to Calculate Data
            meal=88.67;
            tax=( meal * 0.0675 );
            tip=(( meal + tax ) * 0.20);
            total= meal + tax + tip;
            
        // Display Information on Screen
            cout << "STEP THREE:" << endl;
            cout << "The cost of the meal before tax is: $" << meal << endl;
            cout << "The cost of the tax for the meal is: $" << tax << endl;
            cout << "The tip that you should pay would be: $" << tip << endl;
            cout << "Therefore" << endl;
            cout << "The total cost of the meal is: $" << total << endl;
        //Line Break End of Step
        cout << " " << endl;
    }
 /*
 * Step 4: A car holds 15 gallons of gasoline and can travel 375 miles before 
 * refueling. Write a program that calculates the number of miles per gallon
 * the car gets. Display the result on the screen.
 */
    
    {
        // Decide my Data I am storing, and assign values to inputs
            double gallons;
            double miles;
            double MPG;
            double g;
            double m;
        
        gallons=15;
        miles=375;
        
        //Shorten Values for Q.o.L
            g=gallons;
            m=miles;
            
        // Create Function to Calculate Data
            MPG=(m/g);
            
        // Display Information on Screen   
            cout << "STEP FOUR:" << endl;
            cout << "The vehicle gets " << MPG << " miles per gallon." << endl;
        //Line Break End of Step
            cout << " " << endl;
    }
    
/*
 * Step 5: Write a program that displays the following pieces of information,
 * each on a separate line:
 * Your name
 * Your telephone number (fake)
 * Your college major
 * Use only a single cout statement to display all of this information.
 */ 
    
    {   
        // Display Information on Screen
        cout << "STEP FIVE:" << endl;
        cout<<"Marc Fleming"<<endl<<"505-879-6457"<<endl<<"Cyber Defense!"<<endl;
    
    //Line Break End of Step
    cout << " " << endl;
    }
    
/*
 * Step 6: Using the image below, complete questions (1-4) 
 */    
    cout << "Step 6\n" << endl;
    {
        int number_of_pods, peas_per_pod, total_peas;
        
        cout << "Hello!" << endl;
        cout << "Press return after entering a number.\n'";
        cout << "Enter the number of pods:\n";
        
        cin >> number_of_pods;
        
        cout << "Enter the number of peas in a pod:\n";
        cin >> peas_per_pod;
        total_peas = number_of_pods + peas_per_pod;
        cout << "If you have ";
        cout << number_of_pods;
        cout << " pea pods\n";
        cout << "and ";
        cout << peas_per_pod;
        cout << " peas in each pod, then\n";
        cout << "you have ";
        cout << total_peas;
        cout << " peas in all the pods.\n";
        cout << "Goodbye\n";
    }
    return 0;
}
