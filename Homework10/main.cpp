/*
 * Name: Marc Fleming
 * Student ID: 2858623
 * Date: 05/16/2022
 * HW: Homework 10
 * Problem: 6 Questions
 * I certify this is my own work and code
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <vector>


using namespace std;

int menuFunction();
void displayValues();
int* createArray(int size);
void randArray(int num[], int size);
void output(const int a[], int size);
void output(const vector<int>& v);
void randVector(vector<int>& v);
void selectionSort(vector<int>& v);
int binarySearch(const vector<int> &v, int value);


void problem1();
void problems2And3();
void problem4();
void problem5();
void problem6();

// GLOBAL CONSTANTS
const int VECTORSIZE = 100000;

int main(int argc, char** argv)
{
    string doWhileMenuInput;
    srand(time(0)); // SEED
    do
    {
        int menuOption;
        menuOption = menuFunction();
        switch(menuOption)
        {
            case 1:
                problem1();
                break;

            case 2: case 3:
                problems2And3();
                break;

            case 4:
                problem4();
                break;
                
            case 5:
                problem5();
                break;
                
            case 6:
                problem6();
                break;
                
            default:
            {
                cout << "You Selected an Invalid Response!" << endl;
            }
        }
        cout << setfill('-') << setw(65) << " " << endl;
        cout << "Would You Like to Return To the Main Menu? Y/N?" << endl;
        cin >> doWhileMenuInput;
    }
    while(tolower(doWhileMenuInput[0]) == 'y');
    cout << "Have a Wonderful Day!" << endl;
    return 0;
}
//END OF MAIN

/**
 * Function created for the menu. Prompts the user to select which assignment they
 * would like to run.
 * @return the user's option
 */
int menuFunction()
{
    int menuOption;
    //Here I am creating the Menu
    cout << "Welcome to the Homework Menu!" << endl;
    cout << "Please Select From the Following Options:" << endl;
    cout << setfill('-') << setw(71) << " " << endl;
    cout << setfill(' ') << setw(40) << left << "(1): Confusing Pointers"
            << "(2-3): Dynamic Arrays" << endl;
    cout << setw(40) << left << "(4): Static Array" << "(5): Building Dynamic Arrays" << endl;
    cout << setw(40) << left << "(6): Binary Search" << endl;
    cin >> menuOption;

    return menuOption;
}

void displayValues(int &v, int &v1)
{
    cout << "v: " << v << endl;
    cout << "v1: " << v1 << "\n\n";
}

int* createArray(int size)
{
    int* array = new int(size);
    
    randArray(array, size);

    return array;
}

/**
 * Function to randomize an array in the size of the User's choice. Randomizes 
 * between 1 and 1000
 * @param num the array
 * @param size the size of the array
 */
void randArray(int num[], int size)
{
    // Store the values inside an array
    for(int i = 0; i < size; i++)
    {
        int randNum = (rand() % 100) + 1;
        num[i] = randNum;
    }
}

/** 
 * Function to Read from an array. Runs a loop, iterates through the vector to
 * the end of it.
 * @param a the array
 * @param size the size of the array
 */
void output(const int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << setw(4) << left << a[i] << " ";
    }
    cout << endl;
}

/**
 * Function to Read from an int vector. Runs a loop, iterates through the vector to
 * the end of it.
 * @param v the vector
 */
void output(const vector<int>& v)
{
    for(int i = 0; i < v.size(); i++)
    {
        cout << v[i] << "  ";
    }
    cout << endl;
}

void randVector(vector<int>& v)
{
    for(int i = 0; i < v.size(); i++)
    {
        v[i] = (rand() % 1000) + 1;
    }
}

/**
 * Selection sort function to sort a vector
 * @param v the vector
 */
void selectionSort(vector<int>& v)
{
    for(int i = 0; i < v.size() - 1; i++)
    {
        int min = i;

        for(int j = i; j < v.size(); j++)
        {
            if(v[j] < v[min])
            {
                min = j;
            }
        }

        // Swap
        swap(v[i], v[min]);
    }
}

/**
 * Function to run the Binary search of a vector. Checks the middle of the vector looking
 * for a match
 * @param v
 * @param value
 * @return 
 */
int binarySearch(const vector<int> &v, int value)
{
    int low = 0;
    int high = v.size() - 1;
    int middle = (low + high) / 2;
    int guess = v[middle];

    // Keep Splitting vector in halves and search through that section
    while(guess != value && low < high)
    {
        if(guess < value)
        {
            low = middle + 1;
            middle = (low + high) / 2;
            guess = v[middle];
        }
        else if(guess > value)
        {
            high = middle - 1;
            middle = (low + high) / 2;
            guess = v[middle];
        }
        else
        {
            // MATCH!!
        }
    }
    if(low > high)
    {
        return -1;
    }
    else
    {
        return middle;
    }
}

void problem1()
{
    /* QUESTION 1: Create an integer variable with the value 42 and another
     * integer variable assigned to the first one. Assign two different pointers 
     * to the addresses of the integer variables. Change the first value three 
     * different ways. First change it to 12, using the variable. Then, change 
     * it to 25, using one of the pointers and then change it to 60 using the 
     * other pointer. Output the two integer variables every single time the 
     * value has been changed.
     */

    int v = 42;
    int v1 = v;

    displayValues(v, v1);

    v = 12;

    displayValues(v, v1);

    int* pointer1;
    pointer1 = &v;
    *pointer1 = 25;

    displayValues(v, v1);

    int* pointer2;
    pointer2 = &v;

    cout << "Address of second pointer: " << pointer2 << endl;
    cout << "Value in address: " << *pointer2 << endl;

    *pointer2 = 60;
    cout << "Value in address: " << *pointer2 << endl;

    displayValues(v, v1);
}

void problems2And3()
{
    /* QUESTION 2: Write a program that takes an integer from the user 
     * and creates a dynamic array of that size. Store the array with 
     * random values and output the values after they have all been 
     * stored.
     */
    int size;

    cout << "How Large Would You Like the Dynamic Array to Be?" << endl;
    cin >> size;

    // Create Dynamic Array
    int* array = createArray(size);
    output(array, size);

    // Deallocation
    delete[] array;
}

void problem4()
{
    /* QUESTION 4: Create a static array and a pointer. Have the pointer point
     *  to the same location as the array. Output both the contents of the array, 
     * and the pointer. Remember, pointers and arrays are virtually the same 
     * thing. 
     */

    int size = 9;
    int staticArray[size];

    cout << "Static Array is:" << endl;
    randArray(staticArray, size);
    output(staticArray, size);

    // Create pointer
    int* p;

    cout << "Address of Static Array is: " << &staticArray << endl;
    // Assign pointer to address
    p = staticArray;

    cout << "Address Pointer Points to is: " << p << endl;
}

void problem5()
{
    /* QUESTION 5: Create a dynamic array of size 5. Allow the user to enter 
     * as many values as they like, even if it surpasses the original size. 
     * How can we handle this type of error? Once the user is done entering 
     * values, output the final array.
     */

    string input;
    int size = 5;
    int value;
    int* array = new int[size];
    int indexSize = 0;
    int i = 0;

    do
    {

        cout << "Please Enter A Value: ";
        cin >> value;
        cout << endl;
        array[i] = value;
        cout << array[i] << endl;
        indexSize++;
        i++;

        cout << "Would You Like To Enter Another Value Into the Array?" << endl;
        cin >> input;
        cout << endl;
    }
    while(tolower(input[0]) == 'y');

    // Error Check the array
    if(indexSize > size)
    {
        cout << "Dynamic Array is Set to Size 5. Only 5 Values Will Be Returned" << endl;
        output(array, size);
    }
    else
    {
        output(array, size);
    }
    // Deallocation
    delete[] array;
}

void problem6()
{
    vector<int> largeVector(VECTORSIZE);
    int number, location;
    
    randVector(largeVector);
    cout << "Vector Is:" << endl;
    output(largeVector);
    
    cout << "Sorted Vector Is:" << endl;
    selectionSort(largeVector);
    output(largeVector);
    
    cout << "What Number Would You Like To Search For?" << endl;
    cin >> number;
    
    cout << "Implementing Binary Search..." << endl;
    
    location = binarySearch(largeVector, number);
    
    if(location == -1)
    {
        cout << "That Number Was Not Found!!!" << endl;
    }
    else
    {
        cout << "The Number Is Located at: " << location << endl;
    }
    
}