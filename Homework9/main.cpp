
/*
 * Name: Marc Fleming
 * Student ID: 2858623
 * Date: 05/09/2022
 * HW: Homework 9
 * Problem: 7 Questions
 * I certify this is my own work and code
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>

using namespace std;

// Global Constants
int COLUMNS = 4;

int menuFunction();
void output(const vector<int>& v);
void output(const vector<string>& names);
void outputIDAndName(const vector<int>& num, const vector<string>& name);
void noRepeats(vector<int>& num, int& randNum);
void outputFirstAndLast(const vector<string> firstName, const vector<string>& lastName);
void readFromInput(istream& input);
void randArray(int num[], int size);
void bubbleSort(int num[], int size);
void selectionSort(int num[], int size);
void selectionSort(vector<int>& v);
void output(const int a[], int size);
void removal(vector<int>& v, int deleteLocation);
bool contains(const vector<int>& v, int randNum);


void problem1();
void problem2();
void problem3();
void problem4();
void problem5();
void problem6();
void problem7();

int main(int argc, char** argv)
{
    string doWhileMenuInput;
    srand(time(0)); // SEED
    do
    {
        int menuOption;
        menuOption = menuFunction();
        switch(menuOption)
        {
            case 1:
                problem1();
                break;

            case 2:
                problem2();
                break;

            case 3:
                problem3();
                break;

            case 4:
                problem4();
                break;
            case 5:
                problem5();
                break;
            case 6:
                problem6();
                break;
            case 7:
                problem7();
            default:
            {
                cout << "You Selected an Invalid Response!" << endl;
            }
        }
        cout << setfill('-') << setw(65) << " " << endl;
        cout << "Would You Like to Return To the Main Menu? Y/N?" << endl;
        cin >> doWhileMenuInput;
    }
    while(tolower(doWhileMenuInput[0]) == 'y');
    cout << "Have a Wonderful Day!" << endl;
    return 0;
}
//END OF MAIN



/**
 * Function created for the menu. Prompts the user to select which assignment they
 * would like to run.
 * @return the user's option
 */
int menuFunction()
{
    int menuOption;
    //Here I am creating the Menu
    cout << "Welcome to the Homework Menu!" << endl;
    cout << "Please Select From the Following Options:" << endl;
    cout << setfill('-') << setw(71) << " " << endl;
    cout << setfill(' ') << setw(40) << left << "(1): Parallel Vectors"
            << "(2): Parallel Names" << endl;
    cout << setw(40) << left << "(3): Broken Names, Broken People" << "(4): Bubble Sorting" << endl;
    cout << setw(40) << left << "(5): Selection Sorting" << "(6): GradeBook" << endl;
    cout << setw(40) << left << "(7): Add and Delete Values" << endl << right;
    cin >> menuOption;

    return menuOption;
}

/**
 * Function to Read from an int vector. Runs a loop, iterates through the vector to
 * the end of it.
 * @param v the vector
 */
void output(const vector<int>& num)
{
    for(int i = 0; i < num.size(); i++)
    {
        cout << num[i] << "  ";
    }
    cout << endl;
}

/**
 * Function to Read from a string vector. Runs a loop, iterates through the vector to
 * the end of it.
 * @param name the string vector
 */
void output(const vector<string>& name)
{
    for(int i = 0; i < name.size(); i++)
    {
        cout << name[i] << " ";
    }
    cout << endl;
}

/**
 * Function to output a parallel vector of IDs and Names
 * @param num The ID of the person
 * @param name The Name of the person
 */
void outputIDAndName(const vector<int>& num, const vector<string>& name)
{
    for(int i = 0; i < name.size(); i++)
    {
        cout << "Name: " << setw(10) << left << name[i] << "| ID: " << num[i] << endl;
    }
    cout << endl;
}

/**
 * Function to prevent any repeats when assigning a new ID to a name
 * @param num checks the ID
 * @param randNum Checks if ID matches randNum
 */
void noRepeats(vector<int>& num, int& randNum)
{
    vector<int> possibleValues;
    
    for(int i = 0; i < 40; i++)
    {
        possibleValues.push_back(i+1);
    }
    
    bool match;
    do
    {
        randNum = (rand() % 40) + 1;
        
        match = contains(possibleValues, randNum);
    }
    while(!match);
    
}

bool contains(const vector<int>& v, int num)
{
    for (int i = 0; i < v.size(); i++)
    {
        if (v[i] == num)
        {
            return true;
        }
    }
    return false;
}
/**
 * Function to output the first and last name in a parallel vector
 * @param firstName the first name
 * @param lastName the last name
 */
void outputFirstAndLast(const vector<string> firstName, const vector<string>& lastName)
{
    for(int i = 0; i < firstName.size(); i++)
    {
        cout << "Name: " << setw(12) << left << firstName[i] << " " << lastName[i] << endl;
    }
    cout << endl;
}

/**
 * Function to read from input
 * @param input
 */
void readFromInput(istream& input)
{
    string value;
    input >> value;

    cout << value << endl;
}

/**
 * Function to randomize an array in the size of the User's choice. Randomizes 
 * between 1 and 1000
 * @param num the array
 * @param size the size of the array
 */
void randArray(int num[], int size)
{
    // Store the values inside an array
    for(int i = 0; i < size; i++)
    {
        int randNum = (rand() % 1000) + 1;
        num[i] = randNum;
    }
}

/**
 * Bubble sort function. Compares two values inside an array, then moves the smaller
 * one
 * @param num the array
 * @param size the size of the array
 */
void bubbleSort(int num[], int size)
{
    int temp;
    int innerLoop = 0;
    for(int i = 0; i < size; i++)
    {
        // Bubble two values at a time, and swap if needed
        for(int j = 0; j < size - 1; j++)
        {
            innerLoop++;
            if(num[j] > num[j + 1])
            {
                temp = num[j];
                num[j] = num[j + 1];
                num[j + 1] = temp;
            }
        }
    }
    output(num, size);
    cout << "Inner loops: " << innerLoop << endl;
}

/**
 * Selection Sort function. Selects the smallest value and moves it upfront, then
 * does a smaller loop checking for the next smallest value
 * @param num the array
 * @param size the size of the array
 */
void selectionSort(int num[], int size)
{
    int temp;
    int innerLoop = 0;
    // Iterate through the Array
    for(int i = 0; i < (size - 1); i++)
    {
        // Minimum location
        int min = i;

        // Move the smallest value so far
        for(int j = i; j < size; j++)
        {
            innerLoop++;
            if(num[j] < num[min])
            {
                min = j;
            }
        }
        temp = num[i];
        num[i] = num[min];
        num[min] = temp;
    }
    output(num, size);
    cout << "Inner Loops: " << innerLoop << endl;
}

/** 
 * Function to Read from an array. Runs a loop, iterates through the vector to
 * the end of it.
 * @param a the array
 * @param size the size of the array
 */
void output(const int a[], int size)
{
    for(int i = 0; i < size; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}

/**
 * Function to Remove the location from a vector. Unordered removal, vector sets 
 * the delete location value to the last space of the vector, then pop pack the 
 * last space
 * @param v vector
 * @param deleteLocation the location the user wanted to delete
 */
void removal(vector<int>& v, int deleteLocation)
{
    int temp;
    // Error Checking on the Vector
    if(deleteLocation > v.size() || deleteLocation < 0)
    {
        cout << "That Is Outside the Parameters of This Specific Vector!" << endl;
    }
    else
    {
        // May you please explain the swap function? I could not get it to work
        temp = v[deleteLocation];
        v[deleteLocation] = v[v.size() - 1];
        v[v.size() - 1] = temp;
        // Pop back the last value
        v.pop_back();
    }
}

/**
 * Selection sort function to sort a vector
 * @param v the vector
 */
void selectionSort(vector<int>& v)
{
    for(int i = 0; i < v.size() - 1; i++)
    {
        int min = i;

        for (int j = i; j < v.size(); j++)
        {
            if (v[j] < v[min])
            {
                min = j;
            }
        }
        
        // Swap
        swap(v[i], v[min]);
    }
}

void problem1()
{
    /* QUESTION 1: Write a program that creates two parallel vectors
     * representing the name of the person, and the plane ticket ID
     * of the corresponding person. The program should allow the user
     * to enter a new name, of which the user is then provided with
     * a new random ticket ID number between 1- 40.
     */

    vector<int> ids;
    vector<string> names;
    int randNum;

    // Add a new name
    cout << "Would You Like To Add a Name?? ";
    string input;
    cin >> input;

    // User controlled loop
    while(tolower(input[0]) == 'y')
    {
        cout << "Please enter a name: ";
        string name;
        cin >> name;

        names.push_back(name);
        randNum = (rand() % 40) + 1;
        noRepeats(ids, randNum);
        ids.push_back(randNum);

        outputIDAndName(ids, names);

        cout << "Would You Like to Add Another Name? ";
        cin >> input;
    }
}

void problem2()
{
    /* QUESTION 2: Write two parallel vectors that store the first 
     * name and last names of people. Read in the names from a file 
     * called “names.dat”, and store the last name and first name 
     * appropriately. You can assume each line in the file is a first 
     * and a last name. The first and last name will not be on 
     * separate lines.
     */
    string string1, string2;
    vector<string> firstName;
    vector<string> lastName;

    // Add a new name
    cout << "Would You Like To Add a Name?? ";
    string input;
    cin >> input;

    // User controlled loop
    while(tolower(input[0]) == 'y')
    {
        cout << "Please Enter First and Last Name: " << endl;
        cin >> string1 >> string2;
        cout << endl;

        firstName.push_back(string1);
        lastName.push_back(string2);

        ofstream fout;

        fout.open("names.dat");
        for(int i = 0; i < firstName.size(); i++)
        {
            fout << firstName[i] << " " << lastName[i] << endl;
        }
        fout.close();

        ifstream fin;

        fin.open("names.dat");

        // Error check the file
        if(fin.fail())
        {
            cout << "The reading of the file failed. It does not exist!" << endl;
        }
        else
        {
            while(!fin.eof())
            {
                readFromInput(fin);
            }
        }

        cout << "Would You Like to Add Another Name? ";
        cin >> input;
    }
}

void problem3()
{
    /* QUESTION 3: Create a text file called “broken.dat” where not all the names
     * have a last or first name. Names that do not have a first name are written
     * with a space before the last name. First names with no last name are written
     * with no last name. Modify problem 3 to accommodate this error.
     */

    string first, last;
    vector<string> firstName;
    vector<string> lastName;
    ofstream fout;

    fout.open("broken.dat");

    fout << "Tony Stark" << endl;
    fout << "Marc " << endl;
    fout << " Cena" << endl;
    fout << "Will Fleming" << endl;
    fout << "Amanda " << endl;
    fout << "Christopher " << endl;
    fout << " Banner" << endl;
    fout << " Jung" << endl;
    fout << "Bob " << endl;
    fout << "" << " " << "" << endl;

    fout.close();

    ifstream fin;

    fin.open("broken.dat");

    // Error check the file
    if(fin.fail())
    {
        cout << "The reading of the file failed. It does not exist!" << endl;
    }
    else
    {
        while(!fin.eof())
        {
            // Read from the file and create the parallel vectors at the same time
            getline(fin, first, ' ');
            firstName.push_back(first);
            getline(fin, last, '\n');
            lastName.push_back(last);
        }
    }

    fin.close();

    outputFirstAndLast(firstName, lastName);

    // Fix Missing First Names
    for(int i = 0; i < firstName.size(); i++)
    {
        if(firstName[i] == "")
        {
            cout << "First Name Missing!!!" << endl;
            cout << "What is " << lastName[i] << "'s First Name?" << endl;
            cin >> firstName[i];
        }
    }

    // Fix Missing Last Names
    for(int i = 0; i < lastName.size(); i++)
    {
        if(lastName[i] == "")
        {
            cout << "Last Name Missing!!!" << endl;
            cout << "What is " << firstName[i] << "'s Last Name?" << endl;
            cin >> lastName[i];
        }
    }

    fout.open("broken.dat");
    // Fix Broken File
    for(int i = 0; i < lastName.size(); i++)
    {
        fout << firstName[i] << " " << lastName[i] << endl;
    }

    fout.close();

    outputFirstAndLast(firstName, lastName);
}

void problem4()
{
    /* QUESTION 4: Implement the bubble sort algorithm from this 
     * week’s lecture for an array.
     */
    int size;

    // Ask the User how large they would like the array to be               
    cout << "How Many Values Would You Like The Array To Hold?: " << endl;
    cin >> size;
    cout << endl;

    int numbers[size];

    cout << "Array is:" << endl;
    randArray(numbers, size);

    output(numbers, size);

    cout << "Implementing the Bubble Sort Algorithm..." << endl;
    cout << "Sorted Array is:" << endl;
    bubbleSort(numbers, size);
}

void problem5()
{
    /* QUESTION 5: Implement the selection sort algorithm from this 
     * week’s lecture for an array. 
     */
    int size;

    // Ask the User how large they would like the array to be               
    cout << "How Many Values Would You Like The Array To Hold?: " << endl;
    cin >> size;
    cout << endl;

    int numbers[size];

    cout << "Array is:" << endl;
    randArray(numbers, size);

    output(numbers, size);

    cout << "Implementing the Selection Sort Algorithm..." << endl;
    cout << "Sorted Array is:" << endl;
    selectionSort(numbers, size);
}

void problem6()
{
    /* QUESTION 6: Create a multi-dimensional array representing the 
     * number of scores from a file. Provide the file with the following data: 
     * The number of students, the number of tests, and the number of 
     * corresponding scores.
     */

    int students, tests, grade;

    cout << "How Many Students Are In the Class?" << endl;
    cin >> students;
    cout << "How Many Tests Have Been Provided This Semester?" << endl;
    cin >> tests;
    int gradeBook[students][tests];

    // Nested For Loop
    for(int i = 0; i < students; i++)
    {
        // For Each Column In the Row
        for(int j = 0; j < tests; j++)
        {
            cout << "What is the grade of test #" << j+1 << " for Student " << i+1 << "??" << endl;
            cin >> grade;
            gradeBook[i][j] = grade;
        }
    }

    // Output the Array
    cout << "Gradebook is as shown: " << endl;
    cout << setfill('-') << setw(70) << " " << endl;
    cout << "Student    |";
    for(int i = 0; i < tests; i++)
    {
        cout << setfill(' ') << "Test " << i + 1 << setw(10);
    }
    cout << " " <<endl;
    for(int i = 0; i < students; i++)
    {
        cout << "Student " << i + 1 << ": |";
        // For Each Column In the Row
        for(int j = 0; j < tests; j++)
        {
            cout << gradeBook[i][j] << left << setw(10) << " ";
        }
        cout << endl;
    }
    
    // Write the array to a file
    ofstream fout;

    fout.open("gradebook.dat");
    fout << "Gradebook is as shown: " << endl;
    fout << setfill('-') << setw(70) << " " << endl;
    fout << "Student    |";
    for(int i = 0; i < tests; i++)
    {
        fout << setfill(' ') << "Test " << i + 1 << setw(10);
    }
    fout << " " <<endl;
    for(int i = 0; i < students; i++)
    {
        fout << "Student " << i + 1 << ": |";
        // For Each Column In the Row
        for(int j = 0; j < tests; j++)
        {
            fout << gradeBook[i][j] << left << setw(10) << " ";
        }
        fout << endl;
    }   
    fout.close();
    
}

void problem7()
{
    /* QUESTION 7: Create a program that allows the user to enter values 
     * into a vector. Keep the vector in sorted order as the values are 
     * being entered. Output the final vector.
     */

    vector<int> numbers;
    int value;
    int modifyInput;
    string modifyRepeat;

    // Start with an initial value               
    cout << "What Value Would You Like to Enter?: " << endl;
    cin >> value;
    cout << endl;

    // Store the value inside a vector
    numbers.push_back(value);

    cout << "Vector is:" << endl;
    output(numbers);

    do
    {
        cout << "Please Select From the Following Options:" << endl;
        cout << setfill('-') << setw(71) << " " << endl;
        cout << setfill(' ') << setw(40) << left << "(1): Add A Value"
                << "(2): Delete From a Location" << right << endl;
        cin >> modifyInput;
        switch(modifyInput)
        {
            case 1:
            {
                int addValue;
                cout << "What Value Would You Like to Add?:" << endl;
                cin >> addValue;

                numbers.push_back(addValue);
                selectionSort(numbers);
                cout << "Sorted Vector is:" << endl;
                output(numbers);
                break;
            }
            case 2:
            {
                int deleteLocation;
                cout << "C++ Starts at Zero Location" << endl;
                cout << "From What Location Would You Like to Delete?:" << endl;
                cin >> deleteLocation;

                removal(numbers, deleteLocation);

                selectionSort(numbers);
                cout << "New Vector is:" << endl;
                output(numbers);
                break;
            }
        }
        cout << "Would You Like To Try to Modify the Vector Again? (Y/N)" << endl;
        cin >> modifyRepeat;
    }
    while(tolower(modifyRepeat[0] == 'y'));
}