/*
 * Name: Marc Fleming
 * Student ID: 2858623
 * Date: 06/26/2022
 * HW: Final Project!
 * Problem: 2 Games
 * I certify this is my own work and code
 */

This is the two games that I have created for my final Project. The First Game is Tic-Tac-Toe. Having Multiple Options
in the game menu like the teacher requested, The user is able to decide between player vs player, player vs easyAI, 
player vs perfectAI, and can watch two easyAI's battle it out. the game will display the current state of the board after
every move, and will check for a three in a row as well before continuing on to the opposite player. The PerfectAI took me
some time to create, as I did not want to create a bunch of specific if else statements. Instead, the minimax algorithm is 
recursive, attempting the best possible scenarious for every scenario, and then the AI will decide what location to select,
due to that location being assigned an arbitrary value from the minimax. Whatever value is the largest is inherintly the best
decision for the AI To Choose. A save file was additionally created to display the user previous games and percentage win rates
for all game types.
The Other Game I made was called Dungeon Diver. This was difficult at some parts, but overall very enjoyable to code out.
Starting by creating an instance class of the Hero, the player is set in front of a dungeon, and must find the Grand Relic
to save his hometown. The player starts with health, spirit, and gold, and can lose or spend resources to progress further
in the dungeon. There is an Easter Egg in the game that makes it easier to defeat the dragon and get the highest score 
possible, can you find it? The game ends when either the Hero has 0 health or spirit, or when they locate the grand Relic.
If they win, the game will check their current inventory and assign a score based upon what they ended the game with.
There is also a save file of the current high score in the game.

Thank you for everything that you taught us this semester. I do hope you enjoy your summer and wish me luck as my 
apprenticeship begins towards the end of the summer!!!

Marc Fleming