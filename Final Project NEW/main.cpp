/*
 * Name: Marc Fleming
 * Student ID: 2858623
 * Date: 06/26/2022
 * HW: Final Project!
 * Problem: 2 Games
 * I certify this is my own work and code
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <windows.h>
#include <cmath>
#include <vector>

using namespace std;

// GLOBAL CONSTANTS OF THE SPACES IN THE TIC-TAC-TOE BOARD
const int ROWS = 3;
const int COLUMNS = 3;

// ALIAS
typedef char* CharPtr;

// STRUCTURES
struct AIMove
{
    int row;
    int column;
};

struct Hero
{
    int health = 10;
    int spirit = 10;
    int gold = 10; 
    string weapon[3] = {"","",""};
    int moves = 0;
};

struct Goblin
{
    int health = 2;
    int gold = 3;
}maxGoblins[4];

struct Spider
{
    int health = 3;
    int gold = 2;
}maxSpiders[4];

struct Dragon
{
    int health = 20;
    int gold = 200;
};

// PROTOTYPES
int mainMenu();
void screenClear();
void updateSaveFileTicTacToe(double totalGames, double gamesWon, double gamesMaster, double masterWins, double gamesEasy, double easyWins);
void displaySaveFile();
void readFromInput(istream& input);
void ticTacToeGameMenu();
void ticTacToeBoard(CharPtr* board);
void playerTurn(int& row, int& column, int turn);
void easyAITurn(int& row, int& column, int turn);
void changeBoard(int row, int column, CharPtr* board, int& turn);
void changeBoardAI(int row, int column, CharPtr* board, int& turn);
CharPtr* create2DArray(int ROWS, int COLUMNS);
void createSlots(CharPtr* board, int ROWS, int COLUMNS);
bool gameOver(const CharPtr* board, bool& draw);
void outputWinner(const int turn, const bool draw);
void outputWinnerEasyAI(const int turn, const bool draw);
void outputWinnerMasterAI(const int turn, const bool draw);
void outputWinnerAIvsAI(const int turn, const bool draw);
void playerVSPlayer();
void playerVSEasyAI();
void playerVSMasterAI();
void easyAIvsEasyAI();
bool anySpacesOpen(CharPtr* board);
int perfectAI(CharPtr* board);
int miniMax(CharPtr* board, int depth, bool isMax);
AIMove findBestMove(CharPtr* board);
void outputSave(const vector<string> lines, const vector<string>& scoring);

void dungeonDiverGameMenu();
void instructionsRules();
void dungeonDiverStart();
int pathChange(int num, Hero& player, vector<int>& dungeonRooms);
void theGrandRelic(Hero& player);
void manaDrainSpiders(Hero& player, vector<int>& dungeonRooms);
void theMarket(Hero& player, vector<int>& dungeonRooms);
void scareRoom(Hero& player, vector<int>& dungeonRooms);
void goblinsDen(Hero& player, vector<int>& dungeonRooms);
void emptyHallway(Hero& player, vector<int>& dungeonRooms);
void theDragonsLair(Hero& player, vector<int>& dungeonRooms);
bool gameOver(Hero& player);
void gameWin(Hero& player);
int getFromInput(istream& input);
int highScoreDungeon(int score);
void displayHighScore(int num);
void deleteNum(vector<int>& v, int deleteValue);
bool findAndDeleteNum(vector<int>& v, int findValue);






int main(int argc, char** argv)
{
    srand(time(0)); // SEED
    string doWhileMenuInput;
    
    do
    {
        int menuOption;
        menuOption = mainMenu();
        switch(menuOption)
        {
            case 1:
                ticTacToeGameMenu();
                break;
            case 2:
                dungeonDiverGameMenu();
                break;
            default:
            {
                cout << "You Selected an Invalid Response!" << endl;
            }
        }
        screenClear();
        cout << setfill('-') << setw(65) << " " << endl;
        cout << "Would You Like to Return To the Main Menu? Y/N?" << endl;
        cin >> doWhileMenuInput;
    }
    while(tolower(doWhileMenuInput[0]) == 'y');
    screenClear();
    cout << "Have a Wonderful Day!" << endl;

    return 0;
}
// END OF MAIN

/**
 * Function created for the menu. Prompts the user to select which assignment they
 * would like to run.
 * @return the user's option
 */
int mainMenu()
{
    int menuOption;
    //Here I am creating the Menu
    cout << "WELCOME TO THE GAME MENU!" << endl;
    cout << "Please Select From the Following Games:" << endl;
    cout << setfill('-') << setw(75) << " " << endl;
    cout << setfill(' ') << setw(40) << left << "(1): Tic-Tac-Toe"
            << "(2): Dungeon Diver" << right << endl;
    cin >> menuOption;

    return menuOption;
}

/**
 * Function created to clear the screen. Created this because clearing the screen
 * is necessary multiple times.
 */
void screenClear()
{
    // Clear the Screen, As requested from the professor (30 lines)
    cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" << endl;
}

/****************************************************************************************
 *                                                                                      *
 *                           FUNCTIONS FOR TIC-TAC-TOE                                  *
 *                                                                                      *
 ****************************************************************************************
 */

void ticTacToeGameMenu()
{
    screenClear();
    string doWhileMenuInput;
    int menuOption;
    do
    {
        cout << "WELCOME TO TIC-TAC-TOE!!!" << endl;
        cout << "Please Select From the Following Options:" << endl;
        cout << setfill('-') << setw(75) << " " << endl;
        cout << setfill(' ') << setw(40) << left << "(1): PLAYER VS PLAYER"
                << "(2): PLAYER VS EASY AI" << endl;
        cout << setw(40) << left << "(3): PLAYER VS MASTER AI" << "(4) EASY AI VS EASY AI" << right << endl;
        cout << setw(40) << left << "(5): GAMING HISTORY" << "(6) EXIT MENU" << right << endl;
        cin >> menuOption;

        switch(menuOption)
        {
            case 1:
                playerVSPlayer();
                break;
            case 2:
                playerVSEasyAI();
                break;
            case 3:
                playerVSMasterAI();
                break;
            case 4:
                easyAIvsEasyAI();
                break;
            case 5:
                displaySaveFile();
                break;
            case 6:
                break;
            default:
            {
                cout << "YOU HAVE ENTERED SOMETHING INVALID!!!" << endl;
                cout << "PLEASE TRY AGAIN" << endl;
            }
        }
        cout << setfill('-') << setw(65) << " " << endl;
        cout << "Would You Like to Return To the Tic-Tac-Toe Menu? Y/N?" << endl;
        cin >> doWhileMenuInput;
    }
    while(tolower(doWhileMenuInput[0]) == 'y');
    screenClear();
}

void updateSaveFileTicTacToe(double totalGames, double gamesWon, double gamesMaster, double masterWins, double gamesEasy, double easyWins)
{
    int totalWinPercent = 0;
    int masterAIWinPercent = 0;
    int easyAIWinPercent = 0;
    string line;
    string score;
    vector<string> lines;
    vector<string> scoring;
    double temp;
    ofstream fout;
    ifstream fin;
       
    fin.open("TicTacToeSave.dat");  
    // Error check the file//Create Initial File if none currently exists
    if(fin.fail())
    {
        // Create Initial File
        fout.open("TicTacToeSave.dat");
        fout << "TICTACTOE" << endl;
        fout << "Total Games Played: " << totalGames << endl;
        fout << "Total Games Player 1 Won: " << gamesWon << endl;
        fout << "Total Win Rate %: " << totalWinPercent << endl;
        fout << "\n" << endl;
        fout << "Total Games Played vs. Smart AI: " << gamesMaster << endl;
        fout << "Total Games Won vs. Smart AI: " << masterWins << endl;
        fout << "Win Rate % vs. Smart AI: " << masterAIWinPercent << endl;
        fout << "\n" << endl;
        fout << "Total Games Played vs. Dumb AI: " << gamesEasy << endl;
        fout << "Total Games Won vs. Dumb AI: " << easyWins << endl;
        fout << "Win Rate % vs. Dumb AI: " << easyAIWinPercent << endl;

        fout.close(); 

    }
    else // If the File Already Exists, We need to read all the information and update it.
    {
        string null;
        while(!fin.eof())
        {
            getline(fin, line, ':');
            lines.push_back(line);
            getline(fin, null, ' ');
            getline(fin, score, '\n');
            scoring.push_back(score); // What we care about is the numerical values and nothing else
        }
        fin.close();
        
        // Add values to the vector then rewrite the file        
        temp = stold(scoring[0]);
        totalGames = stod(scoring[0]) + totalGames;

        temp = stold(scoring[1]);
        gamesWon = stod(scoring[1]) + gamesWon;

        temp = stold(scoring[3]);
        gamesMaster = stod(scoring[3]) + gamesMaster;

        temp = stold(scoring[4]);
        masterWins = stod(scoring[4]) + masterWins;

        temp = stold(scoring[6]);
        gamesEasy = stod(scoring[6]) + gamesEasy;

        temp = stold(scoring[7]);
        easyWins = stod(scoring[7]) + easyWins;

        // Error Checking is needed in the case of 1/0 becoming undefined
        if(totalGames == 0 || gamesWon == 0)
        {
            totalWinPercent = 0;
        }
        else
        {
            totalWinPercent = gamesWon / totalGames * 100;
        }
        if(gamesMaster == 0 || masterWins == 0)
        {
            masterAIWinPercent = 0;
        }
        else
        {
            masterAIWinPercent = masterWins / gamesMaster * 100;
        }
        if(gamesEasy == 0 || easyWins == 0)
        {
            easyAIWinPercent == 0;
        }
        else
        {
            easyAIWinPercent = easyWins / gamesEasy * 100;
        }
        
        
        //Overwrite File
        fout.open("TicTacToeSave.dat");
        fout << "TICTACTOE" << endl;
        fout << "Total Games Played: " << totalGames << endl;
        fout << "Total Games Player 1 Won: " << gamesWon << endl;
        fout << "Total Win Rate %: " << setprecision(2) << fixed << totalWinPercent << endl;
        fout << "\n";
        fout << "Total Games Played vs. Smart AI: " << gamesMaster << endl;
        fout << "Total Games Won vs. Smart AI: " << masterWins << endl;
        fout << "Win Rate % vs. Smart AI: " <<  setprecision(2) << fixed << masterAIWinPercent << endl;
        fout << "\n";
        fout << "Total Games Played vs. Dumb AI: " << gamesEasy << endl;
        fout << "Total Games Won vs. Dumb AI: " << easyWins << endl;
        fout << "Win Rate % vs. Dumb AI: " << setprecision(2) << fixed << easyAIWinPercent << endl;

        fout.close();  
    }
}

void displaySaveFile()
{
    screenClear();
    ifstream fin;
    fin.open("TicTacToeSave.dat");

    // Error check the file
    if(fin.fail())
    {
        cout << "No Games Have Been Played Yet!!! There Is No Data To Display." << endl;
    }
    else
    {
        while(!fin.eof())
        {
            readFromInput(fin);
        }
        cout << "\n\n" << endl;
    }
}

/**
 * Function to read values from input. Takes input, appends to value, then displays 
 * value to screen
 * @param input whatever input is to be read
 */
void readFromInput(istream& input)
{
    string line;
    input >> line;

    while ( getline (input,line) )
    {
      cout << line << '\n';
    }
}

void playerVSPlayer()
{
    screenClear();
    int row, column;
    int turn = (rand() % 2) + 1; // Randomize who starts
    bool draw = false;

    CharPtr* board = create2DArray(ROWS, COLUMNS);
    createSlots(board, ROWS, COLUMNS);
    cout << "Creating The Tic-Tac-Toe board..." << endl;
    ticTacToeBoard(board);
    Sleep(1000);
    do
    {
        cout << "Player 1 is X, Player 2 is O." << endl;
        playerTurn(row, column, turn);
        changeBoard(row, column, board, turn);
        ticTacToeBoard(board);
    }
    while(gameOver(board, draw) == 0);

    //If the loop ends, there was either a winner or a draw
    outputWinner(turn, draw);

    // Deallocation, inner first
    for(int i = 0; i < ROWS; i++)
    {
        delete[] board[i];
    }

    // Delete outer
    delete[] board;
}

void playerVSEasyAI()
{
    screenClear();
    int location;
    int row, column;
    int turn = (rand() % 2) + 1; // Randomize who starts
    string input;
    bool draw = false;

    CharPtr* board = create2DArray(ROWS, COLUMNS);
    createSlots(board, ROWS, COLUMNS);
    cout << "Creating The Tic-Tac-Toe board..." << endl;
    ticTacToeBoard(board);
    Sleep(1000);
    do
    {
        cout << "Player 1(Human) is X, Player 2(AI) is O." << endl;
        if(turn == 1)
        {
            playerTurn(row, column, turn);
            changeBoard(row, column, board, turn);
        }
        else //(turn == 2)
        {
            // Select a random location and make sure it is not already taken 
            cout << "AI's Turn..." << endl;
            Sleep(1000);
            easyAITurn(row, column, turn);
            changeBoardAI(row, column, board, turn);
        }
        ticTacToeBoard(board);
    }
    while(gameOver(board, draw) == 0);

    //If the loop ends, there was either a winner or a draw
    outputWinnerEasyAI(turn, draw);

    // Deallocation, inner first
    for(int i = 0; i < ROWS; i++)
    {
        delete[] board[i];
    }

    // Delete outer
    delete[] board;
}

void playerVSMasterAI()
{
    screenClear();
    int location;
    int row, column;
    int turn = (rand() % 2) + 1; // Randomize who starts
    string input;
    bool draw = false;

    CharPtr* board = create2DArray(ROWS, COLUMNS);
    createSlots(board, ROWS, COLUMNS);
    cout << "Creating The Tic-Tac-Toe board..." << endl;
    ticTacToeBoard(board);
    Sleep(1000);
    do
    {
        cout << "Player 1(Human) is X, Player 2(AI) is O." << endl;
        if(turn == 1)
        {
            playerTurn(row, column, turn);
            changeBoard(row, column, board, turn);
        }
        else //(turn == 2)
        {
            // Use the MiniMax Function to Locate the best possible value
            cout << "AI's Turn..." << endl;
            Sleep(1000);

            AIMove bestMove = findBestMove(board);
            
            cout << "The Optimal Move is :" << endl;
            cout << bestMove.row << bestMove.column << endl;
            
            row = bestMove.row;
            column = bestMove.column;
            
            changeBoardAI(row, column, board, turn);
        }
        ticTacToeBoard(board);
    }
    while(gameOver(board, draw) == 0);

    //If the loop ends, there was either a winner or a draw
    outputWinnerMasterAI(turn, draw);

    // Deallocation, inner first
    for(int i = 0; i < ROWS; i++)
    {
        delete[] board[i];
    }

    // Delete outer
    delete[] board;
}

void easyAIvsEasyAI()
{
    screenClear();
    int location;
    int row, column;
    int turn = (rand() % 2) + 1; // Randomize who starts
    string input;
    bool draw = false;

    CharPtr* board = create2DArray(ROWS, COLUMNS);
    createSlots(board, ROWS, COLUMNS);
    cout << "Creating The Tic-Tac-Toe board..." << endl;
    ticTacToeBoard(board);
    Sleep(1000);
    do
    {
        cout << "Player 1(AI) is X, Player 2(AI) is O." << endl;
        if(turn == 1)
        {
            // Select a random location and make sure it is not already taken 
            cout << "AI 1's Turn..." << endl;
            Sleep(1000);
            easyAITurn(row, column, turn);
            changeBoardAI(row, column, board, turn);
        }
        else //(turn == 2)
        {
            // Select a random location and make sure it is not already taken 
            cout << "AI 2's Turn..." << endl;
            Sleep(1000);
            easyAITurn(row, column, turn);
            changeBoardAI(row, column, board, turn);
        }
        ticTacToeBoard(board);
    }
    while(gameOver(board, draw) == 0);

    //If the loop ends, there was either a winner or a draw
    outputWinnerAIvsAI(turn, draw);

    // Deallocation, inner first
    for(int i = 0; i < ROWS; i++)
    {
        delete[] board[i];
    }

    // Delete outer
    delete[] board;
}

/**
 * Function to create a 2D dynamic array for the tic-tac-toe board.
 * @param ROWS 3 rows in a board
 * @param COLUMNS 3 columns in a board
 * @return the 2d array
 */
CharPtr* create2DArray(int ROWS, int COLUMNS)
{
    // Outside Dynamic Array
    CharPtr* ticTacToeBoard = new CharPtr[ROWS];

    // Inside Dynamic Array
    for(int i = 0; i < ROWS; i++)
    {
        ticTacToeBoard[i] = new char[COLUMNS];
    }
    return ticTacToeBoard;
}

/**
 * Function to Output the current tic-tac-toe board. Uses the dynamic Array to display current state
 * @param board the 2d array
 */
void ticTacToeBoard(CharPtr* board)
{
    cout << "     |     |     \n";
    cout << "  " << board[0][0] << "  |  " << board[0][1] << "  |  " << board[0][2] << " \n";
    cout << "_____|_____|_____\n";
    cout << "     |     |     \n";
    cout << "  " << board[1][0] << "  |  " << board[1][1] << "  |  " << board[1][2] << " \n";
    cout << "_____|_____|_____\n";
    cout << "     |     |     \n";
    cout << "  " << board[2][0] << "  |  " << board[2][1] << "  |  " << board[2][2] << " \n";
    cout << "     |     |     \n";
}

/**
 * Function to get the location of O or X of the user's choice
 * @param location the chosen location
 * @param row the row of the board
 * @param column the column of the board
 */
void playerTurn(int& row, int& column, int turn)
{
    int location;
    cout << "It is Player " << turn << "'s Turn." << endl;
    cout << "What location would you like to take?" << endl;
    cin >> location;
    switch(location)
    {
        case 1:
            row = 0;
            column = 0;
            break;
        case 2:
            row = 0;
            column = 1;
            break;
        case 3:
            row = 0;
            column = 2;
            break;
        case 4: row = 1;
            column = 0;
            break;
        case 5:
            row = 1;
            column = 1;
            break;
        case 6:
            row = 1;
            column = 2;
            break;
        case 7:
            row = 2;
            column = 0;
            break;
        case 8:
            row = 2;
            column = 1;
            break;
        case 9:
            row = 2;
            column = 2;
            break;
        default: // Error Checking
            cout << "INVALID MOVE" << endl;
            playerTurn(row, column, turn);
    }
}

/**
 * Simple function for the easy AI to randomly select a location on the board. If
 * the location is already taken, will select a different option
 * @param row the row of the board
 * @param column the column of the board
 * @param turn who's turn it is
 */
void easyAITurn(int& row, int& column, int turn)
{
    int location;
    location = (rand() % 9) + 1;
    switch(location)
    {
        case 1:
            row = 0;
            column = 0;
            break;
        case 2:
            row = 0;
            column = 1;
            break;
        case 3:
            row = 0;
            column = 2;
            break;
        case 4: row = 1;
            column = 0;
            break;
        case 5:
            row = 1;
            column = 1;
            break;
        case 6:
            row = 1;
            column = 2;
            break;
        case 7:
            row = 2;
            column = 0;
            break;
        case 8:
            row = 2;
            column = 1;
            break;
        case 9:
            row = 2;
            column = 2;
            break;
        default: // Error Checking
            easyAITurn(row, column, turn);
    }
}

/**
 * Function to change the board and add an X or an O depending on whose turn it is
 * A lot of error checking is needed to prevent overwriting and other errors
 * @param row the selected row
 * @param column and the column space
 * @param board the dynamic array that holds the X's and O'x
 * @param turn Who's turn it is
 */
void changeBoard(int row, int column, CharPtr* board, int& turn)
{
    if(turn == 1 && board[row][column] != 'X' && board[row][column] != 'O')
    {
        board[row][column] = 'X';
        turn++;
    }
    else if(turn == 2 && board[row][column] != 'X' && board[row][column] != 'O')
    {
        board[row][column] = 'O';
        turn--;
    }
    else // If the space is already taken
    {
        cout << "ERROR: SPACE HAS ALREADY BEEN TAKEN, PLEASE TRY AGAIN!!!" << endl;
        playerTurn(row, column, turn);
        changeBoard(row, column, board, turn);
    }
}

/**
 * Function to change the board and add an X or an O depending on whose turn it is
 * A lot of error checking is needed to prevent overwriting and other errors
 * ADDED NOTE: This Function Also Works With The Perfect AI, As It Preselects the Exact
 * Best Location And Will Never Reach the 'else' statement
 * @param row the selected row
 * @param column and the column space
 * @param board the dynamic array that holds the X's and O'x
 * @param turn Who's turn it is
 */
void changeBoardAI(int row, int column, CharPtr* board, int& turn)
{
    if(turn == 1 && board[row][column] != 'X' && board[row][column] != 'O')
    {
        board[row][column] = 'X';
        turn++;
    }
    else if(turn == 2 && board[row][column] != 'X' && board[row][column] != 'O')
    {
        board[row][column] = 'O';
        turn--;
    }
    else // If the space is already taken
    {
        easyAITurn(row, column, turn);
        changeBoardAI(row, column, board, turn);
    }
}

/**
 * Function to represent the locations of the tic-tac-toe board. Each space will be assigned 
 * a number
 * @param board the 2D board 
 * @param ROWS 3 rows in the board
 * @param COLUMNS 3 columns in the board
 */
void createSlots(CharPtr* board, int ROWS, int COLUMNS)
{
    // First the Rows
    for(int i = 0; i < ROWS; i++)
    {
        // Then the Columns
        for(int j = 0; j < COLUMNS; j++)
        {
            board[i][j] = i * COLUMNS + j + '1';
        }
    }
}

/**
 * Function to determine if that game has ended or not. First will check for 3 in 
 * a rows, if none are found will check for any empty slots in the board. if the board is full,
 * or the function finds a 3 in a row, will return true and the game will end.
 * @param board the 2D board
 * @param draw bool variable to change if the game is a draw
 * @return true if the game is over, false if the game is going to continue
 */
bool gameOver(const CharPtr* board, bool& draw)
{
    // Checks for a three of a kind in a row or a column. Need to figure out diagonal.
    for(int i = 0; i < 3; i++)
    {
        if(board[i][0] == board[i][1] && board[i][0] == board[i][2]
                || board[0][i] == board[1][i] && board[0][i] == board[2][i])
        {
            return true;
        }
    }
    // Checks for diagonal wins
    if(board[0][0] == board[1][1] && board[1][1] == board[2][2]
            || board[0][2] == board[1][1] && board[1][1] == board[2][0])
    {

        return true;
    }
    // Check if the board is full or not. if the board is full change the bool draw to true
    for(int row = 0; row < 3; row++)
    {
        for(int column = 0; column < 3; column++)
        {
            if(board[row][column] != 'X' && board[row][column] != 'O')
            {
                return false;
            }
        }
    }
    // If the code reaches this point, that means that there was no 3 of a kind and the board is full
    draw = true;

    return true;
}

/**
 * Function to output whoever won the game, or a draw if that is the case
 * @param turn whichever player the game ended on
 * @param draw bool statement of whether the game is a draw or not
 */
void outputWinner(const int turn, const bool draw)
{
    if(turn == 2 && draw == false)
    {
        cout << "Player 1 Wins!!!" << endl;
        updateSaveFileTicTacToe(1, 1, 0, 0, 0, 0);
    }
    else if(turn == 1 && draw == false)
    {
        cout << "Player 2 Wins!!!" << endl;
        updateSaveFileTicTacToe(1, 0, 0, 0, 0, 0);
    }
    else
    {
        cout << "Board Full!! Nobody Wins!!!" << endl;
        //A Draw Is not a win
        updateSaveFileTicTacToe(1, 0, 0, 0, 0, 0);
    }
}

/**
 * Function to output whoever won the game, or a draw if that is the case
 * @param turn whichever player the game ended on
 * @param draw bool statement of whether the game is a draw or not
 */
void outputWinnerAIvsAI(const int turn, const bool draw)
{
    if(turn == 2 && draw == false)
    {
        cout << "Player 1 Wins!!!" << endl;
    }
    else if(turn == 1 && draw == false)
    {
        cout << "Player 2 Wins!!!" << endl;
    }
    else
    {
        cout << "Board Full!! Nobody Wins!!!" << endl;
    }
}

/**
 * Function to output whoever won the game, or a draw if that is the case
 * @param turn whichever player the game ended on
 * @param draw bool statement of whether the game is a draw or not
 */
void outputWinnerEasyAI(const int turn, const bool draw)
{
    if(turn == 2 && draw == false)
    {
        cout << "Player 1 Wins!!!" << endl;
        updateSaveFileTicTacToe(1, 1, 0, 0, 1, 1);
    }
    else if(turn == 1 && draw == false)
    {
        cout << "AI Wins!!!!" << endl;
        updateSaveFileTicTacToe(1, 0, 0, 0, 1, 0);
    }
    else
    {
        cout << "Board Full!! Nobody Wins!!!" << endl;
        updateSaveFileTicTacToe(1, 0, 0, 0, 1, 0);
    }
}

/**
 * Function to output whoever won the game, or a draw if that is the case
 * @param turn whichever player the game ended on
 * @param draw bool statement of whether the game is a draw or not
 */
void outputWinnerMasterAI(const int turn, const bool draw)
{
    if(turn == 2 && draw == false)
    {
        cout << "Player 1 Wins!!!" << endl;
        updateSaveFileTicTacToe(1, 1, 1, 1, 0, 0);
    }
    else if(turn == 1 && draw == false)
    {
        cout << "AI Wins!!!!" << endl;
        updateSaveFileTicTacToe(1, 0, 1, 0, 0, 0);
    }
    else
    {
        cout << "Board Full!! Nobody Wins!!!" << endl;
        updateSaveFileTicTacToe(1, 0, 1, 0, 0, 0);
    }
}

/**
 * Checks if there are any open spaces on the board for the AI to test. 
 * @param board The Tic-Tac-Toe Board
 * @return true if there are open spaces, false if not
 */
bool anySpacesOpen(CharPtr* board)
{
    for(int row = 0; row < 3; row++)
    {
        for(int column = 0; column < 3; column++)
        {
            if(board[row][column] != 'X' && board[row][column] != 'O')
                return true;
        }
    }
    return false;
}

/**
 * Function That guarantee the AI will select the winning option if it's available
 * @param board the Tic-Tac-Toe Board
 * @return Positive 10 if the game will result in a win, negative ten if the game will result in a loss
 */
int perfectAI(CharPtr* board)
{
    // Check Rows First To Look For a Win
    for(int row = 0; row < 3; row++)
    {
        if(board[row][0] == board[row][1] && board[row][1] == board[row][2])
        {
            if(board[row][0] == 'O')
                return +10;
            else if(board[row][0] == 'X')
                return -10;
        }
    }

    // Check Columns Next
    for(int column = 0; column < 3; column++)
    {
        if(board[0][column] == board[1][column] && board[1][column] == board[2][column])
        {
            if(board[0][column] == 'O')
                return +10;

            else if(board[0][column] == 'X')
                return -10;
        }
    }

    // Checking for Wins Diagonally
    if(board[0][0] == board[1][1] && board[1][1] == board[2][2])
    {
        if(board[0][0] == 'O')
            return +10;
        else if(board[0][0] == 'X')
            return -10;
    }

    if(board[0][2] == board[1][1] && board[1][1] == board[2][0])
    {
        if(board[0][2] == 'O')
            return +10;
        else if(board[0][2] == 'X')
            return -10;
    }

    return 0;
}

/**
 * The Legendary MiniMax function. Recursively will check every possible scenario from
 * the current state of the board and will select the best possible move for the AI
 * @param board the Tic-Tac-Toe Board
 * @param depth how many moves are in the game tree
 * @param isMax alternating between minimizer and maximizer to minimize losses and maximize wins
 * @return the best possible choice
 */
int miniMax(CharPtr* board, int depth, bool isMax)
{
    int score = perfectAI(board);
    
    // If AI Has Won The Game
    if(score == 10)
        return score;

    // If The User Has Won
    if(score == -10)
        return score;
    
    // In the Case of A Tie
    if(anySpacesOpen(board) == false)
        return 0;
    
    // If Its the Maximizer's Move(AI)
    if(isMax)
    {
        int best = -100000;
        // Check Every Space
        for(int row = 0; row < 3; row++)
        {
            for(int column = 0; column < 3; column++)
            {
                // If The Space is Empty
                if(board[row][column] != 'X' && board[row][column] != 'O')
                {
                    // Test the move
                    board[row][column] = 'O';

                    // Call minimax recursively and choose max value
                    best = max(best, miniMax(board, depth + 1, !isMax));

                    // Undo the move
                    board[row][column] = row * COLUMNS + column + '1';
                }
            }
        }
        return best;
    }

    // If It's the Minimizer's move(Player)
    else
    {
        int best = 1000;

        // Check Every Slot
        for(int row = 0; row < 3; row++)
        {
            for(int column = 0; column < 3; column++)
            {
                // Check if cell is empty
                if(board[row][column] != 'X' && board[row][column] != 'O')
                {
                    // Make the move
                    board[row][column] = 'X';

                    // Call minimax recursively and choose
                    // the minimum value
                    best = min(best, miniMax(board, depth + 1, !isMax));

                    // Undo the move
                    board[row][column] = row * COLUMNS + column + '1';
                }
            }
        }
        return best;
    }
}

/**
 * Function to find the best possible move in a tic-tac-toe game. Iterates through every
 * empty location on the board, then initiates the MiniMax algorithm recursively until the best
 * possible location is found
 * @param board the tic-tac-toe board
 * @return the structure of bestMove which includes both row and column
 */
AIMove findBestMove(CharPtr* board)
{
    int bestVal = -1000;
    AIMove bestMove;
    bestMove.row = -1;
    bestMove.column = -1;
      
    // Check every slot, run miniMax through every possible move, then chose the best one
    for(int row = 0; row < 3; row++)
    {
        for(int column = 0; column < 3; column++)
        {
            // Check if cell is empty
            if(board[row][column] != 'X' && board[row][column] != 'O')
            {
                // Test the move
                board[row][column] = 'O';
 
                // Evaluate the move
                int moveVal = miniMax(board, 0, false);

                // Undo the move
                board[row][column] = row * COLUMNS + column + '1';

                // If the AI Found A Better Move, Update
                if(moveVal > bestVal)
                {
                    bestMove.row = row;
                    bestMove.column = column;
                    bestVal = moveVal;
                }
            }
        }
    }

    return bestMove;
}

/**
 * Function To Display The Save File in it's current state
 * @param lines the text lines
 * @param scoring the value in each line
 */
void outputSave(const vector<string> lines, const vector<string>& scoring)
{
    for(int i = 0; i < lines.size()-1; i++)
    {
        cout << lines[i] << " " << scoring[i] << endl;
    }
    cout << endl;
}

/****************************************************************************************
 *                                                                                      *
 *                           FUNCTIONS FOR DUNGEON DIVER                                *
 *                                                                                      *
 ****************************************************************************************
 */

/**
 * Function For the Main Menu of Dungeon Diver.
 */
void dungeonDiverGameMenu()
{
    screenClear();
    string doWhileMenuInput;
    int menuOption;
    do
    {
        cout << "WELCOME TO DUNGEON DIVER!!!!!!" << endl;
        cout << "Please Select From the Following Options:" << endl;
        cout << setfill('-') << setw(75) << " " << endl;
        cout << setfill(' ') << setw(40) << left << "(1): Start New Game"
                << "(2): Instructions/Rules" << endl;
        cout << setw(40) << "(3): High Scores" << "(4): Exit Game" << right << endl;
        cin >> menuOption;

        switch(menuOption)
        {
            case 1:
                dungeonDiverStart();
                break;
            case 2:
                instructionsRules();
                break;
            case 3:
                displayHighScore(0);
                break;
            case 4:
            {
                break;
            }
            default:
            {
                cout << "YOU HAVE ENTERED SOMETHING INVALID!!!" << endl;
                cout << "PLEASE TRY AGAIN" << endl;
            }
        }
        cout << setfill('-') << setw(65) << " " << endl;
        cout << "Would You Like to Return To the Dungeon Diver Menu? Y/N?" << endl;
        cin >> doWhileMenuInput;
    }
    while(tolower(doWhileMenuInput[0]) == 'y');
    Sleep(100);
}

/**
 * Instructions and Ruleset to display if the User want's to see them
 */
void instructionsRules()
{
    screenClear();
    cout << "Dungeon Diver Is An Option Based Dungeon Game. The Objective Of The Game\n" 
            << "Is To Explore The Dungeon In Search Of The Grand Relic To Save Your Corrupted\n"
            << "Hometown. Easier Said Than Done, The Dungeon Itself Is Alive, Twisting And Turning,\n"
            << "Changing With Every Decision You Make.\n" << endl;
    // Allow the Player Time to Read
    Sleep(7500);
    cout << "You (The Hero), Begin The Dungeon With These Three Resources:\n" << endl;
    cout << "10 HEALTH '<3'" << endl;
    cout << "10 SPIRIT ' %' " << endl;
    cout << "10 GOLD   ' @' " << endl;
    cout << "During The Game You Will Spend (or lose) Resources to Progress You Further In The Dungeon.\n"
            << "If Your Health Or Spirit Are Reduced To Zero, You Lose The Game And Your Village Is Destroyed.\n"
            << "Complete The Dungeon In The Least Amount Of Moves With The Greatest Amount Of Resources To Get\n"
            << "The High Score!!!!" << endl;
    cout << "NOTE: All Responses in the game can be shorthanded (i.e. Left Or Right? can be answered with as little as 'l',\n"
            << "but full text works as well)" << endl;
}

/**
 * Opening Game. Will Generate First Room and Starting Point for the Hero
 */
void dungeonDiverStart()
{
    screenClear();
    /* Create A Vector That Contains All the Possible Rooms In the Dungeon. This is 
     * Important because After the Hero Enters A Room, Under Most Circumstances The Hero
     * Should Not Encounter That Room Again. Imagine Running Into 4 Dragons in a row?
     */
    vector <int> dungeonRooms{1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 7};
    
    Hero player;// Create An Instance Of The Hero.
    string noises1[4] = {"Faint Cry", "Distant Howl", "Lumbering Growl", "Strange Chittering"};
    string noises2[4] = {"Withered Branches", "Old Dried Bones", "Old Scrolls", "Large Cobwebs"};
    string path;
    int i = (rand() % 4);
    cout << "You Walk Up The Old Stone Path, Leading To The Entrance. A " << noises1[i] << " Echos From Inside The Dungeon.\n"
            << "Pushing Aside Some " << noises2[i] << ", You Step Inside. And As If To Greet You, The Sounds Grow Louder.\n"
            << "The Doors Slam Shut Behind You. This Dungeon Has Been Cursed, And You Are the Last Hope For Your\n"
            << "Village. Be Vigilant Hero. Your Journey Has Just Begun." << endl;
    cout << "Two Paths Lay Ahead Of You Hero. Left Or Right?" << endl;
    do
    {
        cin >> path;
        path = tolower(path[0]);
        if(path == "l")
        {
            pathChange(+2, player, dungeonRooms); 
        }
        else if(path == "r")
        {
            pathChange(-2, player, dungeonRooms);
        }
        else // Error Checking
        {
            cout << "ERROR!!! YOU ENTERED SOMETHING INVALID, PLEASE TRY AGAIN" << endl;
        }
    }
    while(path != "l" && path != "r");
}

int pathChange(int num, Hero& player, vector<int>& dungeonRooms)
{
    player.moves++; // Tracker of rooms entered
    if(gameOver(player) == true)
    {
        cout << "YOU LOSE" << endl;
        return 0;
    }
    else
    {
        // Randomize Calculation and Then locate
        int nextRoom = (rand() % dungeonRooms.size());
        //Error Checking
        if(nextRoom != 0)
        {
            nextRoom = nextRoom + num;
            nextRoom = abs(nextRoom);
        }
        nextRoom = dungeonRooms[nextRoom];

        // Run A Check To See If The Room Has Been Entered Before Or Not
        if(findAndDeleteNum(dungeonRooms, nextRoom) == true)
        {
            switch(nextRoom)
            {
                case 1:
                    theGrandRelic(player);
                    break;
                case 2:
                    scareRoom(player, dungeonRooms);
                    break;
                case 3:
                    emptyHallway(player, dungeonRooms);
                    break;
                case 4:
                    goblinsDen(player, dungeonRooms);
                    break;
                case 5:
                    theMarket(player, dungeonRooms);
                    break;
                case 6:
                    manaDrainSpiders(player, dungeonRooms);
                    break;
                case 7:
                    theDragonsLair(player, dungeonRooms);
                    break;
                default:
                    pathChange(num, player, dungeonRooms);
                    break;
            }
        }
        else // This Was Made For Error Checking, But I Created A Function to Do that, So This Should never occur
        {
            cout << "That Room Has Already Been Entered!" << endl;
            pathChange(num, player, dungeonRooms);
        }
    }
    return 0;
}

void theGrandRelic(Hero& player)
{
    screenClear();
    cout << "Finally, You Reach A Glimmering Room That Stands Out Amongst The Rest. The Doors Shine,\n"
            "As If Calling Out To You, Whispering You Can Finally Rest. Pushing Open The Doors You See It.\n"
            "The Grand Relic. Grasping A Hold Of The Holy Weapon, You Feel A Powerful Sense Of Serenity.\n"
            "Your Journey Has Reached It's Conclusion. The Village Is Saved!!!!\n\n" << endl;
    
    cout << "******************************************************************************************\n"
            "*                                                                                        *\n"
            "*                                      THE END                                           *\n"
            "*                                                                                        *\n"
            "******************************************************************************************\n" << endl;
    
    // Create End Of Game Function
    gameWin(player);
}

void scareRoom(Hero& player, vector<int>& dungeonRooms)
{
    screenClear();
    string action[4] = {"Moving Away Some Cobwebs", "Attempting To Light One Of The Torches On The Wall", "Stumbling Over A Skull On The Floor", "Unable To See Your Path"};
    string scare[4] = {"A Few Large Spiders Land On Your Face", "A Ghost Appears Through The Wall", "A Geist Appears From The Floor", "You Stumble Over A Large Object"};
    int i = (rand() % 4);
    string choice;
    string path;
    
    cout << "You Find Yourself In a Dimly Lit Room. Faded Paintings Can Barely Be Seen Amidst the Darkness. \n"
            << action[i] << ", You Move Forward, When Suddenly " << scare[i] << "." << endl;
    if(i == 3)//Treasure Chest Or Sleeping Zombie?
    {
        // Zombie is 1, Treasure Chest is 2
        int trickOrTreat = (rand() % 2) + 1;
        
        cout << "The Large Object Is Covered By A Thick Hide Of Some Large Animal. You Can't Make Out What You Just Tripped Over.\n"
                << "Remove The Cover? (YES/NO) " << endl;
        cin >> choice;
        choice = tolower(choice[0]);
        if(choice == "y" && trickOrTreat == 1)
        {
            cout << "You Count To Three..." << endl;
            cout << "1..." << endl;
            // Dramatic Pause
            Sleep(1000);
            cout << "2..." << endl;
            Sleep(1000);
            cout << "'THREE!' Yanking Off The Cover, You Hear A Low Growl, Suddenly Getting Much Louder.\n"
                    << "In The Darkness You Attempt To Defend Yourself, But You Cannot See Where The Attacks Are Coming From.\n"
                    << "You Manage To Defeat The Undead Horror, But Not Without Some Inflictions...\n" 
                    << "BITTEN: LOSE 2 HEALTH" << endl;
            player.health = player.health - 2;
            cout << player.health << " HEALTH REMAINING" << endl;
            //Check If the Player Died
            if(gameOver(player) == true)
            {
                cout << "You Look At The Bite. The Wound Has Become Quickly Infected. Black Lines Spread From The Bite Marks\n"
                        << "First, You Feel The Worst Migraine You've Ever Had, Then, You Feel The Hunger....You Can Feel Your\n"
                        << "Body Turning Quickly. Scared, And To Prevent The Same Fate To The Next Weary Traveler, You Take Your Dagger And...."<< endl;
            }
        }
        else if(choice == "y" && trickOrTreat == 2)
        {
            cout << "You Count To Three..." << endl;
            cout << "1..." << endl;
            // Dramatic Pause
            Sleep(1000);
            cout << "2..." << endl;
            Sleep(1000);
            cout << "'THREE!' Yanking Off The Cover, You Can Barely Make Out The Outline Of What Appears To Be An Old Wooden Chest\n"
                    << "Prying It Open, You Discover The Chest Full Of Valuables!!!\n"
                    << "JACKPOT!!! GAIN 5 GOLD" << endl;
            player.gold = player.gold + 5;
            cout << player.gold << " GOLD REMAINING" << endl;  
        }
        else // If the User Chooses To Leave The Object Alone
        {
            cout << "Determining That Sometimes No Action Is The Best Action, You Turn And Slowly Make Your Way Out Of The Room" << endl;
        }
    }
    else // If There Isn't a large object in the room
    {
        int courageOrCoward = (rand() % 2) + 1;
        if(courageOrCoward == 1) // COURAGE
        {
            cout << "Shaking Off The Encounter, You Press On..." << endl;
        }
        else // COWARD
        {
            cout << "Terrified, You Lose Your Composure, And Your Spirit Along With It." << endl;
            cout << "UNNERVED: LOSE 2 SPIRIT" << endl;
            player.spirit = player.spirit - 2;
            cout << player.spirit << " SPIRIT REMAINING" << endl;
            if(gameOver(player) == true)
            {
                cout << "Your Spirit Is Crushed, Your Hopes and Dreams Broken. You No Longer Have The Will To Carry On. Has Everything\n"
                        << "Seemed So Bleak Before This? Is This All There Is To Your Journey??? You Lay Down, Fearful, But Partially Hopeful,\n"
                        << "That You Never Wake Again" << endl;
            }
        }
    }

    if(gameOver(player) == true)
    {
        cout << "YOU LOSE" << endl;
    }
    else
    {
        cout << "The Room Explored, You Have 3 Doors Before You...\n"
                << "LEFT, STRAIGHT, Or RIGHT?" << endl;
        do
        {
            cin >> path;
            path = tolower(path[0]);
            if(path == "l")
            {
                pathChange(0, player, dungeonRooms);
            }
            else if(path == "r")
            {
                pathChange(-1, player, dungeonRooms);
            }
            else if(path == "s")
            {
                pathChange(1, player, dungeonRooms);
            }
            else // Error Checking
            {
                cout << "ERROR!!! YOU ENTERED SOMETHING INVALID, PLEASE TRY AGAIN" << endl;
            }
        }
        while(path != "l" && path != "r" && path != "s");
    }
}

void theMarket(Hero& player, vector<int>& dungeonRooms)
{
    screenClear();
    string path;
    int buy;
    int broke; // Checker To See if the Hero has enough gold for the item
    cout << "Coming Into A Pungent And Dim Room, You Hear The Sounds Of An Old Man Mumbling To Himself.\n"
            << "He Turns And Notices You." << endl;
    cout << "'YOUNG HERO, DO YOU HAVE ANY GooOOOLLLDDDD?!?!?! ME GOTS SOME NICE THINGS FOR YA IF YA GOTS\n"
            << "ANY gOOooOoOLLD!!!!'\n" << endl;
    cout << "He Shows You His Wares. Shockingly Enough, He Actually Has Some Useful Items:" << endl;

    do
    {
        cout << "WHAT WOULD YOU LIKE TO BUY??? (1-4)" << endl;
        cout << setfill('-') << setw(65) << " " << setfill(' ') << endl;
        cout << "(1): HEALTH POTION (restores 4 health) 5 GOLD\n"
                << "(2): MANA POTION (restores 3 spirit) 4 GOLD\n"
                << "(3): LONG SWORD (useful for battles) 10 GOLD\n"
                << "(4): STRANGE AMULET (????) 15 GOLD\n"
                << "(5): nothing (walk away)\n" << endl;
        cin >> buy;
        if(buy == 1)
        {
            broke = player.gold - 5;
            if(broke >= 0)
            {
                player.gold = player.gold - 5;
                player.health = player.health + 4;
                cout << "You Take A Nice Long Drink Of the Potion. Feeling Completely Restored, You Continue Your Journey" << endl;
                cout << "HEALTH RESTORED: GAIN 4 HEALTH" << endl;
                cout << "You Have " << player.health << " Health Now." << endl;
            }
            else
            {
                cout << "'ARE YA TRYNA SCAM ME KIDDO?!?!? I CAN CLEARLY SEE YA DONT HAVE THE\n"
                        << "GoOoOOLLD FOR THIS!" << endl;
                // Change Value of Buy to make the program loop again
                buy = 10000;
            }
        }
        else if(buy == 2)
        {
            broke = player.gold - 4;
            if(broke >= 0)
            {
                player.gold = player.gold - 4;
                player.spirit = player.spirit + 3;
                cout << "You Take A Nice Long Drink Of the Potion. Feeling Completely Restored, You Continue Your Journey" << endl;
                cout << "SPIRIT RESTORED: GAIN 3 SPIRIT" << endl;
                cout << "You Have " << player.spirit << " Spirit Now." << endl;
            }
            else
            {
                cout << "'ARE YA TRYNA SCAM ME KIDDO?!?!? I CAN CLEARLY SEE YA DONT HAVE THE\n"
                        << "GoOoOOLLD FOR THIS!" << endl;
                // Change Value of Buy to make the program loop again
                buy = 100000;
            }
        }
        else if(buy == 3)
        {
            broke = player.gold - 10;
            if(broke >= 0)
            {
                player.gold = player.gold - 10;
                // Ya Only Get One Long Sword
                int entry = 0;
                int loc = 0;
                do
                {
                    if(player.weapon[loc] == "")
                    {
                        player.weapon[loc] = "LONG SWORD";
                        cout << "Holding The Weapon In Your Hand You Feel Quite Powerful. Sheathing It, You Continue Your Journey" << endl;
                        entry++;
                    }
                    loc++;
                }
                while(entry != 1);
            }
            else
            {
                cout << "'ARE YA TRYNA SCAM ME KIDDO?!?!? I CAN CLEARLY SEE YA DONT HAVE THE\n"
                        << "GoOoOOLLD FOR THIS!" << endl;
                // Change Value of Buy to make the program loop again
                buy = -1;
            }
        }
        else if(buy == 4)
        {
            broke = player.gold - 15;
            if(broke >= 0)
            {
                player.gold = player.gold - 15;
                // Ya Only Get One Strange Amulet
                if(player.weapon[0] == "")
                {
                    player.weapon[0] = "STRANGE AMULET";
                }
                else if(player.weapon[1] == "")
                {
                    player.weapon[1] = "STRANGE AMULET";
                }
                else if(player.weapon[2] == "")
                {
                    player.weapon[2] = "STRANGE AMULET";
                }
                else
                {
                    cout << "YOUR INVENTORY IS FULL. YOU CANNOT BUY THIS!" << endl;
                    // Change Value of Buy to make the program loop again
                    buy = -1;
                }
            }
            else
            {
                cout << "'ARE YA TRYNA SCAM ME KIDDO?!?!? I CAN CLEARLY SEE YA DONT HAVE THE\n"
                        << "GoOoOOLLD FOR THIS!" << endl;
                // Change Value of Buy to make the program loop again
                buy = -1;
            }
        }
        else if(buy == 5)
        {
            cout << "'WELL IF YOU SAY SO KIDDO!' The Man Scrambles Off Into A Darker Part Of The Room.\n"
                    << "You Decide To Follow Him, To See Where He Is Going. But When You Reach The Darker Part Of\n"
                    << "The Room He's Gone...." << endl;
        }        
        else
        {
            cout << "You Entered An Invalid Number! Please Try Again!!!" << endl;
        }
    }
    while(buy > 5 || buy < 0);
    
    cout << "The Room Explored, You Find Two Doors Ahead Of You. LEFT or RIGHT???" << endl;
    cout << "Which Door Will You Take?" << endl;
    do
    {
        cin >> path;
        path = tolower(path[0]);
        if(path == "l")
        {
            pathChange(-1, player, dungeonRooms); 
        }
        else if(path == "r")
        {
            pathChange(+1, player, dungeonRooms);
        }
        else // Error Checking
        {
            cout << "ERROR!!! YOU ENTERED SOMETHING INVALID, PLEASE TRY AGAIN" << endl;
        }
    }
    while(path != "l" && path != "r");  
    
}

/**
 * A Bit of a Freebie room, nothing good or bad will happen in this room. gives 2 options
 * out
 * @param player the Hero
 * @param dungeonRooms vector of available dungeonRooms 
 */
void emptyHallway(Hero& player, vector<int>& dungeonRooms)
{
    screenClear();
    string path;
    cout << "Finding Yourself In A Narrow Corridor, You Regain Your Composure. The Lamps\n"
            << "Are Well Lit For You Can See To The End Of The Hallway. Moving Down The Hall You\n"
            << "See Two Doors, One On The Left Side And One On the Right." << endl;
    cout << "Which Door Will You Take? (LEFT/RIGHT)" << endl;
    do
    {
        cin >> path;
        path = tolower(path[0]);
        if(path == "l")
        {
            pathChange(+3, player, dungeonRooms); 
        }
        else if(path == "r")
        {
            pathChange(-3, player, dungeonRooms);
        }
        else // Error Checking
        {
            cout << "ERROR!!! YOU ENTERED SOMETHING INVALID, PLEASE TRY AGAIN" << endl;
        }
    }
    while(path != "l" && path != "r");  
}

void manaDrainSpiders(Hero& player, vector<int>& dungeonRooms)
{
    screenClear();
    string path;
    int damage = 1;
    int spiders = (rand() % 4) + 1;
    
    cout << "Stepping Into A Damp And Cold Room, Your Eyes Adjust. Cobwebs, Cobwebs Everywhere. Sticky, White Lines Cover The Entire Room.\n"
            << "The Silk Looks Much Thicker Than Normal Cobwebs, and You Take A Stick To Push Them Away But They Barely Budge. And Then They Start\n"
            << "To Vibrate. Looking Up, You See " << spiders << " Manadrain Spiders. As Enormous As They Are Terrifying, These Two Foot Spiders Can Drain\n"
            << "Your Life As Well As Your Spirit. " << spiders*8 << " Red Eyes Are Dead Set On You. You Must Fight!" << endl;
    
    // First Check For the Long Sword
    for(int i = 0; i < 2; i++)
    {
        if(player.weapon[i] == "LONG SWORD")
        {
            cout << "You Have The Long Sword!!! Additional Damage Will Be Dealt" << endl;
            damage++;
        }
    }
    
    //FIGHT
    string battle;
    int battleRand;
    for(int i; i < spiders; i++)
    {
        while(maxSpiders[i].health > 0 && gameOver(player) == false)
        {
            // Choose Spiders Move
            battleRand = (rand() % 2) + 1;
            cout << "Spider #" << i+1 << " Attacks!!!" << endl;
            cout << "Fight Or Block?" << endl;
            cin >> battle;
            battle = tolower(battle[0]);
            if(battle == "f" && battleRand == 1)
            {
                cout << "Charging Forward, You Swing At One Of The Spiders. The Creature Lets Out A Pained Scream,\n"
                        << "And Writhes In Agony" << endl;
                maxSpiders[i].health = maxSpiders[i].health - damage;
                cout << "Spider #" << i+1 << " Has " << maxSpiders[i].health << " Health Left!" << endl;
            }
            else if(battle == "b" && battleRand == 2)
            {
                cout << "The Spider Charges, But You are Able To Fend Off It's Attack. Reeling Back, The Spider Stumbles,\n"
                        << "And Taking Advantage Of The Situation, You Strike, Wounding The Creature." << endl;
                maxSpiders[i].health = maxSpiders[i].health - 1;
                cout << "Spider #" << i+1 << " Has " << maxSpiders[i].health << " Health Left!" << endl;               
            }
            else if(battle == "f" && battleRand == 2)
            {
                cout << "Charging Forward, You Swing At One Of The Spiders. The Creature Sways With Shocking Agility,\n"
                        << "Dodging Your Hit. Letting Out What Can Only Be Described As An Evil Laugh, The Spider Spits A Green Venom\n"
                        << "In Your Direction. Unable To Dodge It, It Burns Like Your Body Is On Fire" << endl;
                cout << "VENOM: LOSE 1 HEALTH" << endl;
                player.health = player.health - 1;
                cout << player.health << " HEALTH REMAINING!" << endl;
            }
            else if(battle == "b" && battleRand == 1)
            {
                cout << "The Spider Rushes In With Scittering Haste, Moving Around The Room At A Speed You Can Barely Keep Up With. You\n"
                        << "Can Tell That The Creature Is Looking For A Window To Strike, And You Try Your Best To Keep Up With It's Movements.\n"
                        << "Unfortunately The Creature Is Too Fast For You, And You Feel It's Huge Fangs Digging Into Your Back" << endl;
                cout << "BITTEN AND SPIRIT DRAINED: LOSE 1 HEALTH & 2 SPIRIT" << endl;
                player.health = player.health - 1;
                player.spirit = player.spirit - 2;
                cout << player.health << " HEALTH REMAINING!" << endl;
                cout << player.spirit << " SPIRIT REMAINING!" << endl;
            }
            else
            {
                cout << "YOU ENTERED AN INVALID MOVE. PLEASE TRY AGAIN!!!" << endl;
            }
        }
    }
    
    // Reset Spiders
    for(int i = 0; i < spiders; i++)
    {
        maxSpiders[i] = Spider();
    }
    
    // The Player Only Gets Gold If They Are Still Alive. If They Died To The Spiders, Code Will Skip over the Else
    if(gameOver(player) == true)
    {
        cout << "YOU LOSE" << endl;
    }
    else
    {
        // Earn Gold Per Spider Kill
        for(int i = 0; i < spiders; i++)
        {
            player.gold = player.gold + maxSpiders[i].gold;
        }
        cout << "Taking the Gold From The Fallen Explorers That The Spiders Have Slain, You Now Have\n"
                << player.gold << " Gold!" << endl;
        // Mana Potion If You Slayed 2 or More Spiders
        if(spiders >= 2)
        {
            cout << "The Spider's Slain, You Look Around And Find A Glimmering Blue Bottle On The Ground. Picking It Up, You\n"
                    << "Can Tell It's An Unopened Mana Potion. Some Poor Explorer Must Have Tried To Drink It Before Meeting His Fate\n"
                    << "You Open The Bottle And A Blue Haze Comes Out Of It. It Smells Of the Ocean And Flowers. Taking A Drink, You Feel\n"
                    << "Completely Refreshed" << endl;
            player.spirit = player.spirit + 3;
            cout << "SPIRIT RESTORED: GAIN 3 SPIRIT" << endl;
            cout << "You Now Have " << player.spirit << " Spirit." << endl;
        }
        cout << "The Room Explored, You Find Two Doors Ahead Of You. LEFT or RIGHT???" << endl;
        cout << "Which Door Will You Take?" << endl;
        do
        {
            cin >> path;
            path = tolower(path[0]);
            if(path == "l")
            {
                pathChange(+1, player, dungeonRooms);
            }
            else if(path == "r")
            {
                pathChange(-1, player, dungeonRooms);
            }
            else // Error Checking
            {
                cout << "ERROR!!! YOU ENTERED SOMETHING INVALID, PLEASE TRY AGAIN" << endl;
            }
        }
        while(path != "l" && path != "r");
    }
}

void goblinsDen(Hero& player, vector<int>& dungeonRooms)
{
    screenClear();
    string path;
    int damage = 1;
    int gobs = (rand() % 4) + 1;
    
    cout << "The Air Is Heavy In This Room. You Find It Hard To Breathe. Suddenly You Hear Some Sinister Crackling And Gibberish.\n"
            << gobs << " Short Red Goblin(s) Appear, Each Holding A Different Makeshift Weapon. You Must Fight!" << endl;
    
    // Fight Sequence. May Turn Into A Function Have Not Decided Yet
    // First Check For the Long Sword
    for(int i = 0; i < 2; i++)
    {
        if(player.weapon[i] == "LONG SWORD")
        {
            cout << "You Have The Long Sword!!! Additional Damage Will Be Dealt" << endl;
            damage++;
        }
    }

    //FIGHT
    string battle;
    int battleRand;
    for(int i; i < gobs; i++)
    {
        while(maxGoblins[i].health > 0 && gameOver(player) == false)
        {
            // Choose Goblins Move
            battleRand = (rand() % 2) + 1;
            cout << "Goblin #" << i+1 << " Attacks!!!" << endl;
            cout << "Fight Or Block?" << endl;
            cin >> battle;
            battle = tolower(battle[0]);
            if(battle == "f" && battleRand == 1)
            {
                cout << "Charging Forward, You Swing At One Of The Goblins. The Creature Lets Out A Pained Scream,\n"
                        << "And Writhes In Agony" << endl;
                maxGoblins[i].health = maxGoblins[i].health - damage;
                cout << "Goblin #" << i + 1 << " Has " << maxGoblins[i].health << " Health Left!" << endl;
            }
            else if(battle == "b" && battleRand == 2)
            {
                cout << "The Goblin Charges, But You are Able To Fend Off It's Attack. Reeling Back, The Goblin Stumbles,\n"
                        << "And Taking Advantage Of The Situation, You Strike, Wounding The Fiend." << endl;
                maxGoblins[i].health = maxGoblins[i].health - 1;
                cout << "Goblin #" << i + 1 << " Has " << maxGoblins[i].health << " Health Left!" << endl;
            }
            else if(battle == "f" && battleRand == 2)
            {
                cout << "Charging Forward, You Swing At One Of The Goblins. The Creature Sways With Shocking Agility,\n"
                        << "Dodging Your Hit. Letting Out What Can Only Be Described As An Evil Laugh, The Goblin Cuts Deeply Into\n"
                        << "You With It's Sharp Claws" << endl;
                cout << "SCRATCHED: LOSE 1 HEALTH" << endl;
                player.health = player.health - 1;
                cout << player.health << " HEALTH REMAINING!" << endl;
            }
            else if(battle == "b" && battleRand == 1)
            {
                cout << "The Goblin Charges, Letting Out A Screech As It Comes Towards You. Taken Aback From The Sudden Screech, You\n"
                        << "Lose Your Composure For Just A Moment. Seizing The Opportunity, The Goblin Bites You, Drawing Blood" << endl;
                cout << "BITTEN: LOSE 1 HEALTH" << endl;
                player.health = player.health - 1;
                cout << player.health << " HEALTH REMAINING!" << endl;
            }
            else
            {
                cout << "YOU ENTERED AN INVALID MOVE. PLEASE TRY AGAIN!!!" << endl;
            }
        }
    }
    // Reset Gobs
    for(int i = 0; i < gobs; i++)
    {
        maxGoblins[i] = Goblin();
    }


    // The Player Only Gets Gold If They Are Still Alive. If They Died To The Goblins, Code Will Skip over the Else
    if(gameOver(player) == true)
    {
        cout << "******************************************************************************************\n"
                "*                                                                                        *\n"
                "*                                      YOU  DIED                                         *\n"
                "*                                                                                        *\n"
                "******************************************************************************************\n" << endl;
    }
    else
    {
        // Earn Gold Per Goblin Kill
        for(int i = 0; i < gobs; i++)
        {
            player.gold = player.gold + maxGoblins[i].gold;
        }
        cout << "Taking the Gold That The Goblins Collected From Previous Explorers, You Now Have\n"
                << player.gold << " Gold!" << endl;
        // Free Long Sword If You Slayed 3 or More Goblins
        if(gobs >= 3)
        {
            cout << "The Goblins All Slain, You Look Around At The Cave-like Room. Scattered Trash And Junk Lay\n"
                    << "Everywhere, But Amidst The Junk You See Something Glimmer. Stepping Up Closer You See A Hilt,\n"
                    << "And Drawing It Out You Discover A Beautiful Great Sword" << endl;
            int entry = 0;
            int loc = 0;
            do
            {
                if(player.weapon[loc] == "")
                {
                    player.weapon[loc] = "LONG SWORD";
                    entry++;
                }
                loc++;
            }
            while(entry != 1);
        }

        cout << "The Room Explored, You Find Two Doors Ahead Of You. LEFT or RIGHT???" << endl;
        cout << "Which Door Will You Take?" << endl;
        do
        {
            cin >> path;
            path = tolower(path[0]);
            if(path == "l")
            {
                pathChange(-1, player, dungeonRooms);
            }
            else if(path == "r")
            {
                pathChange(+1, player, dungeonRooms);
            }
            else // Error Checking
            {
                cout << "ERROR!!! YOU ENTERED SOMETHING INVALID, PLEASE TRY AGAIN" << endl;
            }
        }
        while(path != "l" && path != "r");
    }
}

void theDragonsLair(Hero& player, vector<int>& dungeonRooms)
{
    screenClear();
    string path;
    int damage = 1;
    // Create Instance Of Dragon
    Dragon theDragon;
    cout << "You Step Into A Massively Open Hall, So Large You Did Not Expect A Room So Spacious Here.\n"
            << "You Can See A Large Pile Of Gold In The Center Of The Room. Enough To Buy A Castle And\n"
            << "The Lord Of The Castle Along With It....But Of Course...There's Only One Thing In The World\n"
            << "That Hordes Gold Like That.....And Then You Hear It. A Deafening Roar. The Dragon Has Appeared!!!\n" << endl;
    cout << "You Must Fight!" << endl;

    // First Check For the Long Sword
    for(int i = 0; i < 2; i++)
    {
        if(player.weapon[i] == "LONG SWORD")
        {
            cout << "You Have The Long Sword!!! Additional Damage Will Be Dealt" << endl;
            damage = damage + 2;
        }
    }

    // Then Check For the Strange Amulet, the secret easter egg of the game. ANd yes, it is meant to stack with the long sword
    for(int i = 0; i < 2; i++)
    {
        if(player.weapon[i] == "SRANGE AMULET")
        {
            cout << "Before Engaging In The Battle, You Lock Eyes With The Grand Creature. But The Winged Beast Does Not\n"
                    << "Seem To Be Looking Directly At You, But Rather At The Strange Amulet That You Have Been Wearing. The Shiny\n"
                    << "Red Pendant Can Be Seen In The Dragon's Golden Eye. You Take It Off, And Throw It To The Distance. The\n" 
                    << "Dragon Follows The Pendant, Heavily Distracted By It's Newfound Artifact" << endl;
            cout << "With The Dragon Distracted, You Are Able To Attack It Much More Easily!!!" << endl;
            cout << "TREASURE DISTRACTION: DAMAGE MULTIPLIER!!!!!" << endl;
            damage = damage*4;
        }
    }




    //FIGHT
    string battle;
    int battleRand;
    do
    {
        // Choose Dragon's Move
        battleRand = (rand() % 2) + 1;
        cout << "Fight Or Block?" << endl;
        cin >> battle;
        battle = tolower(battle[0]);
        if(battle == "f" && battleRand == 1)
        {
            cout << "Charging At The Monstrous Beast, You Swing Your Weapon With All Your Might. The\n"
                    << "Dragon's Scales Are Tough, But You Are Able To Inflict A Wound Upon The Giant Creature\n" << endl;
            theDragon.health = theDragon.health - damage;
            cout << "The Dragon Has " << theDragon.health << " Health Left!" << endl;
        }
        else if(battle == "b" && battleRand == 2)
        {
            cout << "The Dragon Releases A Roar, And You Can See It's Mouth Starting To Glow. You See A Nearby Shield Laying Next\n"
                    << "To Another Fallen Warrior. Taking It, You Hold It Up Just In Time. The Creature Releases A Billowing Burst Of\n"
                    << "Wind And Flame, Bringing You To Your Knees From The Force. But The Shield Holds, If Just Barely, And You Manage\n"
                    << "To Survive The Attack" << endl;
        }
        else if(battle == "f" && battleRand == 2)
        {
            cout << "Charging At The Ferocious Winged Creature. You Swing Your Weapon With Every Ounce Of Strength You Have\n"
                    << "Sadly, It Is Not Enough. The Tough Scales On The Dragon Hold Like An Iron Shield. It Seems Like You Didn't Even\n"
                    << "Scratch The Scales. Unfortunately The Monster Noticed You, And Whips You Away With It's Massive Black Spiked Tail\n" << endl;
            cout << "THROWN INTO THE WALL: -2 HEALTH" << endl;
            player.health = player.health - 2;
            cout << player.health << " HEALTH REMAINING!" << endl;
        }
        else if(battle == "b" && battleRand == 1)
        {
            cout << "The Dragon Releases A Roar, And You Can See It's Mouth Starting To Glow. You Don't Have Anything To Defend Yourself With\n"
                    << "This Time So You Make A Run For The Nearest Broken Pillar To Try To Stay Safe. Unfortunately It's Too Late. As The\n"
                    << "Dragon Lets Out It's Fiery Blast, You Get Burned Just Before Diving To Safety\n" << endl;
            cout << "BURNED: -1 HEALTH" << endl;
            player.health = player.health - 1;
            cout << player.health << " HEALTH REMAINING!" << endl;
        }
        else
        {
            cout << "YOU ENTERED AN INVALID MOVE. PLEASE TRY AGAIN!!!" << endl;
        }
    }
    while(theDragon.health > 0 && gameOver(player) == false);

    // If The Player Died They Win Nothing
    if(gameOver(player) == true)
    {
        cout << "******************************************************************************************\n"
                "*                                                                                        *\n"
                "*                                      YOU  DIED                                         *\n"
                "*                                                                                        *\n"
                "******************************************************************************************\n" << endl;
    }
    else
    {
        // Earn Gold For Killing The Dragon
        player.gold = player.gold + theDragon.gold;

        cout << "Taking the Gold That The Dragon Collected From Previous Explorers, You Now Have\n"
                << player.gold << " Gold!" << endl;

        cout << "The Room Explored, You Find Three Doors Ahead Of You. LEFT, STRAIGHT, or RIGHT???" << endl;
        cout << "Which Door Will You Take?" << endl;
        do
        {
            cin >> path;
            path = tolower(path[0]);
            if(path == "l")
            {
                pathChange(-1, player, dungeonRooms);
            }
            else if(path == "r")
            {
                pathChange(+1, player, dungeonRooms);
            }
            else if(path == "s")
            {
                pathChange(0, player, dungeonRooms);
            }
            else // Error Checking
            {
                cout << "ERROR!!! YOU ENTERED SOMETHING INVALID, PLEASE TRY AGAIN" << endl;
            }
        }
        while(path != "l" && path != "r");
    }
}

/**
 * Function to check if the player has died. Checks if health or spirit at at 0 or negative.
 * @param player
 */
bool gameOver(Hero& player)
{
    if(player.health <= 0)
    {
        cout << "Your Health Has Fallen To Zero!!!" << endl;
        return true;
    }
    else if(player.spirit <= 0)
    {
        cout << "Your Spirit Has Fallen To Zero!!!" << endl;
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * Function to Output and Save the User's Score at the end of the game. Everything has a value 
 * and they all get added up together. Read's from the HighScore file and if the current score is
 * higher than the score in the save file, will overwrite
 * @param player the player in the current game and all their stats.
 */
void gameWin(Hero& player)
{
    int totalScore = 0;
    int roomScore = 0;
    int weaponScore = 0;
    int oldHighScore;
    // Each piece of health, spirit, or gold is worth 10 points
    totalScore = (player.gold + player.health + player.spirit)*10;
    // There are 7 total rooms. Subtract the number of room the player traveled through to get rooms left
    // 20 points per room remaining
    roomScore = (7 - player.moves)*20;
    // Check For Valuables
    // First Check For the Long Sword
    for(int i = 0; i < 2; i++)
    {
        if(player.weapon[i] == "LONG SWORD")
        {
            cout << "You Obtained The Long Sword This Game!!!" << endl;
            cout << "BONUS POINTS!!!" << endl;
            weaponScore = weaponScore + 150;
        }
    }
    // Then Check For The Amulet
    for(int i = 0; i < 2; i++)
    {
        if(player.weapon[i] == "STRANGE AMULET")
        {
            cout << "You Obtained The Strange Amulet This Game!!!" << endl;
            cout << "BONUS POINTS!!!" << endl;
            weaponScore = weaponScore + 200;
        }
    }
    // Add Up Everything
    totalScore = totalScore + roomScore + weaponScore;
    cout << "YOU RECEIVED A SCORE OF " << totalScore << " POINTS!!!" << endl;
    
    // Check If the Player Got A New High Score
    oldHighScore = highScoreDungeon(totalScore);
    if(totalScore > oldHighScore)
    {
        cout << "NEW HIGH SCORE!!!!" << endl;
    }
    displayHighScore(totalScore);
}

/**
 * Function to get values from an input and return the value back to main
 * @param input whatever input is to be read
 * @return value returns the value back to main
 */
int getFromInput(istream& input)
{
    int value;
    input >> value;

    return value;
}

int highScoreDungeon(int score)
{
    ofstream fout;
    ifstream fin;
    int highScore;
       
    fin.open("DungeonDiverHighScore.dat");  
    // Error check the file//Create Initial File if none currently exists
    if(fin.fail())
    {
        // Create Initial File
        fout.open("DungeonDiverHighScore.dat");
        fout << score << endl;

        fout.close(); 
        return score;

    }
    else // If the File Already Exists, We need to read the information and update it.
    {
        string null;
        while(!fin.eof())
        {
            highScore = getFromInput(fin); 
        }
        fin.close();
        
        // Check If the Player Has The New High Score
        if(highScore < score)
        {
            //Overwrite File
            fout.open("DungeonDiverHighScore.dat");
            fout << score << endl;

            fout.close();
        }
        else // If the High Score Was Previously Obtained
        {

        }
        return highScore;
    }
}

void displayHighScore(int num)
{
    int highScore = highScoreDungeon(num);
    cout << "The High Score Is Currently: " << highScore << " Points" << endl;
}

/**
 * Function to delete a specific number from the list of rooms. Function will 
 * loop, storing every value but that deleted value. 
 * @param v the vector
 * @param size the size of the vector
 * @param deleteValue the value the user wants to delete
 */
void deleteNum(vector<int>& v, int deleteValue)
{
    vector<int> numbers;
    int match = 0;
    int newSize = 0;
    // Check if the vector has the value
    for(int i = 0; i < v.size(); i++)
    {
        // Store all values BUT the value chosen by the user in a temp vector.
        if(deleteValue != v[i])
        {
            numbers.push_back(v[i]);
            newSize++;
        }
        else // If the loop finds a match
        {
            match++;
        }
    }
    // Reassign the temp vector back to main
    for(int i = 0; i < newSize; i++)
    {
        v[i] = numbers[i];
    }
    // delete the last values for each time loop found a match
    for(int i = 0; i < match; i++)
    {
        v.pop_back();
    }
}

/**
 * Function to check if a number is inside the vector or not. Bool function, will
 * return true if value is found in the vector, false if not,
 * @param v the vector
 * @param size the size of the vector
 * @param findValue the value the user wants to check for
 * @return false if there are no matches, true if not
 */
bool findAndDeleteNum(vector<int>& v, int findValue)
{
    int match = 0;
    // Check if the vector has the value
    for(int i = 0; i < v.size(); i++)
    {
        // Check if the number is inside the vector
        if(findValue == v[i])
        {
            match++;
        }
    }
    if(match == 0)
    {
        return false;
    }
    else
    {
        deleteNum(v, findValue);
        return true;
    }
}




