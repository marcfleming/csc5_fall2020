/*
* Name: Marc Fleming
* Student ID: 2858623
* Date: 03/20/2022
* HW: Homework 4
* Problem:
* I certify this is my own work and code
*/

// FLOWCHARTS AVAILABLE HERE
// https://lucid.app/lucidchart/078c77b0-7622-4edb-8343-f352fd377a41/edit?invitationId=inv_05e6845a-38e7-4811-9fab-17307622d598
// https://lucid.app/lucidchart/0c249656-cea7-4e1b-bb38-ba6c7c3d23f2/edit?invitationId=inv_3ff6af50-3fad-45d0-b143-608b7ff49bd1

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{

    //Wrap the Entire Menu in a Do-While Loop

    string doWhileMenuInput;
    string repeatGameInput;
    string rockPaperMenuInput;
    string mpgMenuInput;
    srand(time(0));
    

    do
    {

        //Here I am creating the Menu

        int menuOption;
        cout << "Welcome to the Homework Menu!"
                << "Please Select From the Following Options:" << endl;
        cout << setfill('-') << setw(71) << " " << endl;
        cout << setfill(' ') << "(1-3): Rock Paper Scissors"
                << setw(32) << "(4): MPG Calculator" << endl;
        cout << "(5): The Telephone Game" << endl;
        cin >> menuOption;

        switch ( menuOption )
        {
            case 1: case 2: case 3:
                /* QUESTION 2: Generate pseudo code for Problem 6 of Homework 3. The pseudo code
                 * can be turned in as a regular text file.
                 */

                //PSEUDOCODE
                //NOT ACTUAL CODE

                // Ask Player One and Player Two to choose rock, paper, or scissors.
                // If both players have selected the same option, Output a draw
                // Otherwise, generate win statements to decide who the winner is
                // Output the winner
                // Ask the User if they would like to play again, if so, repeat the game.

                /* QUESTION 3: Rewrite the rock paper scissors to allow two AI to play one 
                 * another. Put this program and the program from Homework 3, problem 6 into 
                 * one project, specifically, allow the user to select between both programs 
                 * through some sort of menu-based operation.
                 */
            {
                do
                {
                    // SubMenu For Rock Paper Scissors.
                    // (Side Note: I originally wanted to make this a switch
                    //  case but I could not seem to get it to work inside 
                    //  another switch case. Is that possible? 
                    
                    int rockPaperOption;
                    cout << "Welcome to the Rock Paper Scissors Menu!" << endl;
                    cout << "Please Select From the Following Options:" << endl;
                    cout << setfill('-') << setw(71) << " " << endl;
                    cout << setfill(' ') << "1) PVP"
                            << setw(37) << "2) CPU vs CPU" << endl;
                    cin >> rockPaperOption;
                    if ( rockPaperOption == 1 )
                    {
                        do
                        {
                            // Decide what Data I am storing, and assign data types for variables
                            
                            string playerOne;
                            string playerTwo;

                            //Ask the User to Input Values for Variables
                            
                            cout << "Welcome to Rock, Paper, Scissors!" << endl;
                            cout << "Two Players Will Each Choose Rock, Paper or Scissors." << endl;
                            cout << "Player 1 Will Go First" << endl;
                            cout << "Player 1, Please Choose Your Weapon!!! (Rock/Paper/Scissors): ";
                            cin >> playerOne;
                            playerOne = (tolower(playerOne[0]));
                            cout << "Thank you! Player 2, Please Choose Your Weapon!!! (Rock/Paper/Scissors): ";
                            cin >> playerTwo;
                            playerTwo = (tolower(playerTwo[0]));
                            
                            //Create Function to achieve desired results    
                            
                            if ( playerOne == ("s") && playerTwo == ("p") )
                            {
                                cout << "Scissors cut paper, Player 1 Wins" << endl;
                            }
                            else if ( playerOne == ("p") && playerTwo == ("r") )
                            {
                                cout << "Paper covers rock, Player 1 Wins" << endl;
                            }
                            else if ( playerOne == ("r") && playerTwo == ("s") )
                            {
                                cout << "Rock breaks scissors, Player 1 Wins" << endl;
                            }
                            else if ( playerOne == ("p") && playerTwo == ("s") )
                            {
                                cout << "Scissors cut paper, Player 2 Wins" << endl;
                            }
                            else if ( playerOne == ("r") && playerTwo == ("p") )
                            {
                                cout << "Paper covers rock, Player 2 Wins" << endl;
                            }
                            else if ( playerOne == ("s") && playerTwo == ("r") )
                            {
                                cout << "Rock breaks scissors, Player 2 Wins" << endl;
                            }
                            else if ( playerOne == playerTwo )
                            {
                                cout << "It's a Draw! Nobody Wins." << endl;
                            }
                            else
                            {
                                cout << "ERROR! A PLAYER HAS ENTERED AN INVALID CHARACTER!!" << endl;
                                cout << "PLEASE TRY AGAIN" << endl;
                            }
                            cout << setfill('-') << setw(65) << " " << endl;
                            cout << "Do you want to play again? Y/N : " << endl;
                            cin >> repeatGameInput;
                        }
                        while ( tolower(repeatGameInput[0]) == 'y' );
                        cout << "Thank you for playing!" << endl;
                    }
                    else if ( rockPaperOption == 2 )
                    {
                        do
                        {
                            // Decide what Data I am storing, and assign data types for variables
                            
                            string weaponOne;
                            string weaponTwo;
                            int ai;
                            int cpuOne = 0, cpuTwo = 0;

                            //Ask the User to Input Values for Variables
                            
                            cout << "Welcome to Rock, Paper, Scissors!" << endl;
                            cout << "Two CPUs Will Each Choose Rock, Paper or Scissors." << endl;
                            cout << "CPU 1 Will Go First" << endl;
                            cout << "CPU 1, Please Choose Your Weapon!!! (Rock/Paper/Scissors): " << endl;
                            ai = (rand() % 3) + 1;
                            cpuOne = ai;
                            if ( cpuOne == 1 )
                            {
                                weaponOne = "Rock";
                            }
                            else if ( cpuOne == 2 )
                            {
                                weaponOne = "Paper";
                            }
                            else if ( cpuOne == 3 )
                            {
                                weaponOne = "Scissors";
                            }
                            ai = 0;
                            // Sleep(2000); I removed sleep. Could You explain why I can't use it? It helps slow down
                            // The system so the user can read what the CPUs are choosing.
                            cout << "Computer One Chose " << weaponOne << endl;
                            weaponOne = (tolower(weaponOne[0]));
                            cout << "Thank you! CPU 2, Please Choose Your Weapon!!! (Rock/Paper/Scissors): " << endl;
                            ai = (rand() % 3) + 1;
                            cpuTwo = ai;
                            if ( cpuTwo == 1 )
                            {
                                weaponTwo = "Rock";
                            }
                            else if ( cpuTwo == 2 )
                            {
                                weaponTwo = "Paper";
                            }
                            else if ( cpuTwo == 3 )
                            {
                                weaponTwo = "Scissors";
                            }
                            ai = 0;
                            //Sleep(2000);
                            cout << "Computer Two Chose " << weaponTwo << endl;
                            weaponTwo = (tolower(weaponTwo[0]));
                            
                            //Create Function to achieve desired results    
                            
                            if ( weaponOne == ("s") && weaponTwo == ("p") )
                            {
                                cout << "Scissors cut paper, CPU 1 Wins" << endl;
                            }
                            else if ( weaponOne == ("p") && weaponTwo == ("r") )
                            {
                                cout << "Paper covers rock, CPU 1 Wins" << endl;
                            }
                            else if ( weaponOne == ("r") && weaponTwo == ("s") )
                            {
                                cout << "Rock breaks scissors, CPU 1 Wins" << endl;
                            }
                            else if ( weaponOne == ("p") && weaponTwo == ("s") )
                            {
                                cout << "Scissors cut paper, CPU 2 Wins" << endl;
                            }
                            else if ( weaponOne == ("r") && weaponTwo == ("p") )
                            {
                                cout << "Paper covers rock, CPU 2 Wins" << endl;
                            }
                            else if ( weaponOne == ("s") && weaponTwo == ("r") )
                            {
                                cout << "Rock breaks scissors, CPU 2 Wins" << endl;
                            }
                            else if ( weaponOne == weaponTwo )
                            {
                                cout << "It's a Draw! Nobody Wins." << endl;
                            }
                            else
                            {
                                cout << "ERROR! A CPU HAS ENTERED AN INVALID CHARACTER!!" << endl;
                                cout << "PLEASE TRY AGAIN" << endl;
                            }
                            cout << setfill('-') << setw(65) << " " << endl;
                            cout << "Do you want to play again? Y/N : " << endl;
                            cin >> repeatGameInput;
                        }
                        while ( tolower(repeatGameInput[0]) == 'y' );
                        cout << "Thank you for playing!" << endl;
                    }
                    else
                    {
                        cout << "Sorry Friend, That's not a Valid Entry!" << endl;
                        break;
                    }
                    cout << setfill('-') << setw(65) << " " << endl;
                    cout << "Would You Like To Return to the Rock Paper Scissors Menu? Y/N : " << endl;
                    cin >> rockPaperMenuInput;
                }
                while ( tolower(rockPaperMenuInput[0]) == 'y' );
                cout << "Come Back Soon!" << endl;
                break;
            }
            case 4:
            {
                int mpgMainInput;
                cout << "Hello And Welcome to the MPG Calculator!" << endl;
                cout << setfill('-') << setw(65) << " " << endl;
                cout << "please select from the following: " << endl;
                cout << setfill(' ') << "(1) File Data Input " << setw(30) 
                        << "(2) User Input" << endl;
                cin >> mpgMainInput;
                
                if (mpgMainInput == 1)
                {
                    do
                    {

                    /* QUESTION 4: A liter is 0.264179 gallons. Write a program that will read 
                     * in the number of liters of gasoline consumed by the user’s car and the 
                     * number of miles traveled by the car. Then, output the number of miles
                     * per gallon the car delivered. Your program should allow the user to
                     * repeat this calculation as often as the user wishes.  
                     */

                    /* QUESTION 8: PSEUDOCODE FOR QUESTION 4: NOT ACTUAL CODE: 
                     */

                    // Initialize Variables, for liters, miles, gallons, mpg.
                    // Also initialize variables to read from file I/O
                    // Create data.dat file, adding data to it to be stored for later use
                    // Reopen data.dat file, using initialized variables to store data
                    // Transfer data out from data.dat file
                    // Create function to calculate MPG
                    // Output information in a clear and concise way
                    // Ask the user if they want to repeat the program


                    // Decide what Data I am storing, and assign data types for variables
                        double liters, litersText;
                        double miles, milesText;
                        double gallons;
                        double mpg;

                        cout << "Welcome to the MPG Calculator!" << endl;
                        cout << "Here, you will input the Liters of gas you used, and the Miles" << endl;
                        cout << "you drove. Then I will show you your vehicle's MPG." << endl;
                        cout << "Please enter how many liters you used: " << endl;

                    /* QUESTION 7: Modify your program from problem 4 so that it 
                     * will take input data, from a file called “data.dat”.
                     */
                        string input;
                        ofstream fout;
                        fout.open("data.dat");

                        fout << "25" << endl;
                        fout << "214" << endl;

                        fout.close();

                        ifstream fin;
                        fin.open("data.dat");
                            {
                                string file = "data.dat";
                                {
                                    ifstream fin(file);
                                    fin >> litersText >> milesText;
                                }
                            }

                        liters = litersText;
                        cout << "Liters = " << liters << endl;
                        cout << " " << endl;
                        cout << "Thank you. How far did you drive? Please enter your information" << endl;
                        cout << "in miles: " << endl;
                        miles = milesText;
                        cout << "Miles = " << miles << endl;
                        cout << " " << endl;

                        fin.close();
                    // Create Function to achieve desired results

                        gallons = liters * 0.264179;
                        mpg = miles / gallons;

                    // Output Data

                        cout << "Thank you for your information!" << endl;
                        cout << "Your car gets " << mpg << " Miles Per Gallon." << endl;

                        cout << setfill('-') << setw(65) << " " << endl;
                        cout << "Do You Want to Run the Program Again? Y/N : " << endl;
                        cin >> mpgMenuInput;
                    }
                    
                    // Menu Return
                    
                    while ( tolower(mpgMenuInput[0]) == 'y' );
                    cout << "Thank you!" << endl;
                    break;
                }
                else if (mpgMainInput == 2)
                {
                    
                    do
                    {

                    /* QUESTION 9: Modify problem 7 in which data for two cars 
                     * will be input. Output the number of miles per gallon 
                     * delivered by each car. Your program will also announce 
                     * which car has the best fuel efficiency. This problem does
                     *  not require the user to perform the calculation as many 
                     * times as they wish.
                     */
                    
                    // PSUEDOCODE
                         
                    // Initialize Variables, for liters, miles, gallons, mpg.
                    // Initialize Variables for second vehicle
                    // Obtain information from user for both vehicles
                    // Create function to calculate MPG
                    // Determine better MPG
                    // Output information in a clear and concise way
                    // Ask the user if they want to repeat the program


                    // Decide what Data I am storing, and assign data types for variables
                        double litersOne, litersTwo;
                        double milesOne, milesTwo;
                        double gallonsOne, gallonsTwo;
                        double mpgOne, mpgTwo;
                        double mpgWinner;
                        string winner;

                        cout << "Welcome To The MPG Calculator!" << endl;
                        cout << "Here, You Will Input The Liters Of Gas For Two Vehicles, " << endl;
                        cout << "And Then The Miles You Drove. Then I Will Show You Your Vehicle's MPG." << endl;
                        cout << "Please Enter How Many Liters Vehicle 1 used: " << endl;
                        cin >> litersOne;

                        cout << " " << endl;
                        cout << "Thank you. How far did Vehicle One drive? Please enter your information" << endl;
                        cout << "in miles: " << endl;
                        cin >> milesOne;
 
                        cout << " " << endl;
                        cout << "Please Enter How Many Liters Vehicle 2 used: " << endl;
                        cin >> litersTwo;

                        cout << " " << endl;
                        cout << "Thank you. How far did Vehicle Two drive? Please enter your information" << endl;
                        cout << "in miles: " << endl;
                        cin >> milesTwo;
                        cout << " " << endl;

                        
                        
                    // Create Function to achieve desired results

                        gallonsOne = litersOne * 0.264179;
                        mpgOne = milesOne / gallonsOne;
                        
                        gallonsTwo = litersTwo * 0.264179;
                        mpgTwo = milesTwo / gallonsTwo;
                        
                        if (mpgOne == mpgTwo)
                        {
                            cout << "Both Vehicles get the same MPG!" << endl;
                            winner = "Both Vehicles";
                            mpgWinner = mpgOne = mpgTwo;
                        }
                        else if (mpgOne > mpgTwo)
                        {
                            mpgWinner = mpgOne;
                            winner = "Vehicle One";
                        }
                        else if (mpgOne < mpgTwo)
                        {
                            mpgWinner = mpgTwo;
                            winner = "Vehicle Two";
                        }
                        else
                        {
                            cout << "There must have been an error" << endl;
                        }
                        
                    // Output Data

                        cout << "Thank you for your information!" << endl;
                        cout << "Vehicle One gets " << mpgOne << " Miles Per Gallon." << endl;
                        cout << "Vehicle Two gets " << mpgTwo << " Miles Per Gallon." << endl;
                        cout << "Turns out, " << winner << " has the best fuel efficiency with" << endl;
                        cout << mpgWinner << " Miles Per Gallon!" << endl;
                        
                    // Menu Return
                        
                        cout << setfill('-') << setw(65) << " " << endl;
                        cout << "Do You Want to Run the Program Again? Y/N : " << endl;
                        cin >> mpgMenuInput;
                    }
                    while ( tolower(mpgMenuInput[0]) == 'y' );
                    cout << "Thank you!" << endl;
                    break;
                }
                else
                {
                    cout << "You Entered An Incorrect Value!" << endl;
                    cout << "Please Try Again!" << endl;
                }
            }
            case 5:
            /* QUESTION 5: Write a program prompts a user for a telephone number
             * that requires only one dash in between. The program then outputs 
             * the telephone number without the dash. The fake phone number can 
             * be as long as the user wants.
             */
            {
                                    
                string phoneNumber;
                string newNum = "";
                               
                cout << "Welcome To The Phone Number Converter!" << endl;
                cout << "Please Enter Your Phone Number: " << endl;
                cin.ignore();
                getline(cin, phoneNumber);

                for (int i = 0; i < phoneNumber.size(); i++) 
                {
                    if ((phoneNumber[i] >= '0' && phoneNumber[i] <= '9'))
                    {
                        newNum = newNum + phoneNumber[i];
                    }
                }
                phoneNumber = newNum;
                cout << "Your Phone Number is: " << phoneNumber << endl;
                
                break;
            }
            default:
                cout << "Yeah You didn't pick the right value" << endl;
                break;
        }
        cout << setfill('-') << setw(65) << " " << endl;
        cout << "Would You Like to Return To the Main Menu? Y/N?" << endl;
        cin >> doWhileMenuInput;
    }
    while ( tolower(doWhileMenuInput[0]) == 'y' );
    cout << "Have a Wonderful Day!" << endl;
    return 0;
}

