/*
 * Name: Marc Fleming
 * Student ID: 2858623
 * Date: 04/20/2022
 * HW: Homework 7
 * Problem: 6 Questions
 * I certify this is my own work and code
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

/*
 * 
 */
void readFromInput(istream& input);
int getFromInput(istream& input);
void sortArray(int fileNumbers[], int matrixSize);

int main(int argc, char** argv)
{
    string doWhileMenuInput;
    string fileIOMenuInput;
    string vectorMenuInput;
    srand(time(0));

    do
    {

        //Here I am creating the Menu
        int menuOption;
        cout << "Welcome to the Homework Menu!" << endl;
        cout << "Please Select From the Following Options:" << endl;
        cout << setfill('-') << setw(71) << " " << endl;
        cout << setfill(' ') << "(1): File I/O"
                << setw(29) << "(2): Vectors" << endl;
        cin >> menuOption;

        switch(menuOption)
        {
            case 1:
            {
                do
                {
                    int size;
                    int num[size];
                    int fileNumbers[size], evenNumbers[size];
                    int smallestNum = 100;
                    int largestNum = 0;
                    int howManyEven = 0;
                    string filename;
                    int median;

                    cout << "How Many Values Would You Like To Store? (MAX 19): " << endl;
                    cin >> size;

                    ofstream fout;

                    fout.open("data.dat");
                    for(int i = 0; i <= size; i++)
                    {
                        num[i] = (rand() % 100) + 1;
                        fout << num[i] << endl;
                    }
                    fout.close();

                    /* QUESTION 2: Implement number 1 where the file name is 
                     * received from the user. Make sure to do error checking 
                     * on the file itself.
                     */

                    ifstream fin;
                    do
                    {

                        cout << "Please type the file name you would like to use: " << endl;
                        cin >> filename;
                        fin.open(filename);

                        // Error check the file
                        if(fin.fail())
                        {
                            cout << "The reading of the file failed. It does not exist!" << endl;
                        }
                        else
                        {
                            while(!fin.eof())
                            {
                                readFromInput(fin);
                            }
                        }
                    }
                    while(!fin.eof());

                    // Obtain the largest and smallest values
                    for(int i = 0; i <= size; i++)
                    {
                        if(num[i] > largestNum)
                        {
                            largestNum = num[i];
                        }
                    }

                    for(int i = 0; i <= size; i++)
                    {
                        if(num[i] < smallestNum)
                        {
                            smallestNum = num[i];
                        }
                    }
                    fin.close();

                    cout << "The Largest Number is: " << largestNum << endl;
                    cout << "And the Smallest Number is: " << smallestNum << endl;

                    /* Question 3: Modify number 2 to calculate and print the 
                     * number of even numbers that the file contains. Create 
                     * three different files that contain zero, some, and all 
                     * even numbers. 
                     * Name the files data1.dat, data2.dat, and data3.dat
                     */

                    fin.open(filename);

                    // Error check the file
                    if(fin.fail())
                    {
                        cout << "The reading of the file failed. It does not exist!" << endl;
                    }
                    else
                    {
                        for(int i = 0; i < size; i++)
                        {
                            fileNumbers[i] = getFromInput(fin); // Read from the file and store data
                        }
                    }

                    // Calculate the even numbers
                    for(int i = 0; i < size; i++)
                    {
                        if(fileNumbers[i] % 2 == 0)
                        {
                            evenNumbers[i] = fileNumbers[i];
                            howManyEven++;
                            cout << evenNumbers[i] << " is even!" << endl;
                        }
                    }
                    cout << "And There Are " << howManyEven << " Even Numbers!!!" << endl;

                    // Create 3 different files that contain zero, some, and all even numbers
                    fout.open("data1.dat");
                    fout.close();

                    fout.open("data2.dat");
                    for(int i = 0; i <= (size / 2); i++) // Will store some of the even numbers
                    {
                        if(evenNumbers[i] != 0) // Necessary to remove 0s from the matrix
                        {
                            fout << evenNumbers[i] << endl;
                        }
                    }
                    fout.close();

                    fout.open("data3.dat");
                    for(int i = 0; i < size; i++) // Will store all of the even numbers
                    {
                        if(evenNumbers[i] != 0)
                        {
                            fout << evenNumbers[i] << endl;
                        }
                    }
                    fout.close();

                    /* QUESTION 4: Compute the median of a data file. The median
                     * is the middle number in a data set.
                     */

                    // Created a function to sort the matrix in order
                    sortArray(fileNumbers, size);

                    cout << "Sorted Array is:" << endl;
                    for(int i = 0; i < size; i++)
                    {
                        cout << fileNumbers[i] << " ";
                    }
                    cout << "\n";

                    if(size % 2 == 0)
                    {
                        median = (fileNumbers[(size / 2) - 1] + fileNumbers[(size / 2)]) / 2;
                    }
                    else
                    {
                        median = (fileNumbers[size / 2]);
                    }

                    cout << "The Median is: " << median << endl;

                    cout << setfill('-') << setw(65) << " " << endl;
                    cout << "Would You Like To Repeat This Assignment? Y/N : " << endl;
                    cin >> fileIOMenuInput;
                }
                while(tolower(fileIOMenuInput[0]) == 'y');
                cout << "Come Back Soon!" << endl;
                break;
            }
            case 2:
            {
                do
                {
                    /* QUESTION 5: Write a program that inserts 10 random integers 
                     * in a vector.
                     */

                    vector<int> numbers(10);
                    vector<int> oddNum(10);
                    int howManyOdd = 0;
                    int size = 10;
                    // Store the values inside a vector
                    for(int i = 0 + 1; i < size; i++)
                    {
                        numbers[i] = (rand() % 100) + 1;
                    }

                    // Display the vector to confirm integers have been stored
                    for(int i = 0 + 1; i < size; i++)
                    {
                        cout << numbers[i] << endl;
                    }

                    /* QUESTION 6: Modify program 5 to count the number of odd 
                     * integers located inside the vector.
                     */

                    for(int i = 0; i < size; i++)
                    {
                        if(numbers[i] % 2 == 1)
                        {
                            oddNum[i] = numbers[i];
                            howManyOdd++;
                            cout << oddNum[i] << " is odd!" << endl;
                        }
                    }
                    cout << "And There Are " << howManyOdd << " Odd Numbers!!!" << endl;


                    cout << " " << endl;
                    cout << setfill('-') << setw(65) << " " << endl;
                    cout << "Would You Like To Repeat This Assignment? Y/N : " << endl;
                    cin >> vectorMenuInput;
                }
                while(tolower(vectorMenuInput[0]) == 'y');
                cout << "Come Back Soon!" << endl;
                break;
            }
        }
        cout << setfill('-') << setw(65) << " " << endl;
        cout << "Would You Like to Return To the Main Menu? Y/N?" << endl;
        cin >> doWhileMenuInput;
    }
    while(tolower(doWhileMenuInput[0]) == 'y');
    cout << "Have a Wonderful Day!" << endl;
    return 0;
}

/**
 * Function to read values from input. Takes input, appends to value, then displays 
 * value to screen
 * @param input whatever input is to be read
 */
void readFromInput(istream& input)
{
    string value;
    input >> value;

    cout << "The value is: " << value << endl;
}

/**
 * Function to get values from an input and return the value back to main
 * @param input whatever input is to be read
 * @return value returns the value back to main
 */
int getFromInput(istream& input)
{
    int value;
    input >> value;

    return value;
}

/**
 * Function to sort a set matrix or array in order from smallest to greatest. Done
 * by checking if a value is less than any other value, if so, creates temp matrix and 
 * continues loop.
 * @param num
 * @param matrixSize
 */
void sortArray(int num[], int arraySize)
{
    int minimum, temp;
    for(int i = 0; i < arraySize; i++)
    {
        minimum = i;
        for(int j = i + 1; j < arraySize; j++)
            if(num[j] < num[minimum])
            {
                minimum = j;
            }
        temp = num[i];
        num[i] = num[minimum];
        num[minimum] = temp;
    }
}